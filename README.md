﻿# README #

NatNet Cpp Library v0.3.4 is an open source (cross platform thanks to LCG_Core_Cpp library) library used to directly communicate to a NatNet server, over Windows, Linux or Mac OS.


### What is this repository for? ###

#### Quick summary ####
This repo is useful for you if you are making motion capture with NaturalPoint's OptiTrack® software (any OptiTrack® streaming server using the NatNet protocol, like Motive), and you want to get the motion capture data streaming on Linux, Mac OS or Windows, in real time, using the C++ programming language and the NatNet's API.

### Building ###

##### Dependencies #####
* Cmake 2.8 or later
* Git 2.0 or later
* LCG_Core_Cpp library (https://bitbucket.org/lcgonzalez/lcg_core_cpp_library)

##### Building instructions #####
Just git clone this repository, make a building directory within it, execute Cmake inside that building directory 
using the directory where CMakeLists.txt project file is located as the source, and finally execute make.

Some scripts for compiling and testing can be found in the compiling folder.
```
./runme-linux.sh --help
```

**Commands for Linux:**
```
git clone https://git@bitbucket.org/lcgonzalez/natnet_cpp_library.git
cd natnet_cpp_library/compiling
./runme-linux.sh && ./runme-linux.sh install
```
**Commands for Windows:**

Note: For compiling, cmake.exe should be under some directory of the PATH environmental variable.
###### Visual Studio Community 2013 Compiler #######
```
git clone https://git@bitbucket.org/lcgonzalez/natnet_cpp_library.git
mkdir build
cd build
cmake.exe .. -G "Visual Studio 12 Win64"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe NatNet_Cpp_Library.vcxproj
```

##### Building and testing #####
In order to also run tests on the compiled library, please run the following commands.

**Commands for Linux:**
```
git clone https://git@bitbucket.org/lcgonzalez/natnet_cpp_library.git
cd natnet_cpp_library/compiling
./runme-linux.sh && ./runme-linux.sh tests
```
**Commands for Windows:**

Note: For testing, svn.exe and cmake.exe should be under some directory of the PATH environmental variable.
###### Visual Studio Community 2013 Compiler #######
```
git clone https://git@bitbucket.org/lcgonzalez/natnet_cpp_library.git
mkdir build
cd build
cmake.exe .. -DBUILD_TESTING=True -G "Visual Studio 12 Win64"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe NatNet_Cpp_Library.vcxproj
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe RUN_TESTS.vcxproj
```

Note for Visual Studio users under Windows:
Please change the "Visual Studio 12 Win64" flag to a suitable version number and Windows architecture (32 or 64 bits), according to your 
Visual Studio version and Windows architecture (32 or 64 bits)

### Licensing ###
Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.

IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.

                          License Agreement
                     For the NatNet Cpp Library

    NatNet Cpp Library is an open source library used to directly
    communicate to a NatNet server.

    NatNet Cpp Library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    NatNet Cpp Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, see <http://www.gnu.org/licenses/>,
    or write to the Free Software Foundation, Inc., 51 Franklin Street,
    Fifth Floor, Boston, MA  02110-1301  USA.
### Contributions and repo owner ###

##### Repo owner or admin #####
Any suggestions, contributions or issues, please contact the repo owner:

*Luis González <luis@lcgonzalez.com>*
