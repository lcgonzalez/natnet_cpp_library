@echo off

echo Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
echo.       
echo     This file is part of the NatNet Cpp Library.
echo.          
echo                          License Agreement
echo                     For the NatNet Cpp Library
echo. 
echo     NatNet Cpp Library is an open source library used to directly
echo     communicate to a NatNet server.
echo. 
echo     NatNet Cpp Library is free software; you can redistribute it and/or
echo     modify it under the terms of the GNU Lesser General Public
echo     License as published by the Free Software Foundation; either
echo     version 2.1 of the License, or (at your option) any later version.
echo. 
echo     NatNet Cpp Library is distributed in the hope that it will be useful,
echo     but WITHOUT ANY WARRANTY; without even the implied warranty of
echo     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
echo     Lesser General Public License for more details.
echo. 
echo     You should have received a copy of the GNU Lesser General Public
echo     License along with this library; if not, see ^<http://www.gnu.org/licenses/^>,
echo     or write to the Free Software Foundation, Inc., 51 Franklin Street,
echo     Fifth Floor, Boston, MA  02110-1301  USA.
echo. 
echo. 
echo //////////////////////////////////////////////////////////////////////////////
echo.
echo Note 1: For compiling, cmake.exe and svn.exe should be under some directory of the PATH environmental variable.
echo Currently, PATH = %path% 
echo.
rem Uncomment the following line if you already instaled Cmake in the following path, but that path is not in the PATH variable.
rem set "PATH=%PATH%;C:\Program Files (x86)\Cmake\bin"

echo If cmake.exe is not in the PATH, please uncomment the following line:
echo rem set "PATH=PATH;C:\Program Files (x86)\Cmake\bin"
echo Is it safe to place svn.exe in the same directory as cmake.exe:
echo C:\Program Files (x86)\Cmake\bin
echo.
echo Note 2: Please change the Visual Studio version number, in this line:
echo cmake .. -G "Visual Studio 12 Win64"
echo for one suitable to you, as well as the Win64 flag, if you are under a 32 bit Windows.
echo.
echo.
echo //////////////////////////////////////////////////////////////////////////////
echo Starting building process
echo.
echo.

rem building environment set up
SET @callerPath=%CD%
rem cd to batch file directory ~dp0
cd %~dp0

cd ..\..\
mkdir build
cd build
cmake.exe .. -DBUILD_TESTING=True -G "Visual Studio 12 Win64"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe NatNet_Cpp_Library.vcxproj
cd Debug
set "PATH=%PATH%;%cd%"
cd ..
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe tests.vcxproj
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe RUN_TESTS.vcxproj

rem building environment tear down
cd %@callerPath%

PAUSE
