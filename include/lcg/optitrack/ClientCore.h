// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file ClientCore.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.4
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_OPTITRACK_NATNETCLIENTCORE_H
#define LCG_OPTITRACK_NATNETCLIENTCORE_H

#include "NatNet.h"                 // lcg::optitrack::Command
#include "Dataset.h"                // lcg::optitrack::Position
#include "Markerset.h"              // lcg::optitrack::Markerset
#include "RigidBody.h"              // lcg::optitrack::RigidBody
#include "Skeleton.h"               // lcg::optitrack::Skeleton

#include <lcg/core/LCG_Core.h>      // LCG_CORE_LIB_API
#include <lcg/core/UdpSocket.h>     // lcg::core::UdpSocket
#include <lcg/core/Thread.h>        // lcg::core::Thread
#include <lcg/core/Version.h>       // lcg::core::Version
#include <map>                      // std::map
#include <vector>                   // std::vector

namespace lcg{
namespace optitrack{

#define MAX_NAMELENGTH              256

// NATNET message ids
#define NAT_PING                    0
#define NAT_PINGRESPONSE            1
#define NAT_REQUEST                 2
#define NAT_RESPONSE                3
#define NAT_REQUEST_MODELDEF        4
#define NAT_MODELDEF                5
#define NAT_REQUEST_FRAMEOFDATA     6
#define NAT_FRAMEOFDATA             7
#define NAT_MESSAGESTRING           8
#define NAT_UNRECOGNIZED_REQUEST    100
#define UNDEFINED                   999999.9999

#define MAX_PACKETSIZE				100000	// max size of packet (actual packet size is dynamic)

// sender
struct sSender
{
    char szName[MAX_NAMELENGTH];            // sending app's name
    unsigned char Version[4];               // sending app's version [major.minor.build.revision]
    unsigned char NatNetVersion[4];         // sending app's NatNet version [major.minor.build.revision]

};

struct sPacket
{
    unsigned short iMessage;                // message ID (e.g. NAT_FRAMEOFDATA)
    unsigned short nDataBytes;              // Num bytes in payload
    union
    {
        unsigned char  cData[MAX_PACKETSIZE];
        char           szData[MAX_PACKETSIZE];
        unsigned long  lData[MAX_PACKETSIZE/4];
        float          fData[MAX_PACKETSIZE/4];
        sSender        Sender;
    } Data;                                 // Payload

};

class LCG_CORE_LIB_API ClientCore {
public:
	ClientCore();
    ~ClientCore();

    enum State {NOT_CONNECTED, GETTING_VERSION, GETTING_MODEL_DEF, CONNECTED};

    int sendCommand(Command command);
    int connectToServer(std::string szLocalAddress, std::string szServerAddress, int serverCommandPort, int clientDataPort);
    int setWaitForNewFrameTimeout(unsigned int secTimeout, unsigned int uSecTimeout);
    int waitForNewFrame();  // The worker exceptions are processed and re-thrown on the main thread here
    int getServerVersion(int* serverVersion);   // Deprecated
    int getNatNetVersion(int* natNetVersion);   // Deprecated
    lcg::core::Version getServerVersion();
    lcg::core::Version getNatNetVersion();
    int getFrameNumber();
    double getLatency();

    // The Timecode parameter is interpreted differently when streaming Live or from File Playback (Edit).
    // In Live mode, the Timecode parameter is only valid when SMPTE timecode is present in your Mocap
    // hardware setup, typically when using the eSync and a timecode generator.  When present, the
    // Timecode parameter will be a correctly formatted SMPTE timecode value.
    // In Edit Mode, the Timecode parameter is the current frame number converted to SMPTE Timecode format.
    void getTimecode(unsigned int& outTimecode, unsigned int& outTimecodeSubFrame);
    std::string getTimecodeString();
    double getTimestamp();
    bool isServerRecording();
    bool haveTrackedModelsChanged();
    std::string getServerAppName();
    Markerset getMarkersetFromName(std::string markersetName);
    Markerset getUnidentifiedMarkers(); // Convenient method for calling getMarkersetFromName("_Unidentified_")
    Markerset getLabeledMarkers();  // Convenient method for calling getMarkersetFromName("_Labeled_")
    RigidBody getRigidBodyFromName(std::string rigidBodyName);
    Skeleton getSkeletonFromName(std::string skeletonName);
    void disconnect();
private:
    class BasicThread : public core::Thread {
        void initExceptionMap();

        // Exceptions
        std::runtime_error processExceptions_unknownExceptionID_Exception(int ID);
    protected:
        ClientCore* owner_;    // This is needed because C++ doesn't allow the use of non-static enclosing class members (until C++11)
        volatile bool exitThread_;
        volatile bool waitingTimeout_;
        int uSecTimeout_;    // Thread waiting for datagram timeout (Seconds),
        int secTimeout_;
        volatile bool exceptionRised_;

        enum ExceptionID {
            EXCEPTION,
            RUNTIME_ERROR,
            INVALID_ARGUMENT,
            DOMAIN_ERROR
        };

        // Exception handlers (for re-throwing the exceptions on the main thread).
        std::map<ExceptionID, std::exception*> exceptionsMap_;

        void riseException(std::exception* e);
    public:
        explicit BasicThread(ClientCore* owner);
        ~BasicThread();
        void exit();
        virtual int seTimeout(unsigned int secTimeout, unsigned int uSecTimeout);
        void getTimeout(unsigned int& outSecTimeout, unsigned int& outuSecTimeout);
        bool waitingTimeoutReached();
        void processExceptions();
    };

    class DataReadingThread : public BasicThread {
        virtual void run();
    public:
        explicit DataReadingThread(ClientCore* owner);
        virtual int seTimeout(unsigned int secTimeout, unsigned int uSecTimeout);
    };

    class CommandReadingThread : public BasicThread {
        virtual void run();
    public:
        explicit CommandReadingThread(ClientCore* owner);
        virtual int seTimeout(unsigned int secTimeout, unsigned int uSecTimeout);
    };

    class WaitForServerThread : public BasicThread {
        virtual void run();
    public:
        explicit WaitForServerThread(ClientCore* owner);
    };

    core::UdpSocket dataSocket_;        // The sockets should be declared before the threads so that the threads
    core::UdpSocket commandSocket_;     // can use them properly while in the constructor initailization list.
    DataReadingThread dataReadingThread_;
    CommandReadingThread commandReadingThread_;
    WaitForServerThread waitForServerThread_;
    core::Mutex mutex_;
    std::string *pLocalAddress;
    std::string *pServerAddress;
    std::string *pMulticastAddress;
    lcg::core::Version natNetVersion_;
    lcg::core::Version serverVersion_;
    int serverCommandPort;
    int clientDataPort;
    std::string appID;

    std::string serverName_;
    State state_;

    // Exceptions
    std::runtime_error errorWaitForNewFrameTimeoutException();
    std::runtime_error errorNoMarkersetFromNameException(std::string markersetName);
    std::runtime_error errorNoRigidBodyFromNameException(std::string rigidBodyName);
    std::runtime_error errorNoSkeletonFromNameException(std::string skeletonName);

    void createSockets();
    void requestVersions();
    void autoConnectToServer();
    int checkConnectionStatus();
    int updateModelDefinition();
    void processThreadsExceptions();

    class CommandProcessor {
        ClientCore* owner_;
    public:
        CommandProcessor(ClientCore* owner);
        void processData(const sPacket* pPacket);
        void unpackAndUpdateVersions(const char* pRawData);
    };

    class Frame {
        const ClientCore* owner_;    // This is needed because C++ doesn't allow the use of non-static enclosing class members (until C++11)
    public:
        Frame(const ClientCore* owner);
        void processData(const char* pRawData); // On error rises an exception of type std::runtime_error
        bool hasFrameChanged();
        int getFrameNumber();
        void getTimecode(unsigned int &outTimecode, unsigned int &outTimecodeSubFrame);
        std::string getTimecodeString();
        double getTimestamp();
        double getLatency();
        void resetLatency();
        bool isServerRecording();
        bool haveTrackedModelsChanged();

    private:
        int_fast16_t nBytes_;
        volatile int nLastFrameNumber_;   // Used to wait until last frame of data arrives
        int nOldFrameNumber_;    // Used to wait until last frame of data arrives
        unsigned int timecode_;
        unsigned int timecodeSubFrame_;
        double timestamp_;
        double lastTimestamp_;
        bool bIsRecording_;
        bool bHaveTrackedModelsChanged_;

        class Unpacker {
            Frame* owner_;
        public:
            Unpacker(Frame *owner);

            void unpackAndUpdateFrame(const char*& pRawFrameData);
            int getFrameNumber();
            double getLatency();
            void resetLatency();
            bool timecodeStringify(const unsigned int inTimecode,
                                   const unsigned int inTimecodeSubframe,
                                   std::string &str);

        private:
            const int_fast32_t EOD_TAG_;    // End of Data Packet Tag
            int tmpFrameNumber_;
            float badLatency_;
            double calculatedLatency_;
            bool firstLatency_;

            void checkIfPacketIsValid(const char*& pRawData);
            void unpackAndUpdateFrameNumber(const char*& pRawData);
            void unpackAndUpdateMarkersets(const char*& pRawData);
            void unpackAndUpdateUnidentifiedMarkers(const char*& pRawData);
            void unpackAndUpdateRigidBodies(const char*& pRawData, Skeleton *pSkeleton=NULL);
            void unpackAndUpdateSkeletons(const char*& pRawData);
            void unpackAndUpdateLabeledMarkers(const char*& pRawData);
            void unpackAndUpdateLatency(const char*& pRawData);
            void unpackAndUpdateTimecode(const char*& pRawData);
            void unpackAndUpdateTimestamp(const char*& pRawData);
            void updateCalculatedLatencyFromTimestamp();
            void unpackAndUpdateFrameParameters(const char*& pRawData);
            int_fast32_t unpackAndGetEODTag(const char*& pRawData);

            bool decodeTimecode(const unsigned int inTimecode,
                                const unsigned int inTimecodeSubframe,
                                int& hour, int& minute, int& second, int& frame,
                                int& subframe);

            // Exceptions
            std::runtime_error errorFrameOfMOCAPDataCorruptedException(int_fast32_t actualEODTag);
            std::runtime_error errorAttemptToFillUndefinedMarkersetException(const std::string& markersetName);
            std::runtime_error errorMarkersetMisalignmentErrorException(const std::string& markersetName, const int nMarkers);
            std::runtime_error errorAttemptToFillUndefinedRigidBodyFromIDException(const int rigidBodyID);
            std::runtime_error errorAttemptToFillUndefinedSkeletonFromIDException(const int skeletonID);
        };

        class Model {
            const Frame* owner_; // This is needed because C++ doesn't allow the use of non-static enclosing class members (until C++11)
        public:
            Model(const Frame* owner);

            int unpackAndDefineDefinition(const char* rawData);
            bool hasDefinition();
            void deleteDefinition();

            Markerset* getMarkersetFromName(std::string markersetName) const;
            Markerset* getMarkersetFromID(int markersetID) const;
            RigidBody* getRigidBodyFromName(std::string rigidBodyName) const;
            RigidBody* getRigidBodyFromID(int rigidBodyID) const;
            Skeleton* getSkeletonFromName(std::string skeletonName) const;
            Skeleton* getSkeletonFromID(int skeletonID) const;

        private:
            void addMarkerset(Markerset *newMarkerset);
            void addRigidBody(RigidBody *newRigidBody);
            void addSkeleton(Skeleton *newSkeleton);

            // These field members are 'mutable' because they are used from within const methods.
            mutable std::map<std::string, Markerset*> mNameToMarkerset;
            mutable std::map<int, Markerset*> mIDtoMarkerset;
            mutable std::map<std::string, RigidBody*> mNameToRigidBody;
            mutable std::map<int, RigidBody*> mIDtoRigidBody;
            mutable std::map<std::string, Skeleton*> mNameToSkeleton;
            mutable std::map<int, Skeleton*> mIDtoSkeleton;

            std::vector<Dataset*> mAllDatasets;

            void addUnidentifiedMarkersDefinition();
            void addLabeledMarkersDefinition();
        };

        Model model_;
        Unpacker unpacker_;

    public:
        Model* getModel();
    };

    CommandProcessor commandProcessor_;
    Frame frame_;
};

} // namespace optitrack
} // namespace lcg

#endif // LCG_OPTITRACK_NATNETCLIENTCORE_H
