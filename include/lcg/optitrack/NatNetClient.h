// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file NatNetClient.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.3
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_OPTITRACK_NATNETCLIENT_H
#define LCG_OPTITRACK_NATNETCLIENT_H

#include "NatNet.h"     // lcg::optitrack::Command
#include "Markerset.h"  // lcg::optitrack::Position
#include "RigidBody.h"  // lcg::optitrack::RigidBody
#include "Skeleton.h"   // lcg::optitrack::Skeleton

#include <lcg/core/Version.h>   // lcg::core::Version

#include <string>       // std::string

namespace lcg{
namespace optitrack{

class ClientCore;

class LCG_CORE_LIB_API NatNetClient {
public:
    NatNetClient();
    ~NatNetClient();

    int connectToServer(std::string szLocalAddress, std::string szServerAddress, int hostCommandPort, int hostDataPort);
    int setWaitForNewFrameTimeout(unsigned int secTimeout, unsigned int uSecTimeout);
    int waitForNewFrame();
    int sendCommand(Command command);

    LCG_CORE_DEPRECATED(int getServerVersion(int* serverVersion));  // Depracated
    LCG_CORE_DEPRECATED(int getNatNetVersion(int* natNetVersion));  // Depracated
    lcg::core::Version getServerVersion();
    lcg::core::Version getNatNetVersion();
    int getFrameNumber();
    double getLatency();
    void getTimecode(unsigned int& outTimecode, unsigned int& outTimecodeSubFrame);
    std::string getTimecodeString();
    double getTimestamp();
    bool isServerRecording();
    bool haveTrackedModelsChanged();
    std::string getServerAppName();
    Markerset getMarkersetFromName(std::string markersetName);
    Markerset getUnidentifiedMarkers(); // Convenient method for calling getMarkersetFromName("_Unidentified_")
    Markerset getLabeledMarkers();  // Convenient method for calling getMarkersetFromName("_Labeled_")
    RigidBody getRigidBodyFromName(std::string rigidBodyName);
    Skeleton getSkeletonFromName(std::string skeletonName);

    void disconnect();

private:
    ClientCore *pClientCore_;
};

} // namespace optitrack
} // namespace lcg

#endif // LCG_OPTITRACK_NATNETCLIENT_H
