// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Skeleton.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_OPTITRACK_SKELETON_H
#define LCG_OPTITRACK_SKELETON_H

#include <lcg/core/Core.h>              // lcg::core::Core
#include "lcg/optitrack/RigidBody.h"    // lcg::optitrack::RigidBody
#include <map>                          // std::map
#include <string>                       // std::string
#include <stdexcept>                    // std::range_error

namespace lcg{
namespace optitrack {

class Skeleton : public Dataset {
    std::runtime_error noBoneFromNameException(const std::string& boneName) const;
    std::runtime_error noBoneFromIDException(const int& boneID) const;
    static std::runtime_error throwPrintNullException();

    // These fields are mutable because they are used within const methods.
    mutable std::map<std::string, RigidBody> nameToBone;    // Stores the bones
    mutable std::map<int, RigidBody*> IDtoBone;             // map from ID to bones

    int nRigidBodies;

public:
    Skeleton();
    virtual ~Skeleton();

    virtual void print() const;
    virtual bool isEqual(const Core& other) const;

    int addBone(const RigidBody& newBone);
    RigidBody getBoneFromName(const std::string& boneName) const;
    RigidBody getBoneFromID(const int& boneID) const;
    void setNumberOfElements(int numberOfElements);
    virtual int getNumberOfElements() const;
    int clear();

    virtual bool operator==(const Core& other) const;
    virtual bool operator!=(const Core& other) const;
    friend std::ostream& operator<<(std::ostream& os, const Skeleton& skeleton);
    friend std::ostream& operator<<(std::ostream& os, const Skeleton* skeleton);

};

}   // optitrack namespace
}   // lcg namespace

#endif // LCG_OPTITRACK_SKELETON_H
