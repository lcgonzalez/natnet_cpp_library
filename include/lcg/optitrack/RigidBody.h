// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file RigidBody.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.2.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_OPTITRACK_RIGIDBODY_H
#define LCG_OPTITRACK_RIGIDBODY_H

#include "Markerset.h"  // lcg::optitrack::Position

#include <map>          // std::map
#include <stdexcept>    // std::runtime_error

namespace lcg{
namespace optitrack{

class RigidBody : public Markerset
{
    Orientation orientation_;
    Position position_;
    float fMeanMarkerError_;
    bool bTrackingValid_;   // Rigid body was successfully tracked in current frame
    int iParentID_;
    Position offset_;      // [X,Y,Z] Offset

    static std::runtime_error throwPrintNullException();

public:
    RigidBody();
    virtual ~RigidBody();

    virtual void print() const;
    virtual bool isEqual(const Core& other) const;

    void setPosition(const float& newX, const float& newY, const float& newZ);
    void setPosition(const Position& newPosition);
    Position getPosition() const;
    void setOrientation(const float& newQX, const float& newQY, const float& newQZ, const float& newQW);
    void setOrientation(const Orientation& newOrientation);
    Orientation getOrientation() const;

    void setMeanMarkerError(const float& newMeanMarkerError);
    float getMeanMarkerError() const;
    void setIsTrackingValid(const bool& newIsTrackingValid);
    bool isTrackingValid() const;
    void setParentID(const int& newParentID);
    int getParentID() const;
    void setOffset(Position newOffset);
    Position getOffset() const; // [X,Y,Z] convention

    int clear();

    virtual bool operator==(const Core& other) const;
    virtual bool operator!=(const Core& other) const;
    friend std::ostream& operator<<(std::ostream& os, const RigidBody& rigidBody);
    friend std::ostream& operator<<(std::ostream& os, const RigidBody* rigidBody);
};

std::ostream& operator<<(std::ostream& os, const RigidBody& rigidBody);
std::ostream& operator<<(std::ostream& os, const RigidBody* rigidBody);

} // namespace optitrack
} // namespace lcg

#endif // LCG_OPTITRACK_RIGIDBODY_H
