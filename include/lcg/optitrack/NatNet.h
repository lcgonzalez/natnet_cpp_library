// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file NatNet.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.2.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_OPTITRACK_NATNET_H
#define LCG_OPTITRACK_NATNET_H

namespace lcg{
namespace optitrack{

enum Command {MODEL, DATA, TEST, PING};

} // namespace optitrack
} // namespace lcg


#endif // LCG_OPTITRACK_NATNET_H
