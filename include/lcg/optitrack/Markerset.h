// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Markerset.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.2.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_OPTITRACK_MARKERSET_H
#define LCG_OPTITRACK_MARKERSET_H

#include "Dataset.h"    // lcg::optitrack::Marker

#include <map>          // std::map
#include <stdexcept>    // std::runtume_error

namespace lcg{
namespace optitrack{

class Markerset : public Dataset
{
    std::runtime_error noMarkerFromNameException(const std::string& markerName) const;
    std::runtime_error noMarkerFromIdException(const int& markerID) const;
    static std::runtime_error throwPrintNullException();

protected:
    mutable std::map<std::string, Marker> stringToMarker;   // Stores the markers
    mutable std::map<int, Marker*> IDtoMarker; // Stores references to the markers

public:
    Markerset();
    virtual ~Markerset();

    virtual void print() const;
    virtual bool isEqual(const Core& other) const;

    int addMarker(const Marker& newMarker);
    Marker getMarkerFromName(const std::string& markerName) const;
    Marker getMarkerFromID(const int& ID) const;
    virtual int getNumberOfElements() const;
    int clear();

    virtual bool operator==(const Core& other) const;
    virtual bool operator!=(const Core& other) const;
    friend std::ostream& operator<<(std::ostream& os, const Markerset& markerset);
    friend std::ostream& operator<<(std::ostream& os, const Markerset* markerset);
};

std::ostream& operator<<(std::ostream& os, const Markerset& markerset);
std::ostream& operator<<(std::ostream& os, const Markerset* markerset);

} // namespace optitrack
} // namespace lcg

#endif // LCG_OPTITRACK_MARKERSET_H
