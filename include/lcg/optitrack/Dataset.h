// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Dataset.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_OPTITRACK_DATASET_H
#define LCG_OPTITRACK_DATASET_H

#include <lcg/core/Core.h>  // lcg::core::Core

#include <stdexcept>        // std::runtime_error

namespace lcg{
namespace optitrack{

class Position : public core::Core {
    std::runtime_error throwGetPointerNullException() const;
    static std::runtime_error throwPrintNullException();
public:
    union{
        mutable float xyz[3];
        struct{
            float x, y, z; // This is float because the original data is float
        };
    };

    Position();
    Position(const float& x, const float& y, const float& z);
    virtual ~Position();

    virtual void print() const;
    virtual bool isEqual(const Core& other) const;

    void set(const float& newX, const float& newY, const float& newZ);
    void get(float* currentPosition) const; // [X,Y,Z] convention

    virtual bool operator==(const Core& other) const;
    virtual bool operator!=(const Core& other) const;
    friend std::ostream& operator<<(std::ostream& os, const Position& position);
    friend std::ostream& operator<<(std::ostream& os, const Position* position);
};

std::ostream& operator<<(std::ostream& os, const Position& position);
std::ostream& operator<<(std::ostream& os, const Position* position);

class Orientation : public core::Core {
    std::runtime_error throwGetPointerNullException() const;
    static std::runtime_error throwPrintNullException();
public:
    enum Notation {QUATERNIONS, EULER_XYZ};

    union{
        mutable float qXqYqZqW[3];
        struct {
            float qx,qy,qz,qw; // This is float because the original data is it too.
        };
    };

    Orientation();

    // Store quaternions (can be non-normalized)
    Orientation(const float& qX, const float& qY, const float& qZ, const float& qW);

    // Turn Euler (X-Y-Z) angles given in Radians into Quaternions to be stored.
    // Be carefull of singularities inherent of the Euler-to-Quaternions conversion.
    // The resulting Quaternions are normalized.
    Orientation(const float& eulerX, const float& eulerY, const float& eulerZ);
    ~Orientation();

    virtual void print() const;
    virtual bool isEqual(const Core& other) const;

    // Store quaternions (can be non-normalized)
    void set(const float& newQX, const float& newQY, const float& newQZ, const float& newQW);

    // Turn Euler (X-Y-Z) angles given in Radians into Quaternions to be stored.
    // Be carefull of singularities inherent of the Euler-to-Quaternions conversion.
    // The resulting Quaternions are normalized.
    void set(const float& newEulerX, const float& newEulerY, const float& newEulerZ);

    void setPrintNotation(Notation newPrintNotation);

    // If Notation==QUATERNIONS, will return the stored representation of rotation
    // in Quaternions, in [qx, qy, qz, qw].
    //
    // If Notation==EULER_XYZ, will return the Quaternions (normalized or non-normalized)
    // turned into Euler angles (Radians), from -pi(-180°) to pi(180°) to in Rx and Ry, and
    // from -pi/2(-90°) to pi/2(90°) in Rz.
    // The order of the rotation is Ry first, then Rz, and last Rx ([Ry][Rz])[Rx].
    // Singularity in Rz at +-pi/2(90°), at that point Rx is zero and Ry is used as the rotation.
    void get(Notation notation, float* currentOrientation) const; // [Rx,Ry,Rz] or [qx, qy, qz, qw]

    virtual bool operator==(const Core& other) const;
    virtual bool operator!=(const Core& other) const;
    friend std::ostream& operator<<(std::ostream& os, const Orientation& orientation);
    friend std::ostream& operator<<(std::ostream& os, const Orientation* orientation);
private:
    Notation printNotation;
public:
    ///////////// Helper static methods
    static double radiansToDegrees(const double& radians);
    static double degreesToRadians(const double& degrees);
};

std::ostream& operator<<(std::ostream& os, const Orientation& orientation);
std::ostream& operator<<(std::ostream& os, const Orientation* orientation);

class Marker : public core::Core {
public:
    // UNKNOWN -> The marker real-tome 3D position was reconstructed, but is unknown if it whas through point cloud or model.
    // OCCLUDED -> The marker position was not reconstructed because its 2D position is not visible from 2 or more cameras
    // POINT_CLOUD_SOLVED -> The marker real-time 3D position was reconstructed through triangulation.
    // MODEL_SOLVED -> The marker real-time 3D position was reconstructed through the model.
    enum Visibility {UNKNOWN, OCCLUDED, POINT_CLOUD_SOLVED, MODEL_SOLVED};

    Marker(const float x=0.0, const float y=0.0, const float z=0.0);
    virtual ~Marker();

    virtual void print() const;
    virtual bool isEqual(const Core& other) const;

    void setPosition(const float newX, const float newY, const float newZ);
    void setPosition(const Position& newPosition);
    Position getPosition() const;
    void setSize(const float newSize);
    float getSize() const;
    void setVisibility(Visibility newVisibility);
    Visibility getVisibility();

    virtual bool operator==(const Core& other) const;
    virtual bool operator!=(const Core& other) const;
    friend std::ostream& operator<<(std::ostream& os, const Marker& marker);
    friend std::ostream& operator<<(std::ostream& os, const Marker* marker);

protected:
    mutable Position position_;
    float size_;

    static std::runtime_error throwPrintNullException();

private:
    Visibility visibility_;
};

std::ostream& operator<<(std::ostream& os, const Marker& marker);
std::ostream& operator<<(std::ostream& os, const Marker* marker);

class Dataset : public core::Core {
public:
    // TODO: add virtual methods to map nameToDataset and IDtoDataset, and implement them in all Datasets
    Dataset();
    virtual ~Dataset();

    virtual bool isEqual(const Core& other) const;

    virtual int getNumberOfElements() const = 0;

    virtual bool operator==(const Core& other) const;
    virtual bool operator!=(const Core& other) const;
};

} // namespace optitrack
} // namespace lcg

#endif // LCG_OPTITRACK_DATASET_H
