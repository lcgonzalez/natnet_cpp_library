// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file MarkersetTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.2.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/Markerset.h"    // lcg::optitrack::Markerset

#include <string>           // std::string
#include <vector>           // std::vector
#include <gtest/gtest.h>

class Markerset_Test : public ::testing::Test {
public:
    lcg::optitrack::Marker expectedMarker1, expectedMarker2, expectedMarker3;
    lcg::optitrack::Marker testMarker1, testMarker2, testMarker3;
    std::vector<lcg::optitrack::Marker> expectedMarkers;

    const void testMarkersEqual(std::vector<lcg::optitrack::Marker> testMarkers){
        if(expectedMarkers.size() != testMarkers.size()){
            FAIL() << "The expectedMarkers.size() and testMarkers.size() are not equal!";
            return;
        }
        std::vector<lcg::optitrack::Marker>::iterator expectedMarkerIt = expectedMarkers.begin();
        for(std::vector<lcg::optitrack::Marker>::iterator markerIt = testMarkers.begin();
            markerIt < testMarkers.end(); markerIt++){
            lcg::optitrack::Marker expectedMarker = *expectedMarkerIt++;
            lcg::optitrack::Marker testMarker = *markerIt;
            ASSERT_STREQ(expectedMarker.getName().c_str(), testMarker.getName().c_str());
            ASSERT_EQ(expectedMarker.getID(), testMarker.getID());
            ASSERT_EQ(expectedMarker.getPosition(), testMarker.getPosition());
        }
    }

    void SetUp() {
        expectedMarker1.setID(3);
        expectedMarker2.setID(89);
        expectedMarker3.setID(107);
        expectedMarker1.setName("voltereta");
        expectedMarker2.setName("foo");
        expectedMarker3.setName("bar");
        expectedMarker1.setPosition(0.956108, 1.55893, 9.65595);
        expectedMarker2.setPosition(23.15417, -4.8132, -0.0117);
        expectedMarker3.setPosition(5.561371, -10.7789, 105.726);
        expectedMarkers.push_back(expectedMarker1);
        expectedMarkers.push_back(expectedMarker2);
        expectedMarkers.push_back(expectedMarker3);
    }
    void TearDown(){
        testMarker1.setID(-1);
        testMarker2.setID(-1);
        testMarker3.setID(-1);
        testMarker1.setName("no_name_set");
        testMarker2.setName("no_name_set");
        testMarker3.setName("no_name_set");
        testMarker1.setPosition(0.0, 0.0, 0.0);
        testMarker2.setPosition(0.0, 0.0, 0.0);
        testMarker3.setPosition(0.0, 0.0, 0.0);
        while(!expectedMarkers.empty()){
            expectedMarkers.pop_back();
        }
    }
};

TEST_F(Markerset_Test, CompareAsEqualAndNotEqual)
{
    lcg::optitrack::Markerset testMarkerset, expectedMarkerset;

    testMarkerset.addMarker(expectedMarker1);
    testMarkerset.addMarker(expectedMarker2);
    testMarkerset.addMarker(expectedMarker3);

    expectedMarkerset.addMarker(expectedMarker1);
    expectedMarkerset.addMarker(expectedMarker2);
    expectedMarkerset.addMarker(expectedMarker3);

    ASSERT_EQ(testMarkerset, expectedMarkerset);
    testMarkerset.setName("foo");
    ASSERT_NE(testMarkerset, expectedMarkerset);
}

TEST_F(Markerset_Test, AddAndGetMarkersFromName)
{
    lcg::optitrack::Markerset testMarkerset;

    testMarkerset.addMarker(expectedMarker1);
    testMarkerset.addMarker(expectedMarker2);
    testMarkerset.addMarker(expectedMarker3);

    testMarker1 = testMarkerset.getMarkerFromName(expectedMarker1.getName());
    testMarker2 = testMarkerset.getMarkerFromName(expectedMarker2.getName());
    testMarker3 = testMarkerset.getMarkerFromName(expectedMarker3.getName());
    std::vector<lcg::optitrack::Marker> testMarkers;
    testMarkers.push_back(testMarker1);
    testMarkers.push_back(testMarker2);
    testMarkers.push_back(testMarker3);

    Markerset_Test::testMarkersEqual(testMarkers);
}

TEST_F(Markerset_Test, GetMarkersFromName_ThrowException)
{
    lcg::optitrack::Markerset testMarkerset;

    EXPECT_THROW(testMarkerset.getMarkerFromName("foo_bar"), std::runtime_error);
}

TEST_F(Markerset_Test, AddAndGetMarkersFromID)
{
    lcg::optitrack::Markerset testMarkerset;

    testMarkerset.addMarker(expectedMarker1);
    testMarkerset.addMarker(expectedMarker2);
    testMarkerset.addMarker(expectedMarker3);

    testMarker1 = testMarkerset.getMarkerFromID(expectedMarker1.getID());
    testMarker2 = testMarkerset.getMarkerFromID(expectedMarker2.getID());
    testMarker3 = testMarkerset.getMarkerFromID(expectedMarker3.getID());
    std::vector<lcg::optitrack::Marker> testMarkers;
    testMarkers.push_back(testMarker1);
    testMarkers.push_back(testMarker2);
    testMarkers.push_back(testMarker3);

    Markerset_Test::testMarkersEqual(testMarkers);
}

TEST_F(Markerset_Test, GetMarkersFromID_ThrowException)
{
    lcg::optitrack::Markerset testMarkerset;

    EXPECT_THROW(testMarkerset.getMarkerFromID(343), std::runtime_error);
}
