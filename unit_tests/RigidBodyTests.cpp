// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file RigidBodyTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.2.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/RigidBody.h"    // lcg::optitrack::RigidBody

#include <gtest/gtest.h>

class RigidBody_Test : public ::testing::Test {
public:
    lcg::optitrack::RigidBody expectedRigidBody1;
    lcg::optitrack::RigidBody testRigidBody1;
    lcg::optitrack::Marker tmpMarker1, tmpMarker2, tmpMarker3;

    void SetUp() {
        expectedRigidBody1.setID(5);
        expectedRigidBody1.setName("fooBar");
        tmpMarker1.setPosition(0.956108, 1.55893, 9.65595);
        tmpMarker1.setName("marker1");
        tmpMarker1.setID(1);
        tmpMarker2.setPosition(23.15417, -4.8132, -0.0117);
        tmpMarker2.setName("marker2");
        tmpMarker2.setID(2);
        tmpMarker3.setPosition(5.561371, -10.7789, 105.726);
        tmpMarker3.setName("marker3");
        tmpMarker3.setID(3);
        expectedRigidBody1.addMarker(tmpMarker1);
        expectedRigidBody1.addMarker(tmpMarker2);
        expectedRigidBody1.addMarker(tmpMarker3);
        expectedRigidBody1.setOrientation(-0.0661533, 0.000187133, 0.161816, 0.984601);
        expectedRigidBody1.setPosition(0.956108, 1.55893, 9.65595);
    }
    void TearDown(){
        testRigidBody1.setID(-1);
        testRigidBody1.setName("no_name_set");
        testRigidBody1.clear();
        expectedRigidBody1.clear();
    }
};

TEST_F(RigidBody_Test, CompareAsEqualAndNotEqual)
{
    testRigidBody1.setID(5);
    testRigidBody1.setName("fooBar");
    testRigidBody1.addMarker(tmpMarker1);
    testRigidBody1.addMarker(tmpMarker2);
    testRigidBody1.addMarker(tmpMarker3);
    testRigidBody1.setOrientation(-0.0661533, 0.000187133, 0.161816, 0.984601);
    testRigidBody1.setPosition(0.956108, 1.55893, 9.65595);

    ASSERT_EQ(testRigidBody1, expectedRigidBody1);
    testRigidBody1.setOrientation(0.0, 0.0, 0.0, 0.0);
    testRigidBody1.setPosition(0.0, 0.0, 0.0);
    ASSERT_NE(testRigidBody1, expectedRigidBody1);
}

TEST_F(RigidBody_Test, AddAndGetOrientation_Quaternions)
{
    testRigidBody1.setOrientation(expectedRigidBody1.getOrientation());
    lcg::optitrack::Orientation testOrientation1 = testRigidBody1.getOrientation();
    lcg::optitrack::Orientation expectedOrientation1(-0.0661533, 0.000187133, 0.161816, 0.984601);

    // Test for set and get Orientation object
    ASSERT_EQ(testOrientation1, expectedOrientation1);

    testRigidBody1.setOrientation(1.317723585255719, 0.4091819711545585, -0.27139869868511823, 0.000187133);
    testOrientation1 = testRigidBody1.getOrientation();
    expectedOrientation1.set(1.317723585255719, 0.4091819711545585, -0.27139869868511823, 0.000187133);

    // Test for set raw orientation and get orientation object
    ASSERT_EQ(testOrientation1, expectedOrientation1);
}

TEST_F(RigidBody_Test, AddAndGetPosition)
{
    testRigidBody1.setPosition(expectedRigidBody1.getPosition());
    lcg::optitrack::Position testPosition1 = testRigidBody1.getPosition();
    lcg::optitrack::Position expectedPosition1(0.956108, 1.55893, 9.65595);

    // Test for set and get Position object
    ASSERT_EQ(testPosition1, expectedPosition1);

    testRigidBody1.setPosition(1.317723585255719, 0.4091819711545585, -0.27139869868511823);
    testPosition1 = testRigidBody1.getPosition();
    expectedPosition1.set(1.317723585255719, 0.4091819711545585, -0.27139869868511823);

    // Test for set raw position and get orientation object
    ASSERT_EQ(testPosition1, expectedPosition1);
}
