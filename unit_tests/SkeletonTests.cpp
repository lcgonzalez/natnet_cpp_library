
// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file SkeletonTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/Skeleton.h"     // lcg::optitrack::Skeleton

#include <gtest/gtest.h>

typedef lcg::optitrack::Skeleton lcgSkeleton;
typedef lcg::optitrack::RigidBody lcgBone;
class Skeleton_Test : public ::testing::Test {
public:
    lcgSkeleton expectedSkeleton1;
    lcgSkeleton testSkeleton1;
    lcgBone bone1, bone2, bone3;

    void SetUp() {
        bone1.setPosition(0.956108, 1.55893, 9.65595);
        bone1.setOrientation(-0.0661533, 0.000187133, 0.161816, 0.984601);
        bone1.setID(1);
        bone1.setName("bone1");

        bone2.setPosition(23.15417, -4.8132, -0.0117);
        bone2.setOrientation(-0.0884104,0.321941,0.147729,-0.930975);
        bone2.setID(2);
        bone2.setName("bone2");

        bone3.setPosition(5.561371, -10.7789, 105.726);
        bone3.setOrientation(-0.24222,0.205425,0.134714,-0.938607);
        bone3.setID(3);
        bone3.setName("bone3");

        expectedSkeleton1.setID(23);
        expectedSkeleton1.setName("piquin");
        expectedSkeleton1.setNumberOfElements(3);
        expectedSkeleton1.addBone(bone1);
        expectedSkeleton1.addBone(bone2);
        expectedSkeleton1.addBone(bone3);

    }

    void TearDown() {
        testSkeleton1.setID(-1);
        testSkeleton1.setName("no_name_set");
        testSkeleton1.clear();
    }
};

TEST_F(Skeleton_Test, compareAsEqualAndNoEqual) {
    testSkeleton1.setID(23);
    testSkeleton1.setName("piquin");
    testSkeleton1.setNumberOfElements(3);
    testSkeleton1.addBone(bone1);
    testSkeleton1.addBone(bone2);
    testSkeleton1.addBone(bone3);

    ASSERT_EQ(testSkeleton1, expectedSkeleton1);
    testSkeleton1.setName("fooBar");
    ASSERT_NE(testSkeleton1, expectedSkeleton1);
    testSkeleton1.setName("piquin");
    ASSERT_EQ(testSkeleton1, expectedSkeleton1);
    testSkeleton1.clear();
    lcgBone tmpBone1;
    tmpBone1.setPosition(0.69542,0.855176,1.68663);
    tmpBone1.setOrientation(0.0151454,0.331087,0.116965,-0.9362);
    tmpBone1.setID(89);
    tmpBone1.setName("tmpBone1");

    testSkeleton1.addBone(bone1);
    testSkeleton1.addBone(bone2);
    testSkeleton1.addBone(tmpBone1);
    ASSERT_NE(testSkeleton1, expectedSkeleton1);
    testSkeleton1.clear();
    testSkeleton1.addBone(bone1);
    testSkeleton1.addBone(bone2);
    testSkeleton1.addBone(bone3);
    ASSERT_EQ(testSkeleton1, expectedSkeleton1);
}
