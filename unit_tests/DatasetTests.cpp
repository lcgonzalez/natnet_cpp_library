// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file DatasetTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.2.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/Dataset.h"  // lcg::optitrack::Dataset

#include <string>           // std::string
#include <gtest/gtest.h>

class Position_Test : public ::testing::Test {
public:
    lcg::optitrack::Position testPosition1, testPosition2, testPosition3;

    void SetUp() {
        testPosition1.set(0.956108, 1.55893, 9.65595);
        testPosition2.set(0.956108, 1.55893, 9.65595);
        testPosition3.set(5.561371, -10.7789, 105.726);
    }
    void TearDown(){
    }
};

TEST_F(Position_Test, SetAndGetTest)
{
    float expectedPosition[3] = {0.956108, 1.55893, 9.65595};
    float actualPosition[3] = {0.0, 0.0, 0.0};

    testPosition3.set(expectedPosition[0], expectedPosition[1], expectedPosition[2]);
    testPosition3.get(actualPosition);

    ASSERT_FLOAT_EQ(expectedPosition[0], actualPosition[0]);
    ASSERT_FLOAT_EQ(expectedPosition[1], actualPosition[1]);
    ASSERT_FLOAT_EQ(expectedPosition[2], actualPosition[2]);
}

TEST_F(Position_Test, SetAndGetThrowException)
{
    float expectedPosition[3] = {0.956108, 1.55893, 9.65595};
    float* actualPosition = 0;

    testPosition3.set(expectedPosition[0], expectedPosition[1], expectedPosition[2]);

    EXPECT_THROW(testPosition3.get(actualPosition), std::runtime_error);
}

TEST_F(Position_Test, CompareAsEqualAndNotEqual)
{
    ASSERT_EQ(testPosition1, testPosition2);
    ASSERT_NE(testPosition1, testPosition3);
}

TEST_F(Position_Test, CopyAndCompareAsEqual)
{
    ASSERT_NE(testPosition1, testPosition3);
    testPosition1 = testPosition3;
    ASSERT_EQ(testPosition1, testPosition3);
}

class Orientation_Test : public ::testing::Test {
public:
    lcg::optitrack::Orientation testOrientation1, testOrientation2, testOrientation3;

    void SetUp() {
        testOrientation1.set(-0.0661533, 0.000187133, 0.161816, 0.984601);
        testOrientation2.set(0.956108, 1.55893, 9.65595, 0.984601);
        testOrientation3.set(5.561371, -0.7789, 0.5726, 0.11415);
    }
    void TearDown(){
    }
};

TEST_F(Orientation_Test, SetAndGetQUATERNIONS)
{
    float expectedOrientation[4] = {-0.0661533, 0.000187133, 0.161816, 0.984601};
    float actualOrientation[4] = {0.0, 0.0, 0.0, 0.0};

    testOrientation3.set(expectedOrientation[0], expectedOrientation[1], expectedOrientation[2], expectedOrientation[3]);
    testOrientation3.get(lcg::optitrack::Orientation::QUATERNIONS, actualOrientation);

    ASSERT_FLOAT_EQ(expectedOrientation[0], actualOrientation[0]);
    ASSERT_FLOAT_EQ(expectedOrientation[1], actualOrientation[1]);
    ASSERT_FLOAT_EQ(expectedOrientation[2], actualOrientation[2]);
    ASSERT_FLOAT_EQ(expectedOrientation[3], actualOrientation[3]);
}

TEST_F(Orientation_Test, SetAndGetEULER_XYZ)
{
    float expectedOrientation[3] = {1.317723585255719, 0.4091819711545585, -0.27139869868511823};
    float actualOrientation[3] = {0.0, 0.0, 0.0};

    testOrientation3.set(expectedOrientation[0], expectedOrientation[1], expectedOrientation[2]);
    testOrientation3.get(lcg::optitrack::Orientation::EULER_XYZ, actualOrientation);

    ASSERT_FLOAT_EQ(expectedOrientation[0], actualOrientation[0]);
    ASSERT_FLOAT_EQ(expectedOrientation[1], actualOrientation[1]);
    ASSERT_FLOAT_EQ(expectedOrientation[2], actualOrientation[2]);
}

//TEST_F(Orientation_Test, SetAndGetEULER_XYZ_Singularities)
//{
//    // Singularity at pi/2 (90°)
//    float inputOrientation[3] = {0.7214548, 0.4091819711545585, M_PI/2.0};
//    float expectedOrientation[3] = {0.0, 1.1306367, M_PI/2.0};
//    float actualOrientation[3] = {0.0, 0.0, 0.0};

//    testOrientation3.set(inputOrientation[0], inputOrientation[1], inputOrientation[2]);
//    testOrientation3.get(lcg::optitrack::Orientation::EULER_XYZ, actualOrientation);

//    ASSERT_FLOAT_EQ(expectedOrientation[0], actualOrientation[0]);
//    ASSERT_FLOAT_EQ(expectedOrientation[1], actualOrientation[1]);
//    ASSERT_FLOAT_EQ(expectedOrientation[2], actualOrientation[2]);

//    // Test before singularity at pi/2 (90°)
//    float inputOrientation1[3] = {7.214372158e-01, 0.4091819711545585, M_PI/2.001};
//    float expectedOrientation1[3] = {7.214536071e-01, 4.091655314e-01, M_PI/2.001}; // This is different from the input due to the input
//    float actualOrientation1[3] = {0.0, 0.0, 0.0};

//    testOrientation3.set(inputOrientation1[0], inputOrientation1[1], inputOrientation1[2]);
//    testOrientation3.get(lcg::optitrack::Orientation::EULER_XYZ, actualOrientation1);

//    ASSERT_FLOAT_EQ(expectedOrientation1[0], actualOrientation1[0]);
//    ASSERT_FLOAT_EQ(expectedOrientation1[1], actualOrientation1[1]);
//    ASSERT_FLOAT_EQ(expectedOrientation1[2], actualOrientation1[2]);


//    // Test after singularity at pi/2 (90°)
//    float inputOrientation2[3] = {0.785375, 0, M_PI/1.99};
//    float expectedOrientation2[4] = {1.900915653e-01, 3.455856740e-01, 9.161298275e-01, -7.170705497e-02}; // This is different from input due to the precision
//    float actualOrientation2[3] = {0.0, 0.0, 0.0};

//    testOrientation3.set(inputOrientation2[0], inputOrientation2[1], inputOrientation2[2]);
//    testOrientation3.get(lcg::optitrack::Orientation::EULER_XYZ, actualOrientation2);

//    //std::streamsize lastPrecision = cout.precision();
//    //std::cout.precision(lcg::numeric_limits<float>::max_digits10);
//    //std::cout << std::scientific;
//    for(int i=0; i<3; i++){
//        //std::cout << "expectedOrientationa2[" << i << "]: " << expectedOrientation[i] << std::endl
//        std::cout  << "actualOrientation2[" << i << "]: " << actualOrientation2[i]*180/M_PI << std::endl;
//    }
//    //cout.unsetf(std::ios_base::floatfield);
//    //cout.precision(lastPrecision);

//    ASSERT_FLOAT_EQ(expectedOrientation2[0], actualOrientation2[0]);
//    ASSERT_FLOAT_EQ(expectedOrientation2[1], actualOrientation2[1]);
//    ASSERT_FLOAT_EQ(expectedOrientation2[2], actualOrientation2[2]);
//}

TEST_F(Orientation_Test, SetQUATERNIONS_AndGetEULER_XYZ)
{
    float inputOrientation[4] = {-0.0661533, 0.000187133, 0.161816, 0.984601};
    float expectedOrientation[3] = {-0.13793283606455550294, 0.022977290301404548062, 0.3242770853637904072};
    float actualOrientation[3] = {0.0, 0.0, 0.0};

    testOrientation3.set(inputOrientation[0], inputOrientation[1], inputOrientation[2], inputOrientation[3]);
    testOrientation3.get(lcg::optitrack::Orientation::EULER_XYZ, actualOrientation);

    ASSERT_FLOAT_EQ(expectedOrientation[0], actualOrientation[0]);
    ASSERT_FLOAT_EQ(expectedOrientation[1], actualOrientation[1]);
    ASSERT_FLOAT_EQ(expectedOrientation[2], actualOrientation[2]);
}

TEST_F(Orientation_Test, SetEULER_XYZ_AndGetQUATERNIONS)
{
    float inputOrientation[3] = {-0.13793283606455550294, 0.022977290301404548062, 0.3242770853637904072};
    float expectedOrientation[4] = {-0.0661533, 0.00018713539, 0.161816, 0.984601};
    float actualOrientation[4] = {0.0, 0.0, 0.0, 0.0};

    testOrientation3.set(inputOrientation[0], inputOrientation[1], inputOrientation[2]);
    testOrientation3.get(lcg::optitrack::Orientation::QUATERNIONS, actualOrientation);

    ASSERT_FLOAT_EQ(expectedOrientation[0], actualOrientation[0]);
    ASSERT_FLOAT_EQ(expectedOrientation[1], actualOrientation[1]);
    ASSERT_FLOAT_EQ(expectedOrientation[2], actualOrientation[2]);
}

class Marker_Test : public ::testing::Test {
public:
//    lcg::optitrack::Position expectedMarker1, expectedMarker2, expectedMarker3;
    lcg::optitrack::Marker testMarker1, testMarker2, testMarker3;

    void SetUp() {
        testMarker1.setName("foo");
        testMarker2.setName("foo");
        testMarker3.setName("bar");
        testMarker1.setID(34);
        testMarker2.setID(34);
        testMarker3.setID(9);
        testMarker1.setSize(0.024);
        testMarker2.setSize(0.024);
        testMarker3.setSize(0.054);
        testMarker1.setPosition(0.956108, 1.55893, 9.65595);
        testMarker2.setPosition(0.956108, 1.55893, 9.65595);
        testMarker3.setPosition(5.561371, -10.7789, 105.726);
    }
    void TearDown(){
    }
};

TEST_F(Marker_Test, CompareAsEqualAndNotEqual)
{
    ASSERT_EQ(testMarker1, testMarker2);
    ASSERT_NE(testMarker1, testMarker3);
}

TEST_F(Marker_Test, CopyAndCompareAsEqual)
{
    ASSERT_NE(testMarker1, testMarker3);
    testMarker1 = testMarker3;
    ASSERT_EQ(testMarker1, testMarker3);
}
