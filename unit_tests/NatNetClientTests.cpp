// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file NatNetClientTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.3
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/NatNetClient.h" // lcg::optitrack::NatNetClient
#include "lcg/optitrack/ClientCore.h"   // lcg::optitrack::ClientCore

#include <lcg/core/UdpSocket.h> // lcg::core::UdpSocket
#include <lcg/core/Thread.h>    // lcg::core::MutexLocker, lcg::core::Mutex
#include <lcg/core/System.h>    // lcg::core::System::getExeDir()
#include <lcg/core/debug.h>     // LCGDEBUG() macro

#include <fstream>  // std::ifstream
#include <cstring>  // std::memcpy
#include <string>   // std::string
#include <vector>   // std::vector
#include <utility>  // std::pair
#include <gtest/gtest.h>
#include <gmock/gmock.h>

#if(1)  // For commeinting all these tests, thus performing other classes' tests faster
std::string exeDir = lcg::core::System::getExeDir();    // Make paths absolute to avoid relative paths problems

#define NATNET_TESTING_MAX_MESSAGE 9 // Max number of messages types (commands)

// Useful typedef
typedef lcg::core::MutexLocker mutex_locker;
typedef lcg::core::UdpSocket lcgUdpSocket;
typedef lcg::core::Mutex lcgMutex;
typedef lcg::core::Thread lcgThread;
typedef lcg::core::Version lcgVersion;

typedef lcg::optitrack::Position lcgPosition;
typedef lcg::optitrack::Skeleton lcgSkeleton;
typedef lcg::optitrack::RigidBody lcgRigidBody;
typedef lcg::optitrack::Markerset lcgMarkerset;
typedef lcg::optitrack::Marker lcgMarker;

class NatNetClient_initialize_Test : public ::testing::Test{
public:

    class NatNetMockServer {
        typedef std::pair<std::string,int> path_type;
        std::vector<path_type> dataPaths;
        // array of pointers to original data to use
        char* dataFromCommand[NATNET_TESTING_MAX_MESSAGE];

        // Executed on the calling thread, before the thread is created and executed
        int prepareDataPaths(){
            mutex_locker ml(&mutex_);
            dataPaths.push_back(path_type(exeDir + "../motive_responses/requests/requestPingData.bin", NAT_PING)); // Ping Data request to compare
            dataPaths.push_back(path_type(exeDir + "../motive_responses/data/pingDataToSend.bin", NAT_PINGRESPONSE)); // Ping Data response to send
            dataPaths.push_back(path_type(exeDir + "../motive_responses/requests/requestDataModel.bin", NAT_REQUEST_MODELDEF)); // Model Definition Data request to compare
            dataPaths.push_back(path_type(exeDir + "../motive_responses/model/skeletonAndRigidBody_model.bin", NAT_MODELDEF)); // Model Definition Data response to send
            dataPaths.push_back(path_type(exeDir + "../motive_responses/data/skeletonAndRigidBody_data.bin", NAT_FRAMEOFDATA)); // Frame of Data response to send
            return 0;
        }

        void setFailure_EmptyDataToCommandPath() {
            FAIL() << "Empty path!" << std::endl
                   << "The path that leads to the files containing the data "
                   << "used to check for data integrity in the incoming packets  "
                   << "of the serverCommandReadingThread, is empty";
        }
        void setFailure_CommandDataFileNotOpen(std::string path) {
            FAIL() << "The command data file: " << path << "couldn't be opened." << std::endl
                   << "This error ocured on the serverCommandReadingThread.";
        }
        void setFailure_DataFileSizeError(std::string path) {
            FAIL() << "Data File: " << path << " size is negative." << std::endl;
        }
        void setFailure_CommandDataFileUnknownCommand(int unknownCommand) {
            FAIL() << "The command: " << unknownCommand << " is not known." << std::endl
                   << "This error ocured on the serverCommandReadingThread.";
        }

        void initDataArray(){
            mutex_locker ml(&mutex_);
            for(int i=0; i<NATNET_TESTING_MAX_MESSAGE; i++){
                dataFromCommand[i] = 0;
            }
        }
        void deleteDataArray(){
            mutex_locker ml(&mutex_);
            for(int i=0; i<NATNET_TESTING_MAX_MESSAGE; i++){
                if(dataFromCommand[i]){
                    delete[] dataFromCommand[i];
                }
            }
        }

        int readDataToCompare(std::vector<path_type> paths){
            if(paths.empty()) {
                setFailure_EmptyDataToCommandPath();
                return -1;
            }
            std::ifstream dataFile;
            for(std::vector<path_type>::iterator it = paths.begin();
                it != paths.end(); it++) {
                std::string path = it->first;
                int commandType = it->second;
                dataFile.open (path.c_str(), std::ios::in | std::ios::binary);
                if(!dataFile.is_open()){
                    setFailure_CommandDataFileNotOpen(path);
                    return -1;
                }
                // Get file size
                dataFile.seekg(0, std::ios::end);
                std::streamsize dataSize = dataFile.tellg();
                dataFile.seekg(0, std::ios::beg);
                if(dataSize < 0){
                    setFailure_DataFileSizeError(path);
                    return -1;
                }

                char* data = new char[dataSize];
                dataFile.read(data, dataSize);
                switch (commandType) {  // Check if the commandType is known
                case NAT_PING:
                case NAT_PINGRESPONSE:
                case NAT_REQUEST_MODELDEF:
                case NAT_MODELDEF:
                case NAT_FRAMEOFDATA:
                    break;
                default:
                    setFailure_CommandDataFileUnknownCommand(commandType);
                    return -1;
                    break;
                }
                mutex_locker ml(&mutex_);
                dataFromCommand[commandType] = data; // Store the data on the array
                dataFile.close();
            }
            return 0;
        }

        int getDataSize(char *message){
            int nBytes = 0;
            char *ptr = message; ptr +=2; // Message ID (command)
            std::memcpy(&nBytes, ptr, 2);
            return nBytes+4; // The +4 is the +2 of the Message ID and +2 of the Payload size
        }

        class CommandReadingAndReplyThread : public lcgThread {
            int getCommandType(char *message){
                char* ptr = message;
                // message ID
                int MessageID = 0;
                std::memcpy(&MessageID, ptr, 2);
                switch (MessageID){ // Check if the commandType is known
                case NAT_PING:
                case NAT_PINGRESPONSE:
                case NAT_REQUEST_MODELDEF:
                case NAT_MODELDEF:
                case NAT_FRAMEOFDATA:
                    return MessageID;
                    break;
                default:
                    setFailure_GetCommandTypeUnknownCommand(MessageID);
                    return -1;
                    break;
                }
            }

            void setFailure_UnknownCommandReceived(){
                FAIL() << "Unknown command received in the serverCommandReadingThread. "
                       << "Please check that the Command Data is sent correctly from "
                       << "the client";
            }
            void setFailure_GetCommandTypeUnknownCommand(int unknownCommand) {
                FAIL() << "The command: " << unknownCommand << "is not known." << std::endl
                       << "This error ocured on the serverCommandReadingThread.";
            }
            void setFailure_CommandDataReceivedIncorrectly() {
                FAIL() << "Command data received incorrectly" << std::endl
                       << "Requested Ping Data is different from Actual Ping Data. "
                       << "Please check that the Ping Data is sent correctly, and that "
                       << "the requestPingData.bin file is being read correctly.";
            }

            int checkForDataIntegrity(char* message) {
                int command = getCommandType(message);
                if(command < 0) { // Error
                    setFailure_UnknownCommandReceived();
                    return -1;
                }
                mutex_locker ml(&(natNetServer_->mutex_));
                char* expectedData = natNetServer_->dataFromCommand[command];
                int dataSize = natNetServer_->getDataSize(expectedData);
                for(int i=0; i<dataSize; i++){
                    if(expectedData[i] != message[i]){
                        setFailure_CommandDataReceivedIncorrectly();
                        return -1;
                    }
                }
                return 0;
            }

            int replyToMessage(char *message, std::string address, int port){
                mutex_locker ml(&(natNetServer_->mutex_));
                int command = getCommandType(message);
                char* response = natNetServer_->dataFromCommand[command+1]; // The response macro always is +1
                int size = natNetServer_->getDataSize(response);
                return natNetServer_->commandSocket.writeDatagram(response, size, address, port);
            }

            virtual void run(){
                lcg::optitrack::sPacket commandPakcetIn;
                std::string theirAddress;
                int theirPort;
                int retValue=0;

                do{
                    retValue = natNetServer_->commandSocket.readDatagram((char *)&commandPakcetIn, sizeof(lcg::optitrack::sPacket), theirAddress, &theirPort);
                    if(retValue < 0) {
                        continue;
                    }

                    char *message = (char *)&commandPakcetIn;
                    if( checkForDataIntegrity(message) ){
                        exit();
                        return;
                    }
                    replyToMessage(message, theirAddress, theirPort);
                }while(!exitThread_);
            }

        protected:
            NatNetMockServer* natNetServer_;
            volatile bool exitThread_;
        public:
            explicit CommandReadingAndReplyThread() : exitThread_(false){
            }

            explicit CommandReadingAndReplyThread(NatNetMockServer* e) : exitThread_(false){
                natNetServer_ = e;
            }

            ~CommandReadingAndReplyThread(){
                exit();
                wait();
            }

            void setNatNetServer(NatNetMockServer* e){
                natNetServer_ = e;
            }

            void exit(){
                exitThread_ = true;
            }
            int start(){
                exitThread_ = false;
                return lcgThread::start();
            }
        };

        class DataStreamingThread : public lcgThread {
            virtual void run(){
                natNetServer_->mutex_.lock();
                char* response = natNetServer_->dataFromCommand[NAT_FRAMEOFDATA];
                natNetServer_->mutex_.unlock();
                int size = natNetServer_->getDataSize(response);
                do{     // Send streaming data continuosly
                    natNetServer_->dataSocket.writeDatagram(response, size, natNetServer_->dataStreamingAddress_, natNetServer_->dataStreamingPort_);
                    LCGDEBUG() << "sending data" << std::endl;
                }while(!exitThread_);
            }

        protected:
            NatNetMockServer* natNetServer_;
            volatile bool exitThread_;
        public:
            explicit DataStreamingThread() : exitThread_(false){
            }

            explicit DataStreamingThread(NatNetMockServer* e) : exitThread_(false){
                natNetServer_ = e;
            }

            ~DataStreamingThread(){
                exit();
                wait();
            }

            void setNatNetServer(NatNetMockServer* e){
                natNetServer_ = e;
            }

            void exit(){
                exitThread_ = true;
            }
            int start(){
                exitThread_ = false;
                return lcgThread::start();
            }
        };

        int setUp(){
            dataSocket.joinMulticastGroup(dataStreamingAddress_, serverAddress_, 15111);
            commandSocket.bind(clientAddress_, commandReadingPort_, lcgUdpSocket::ShareAddress);
            commandSocket.setReadingTimeout(5,0);

            if(readDataToCompare(dataPaths) < 0 ){
                return -1;
            }
            startCommandThread();
            //startDataThread();
        }

        void tearDown(){
            stopCommandThread();
            stopDataThread();
            commandSocket.close();
            dataSocket.close();
        }

    public:
        NatNetMockServer(){
            initDataArray();
            prepareDataPaths();
        }
        ~NatNetMockServer(){
            deleteDataArray();
        }

        int start(std::string serverAddress, std::string clientAddress,
                   int commandReadingPort, std::string dataStreamingAddress,
                   int dataStreamingPort){
            serverAddress_ = serverAddress;
            clientAddress_ = clientAddress;
            commandReadingPort_ = commandReadingPort;
            dataStreamingAddress_ = dataStreamingAddress;
            dataStreamingPort_ = dataStreamingPort;

            setUp();
            return 0;
        }

        void stop(){
            tearDown();
        }

        void startCommandThread(){
            LCGDEBUG() << "CommandReadingAndReplyThread start()" << std::endl;
            CommandReadAndReplyThread_.setNatNetServer(this);
            CommandReadAndReplyThread_.start();
        }

        void startDataThread(){
            LCGDEBUG() << "DataStreamingThread start()" << std::endl;
            DataStreamingThread_.setNatNetServer(this);
            DataStreamingThread_.start();
        }

        void stopCommandThread(){
            CommandReadAndReplyThread_.exit();
            CommandReadAndReplyThread_.wait();
        }

        void stopDataThread(){
            DataStreamingThread_.exit();
            DataStreamingThread_.wait();
        }

    private:
        CommandReadingAndReplyThread CommandReadAndReplyThread_;
        DataStreamingThread DataStreamingThread_;
        lcgUdpSocket commandSocket;
        lcgUdpSocket dataSocket;
        lcgMutex mutex_;   // Mutex lock all methods that access fields of this class from within a thread
        std::string dataStreamingAddress_;
        std::string clientAddress_;
        std::string serverAddress_;
        int commandReadingPort_;
        int dataStreamingPort_;
    };

    lcg::optitrack::NatNetClient natNetClient;

    lcgMarkerset expectedVaritaMarkerset;
    lcgMarkerset expectedCharlieMarkerset;
    lcgMarkerset expectedAllMarkerset;
    lcgMarkerset expectedUnidentifiedMarkerset;
    lcgMarkerset expectedLabeledMarkerset;
    lcgRigidBody expectedVaritaRigidBody;
    lcgSkeleton expectedCharlieSkeleton;

    int expectedServerVersion[4];
    int expectedNatNetVersion[4];
    lcgVersion vExpectedServerVersion, vExpectedServerVersionFail;
    lcgVersion vExpectedNatNetVersion, vExpectedNatNetVersionFail;
    int expectedFrameNumber;
    std::string expectedServerAppName;
    int expectedConnectionStatus;
    double expectedLatency;
    unsigned int expectedTimecode, expectedTimecodeSubFrame;
    std::string expectedTimecodeStr;
    double expectedTimestamp;
    bool expectedIsServerRecording;
    bool expectedHaveTrackedModelsChanged;

    NatNetMockServer natNetServer_;
    std::string dataStreamingAddress_;
    std::string clientAddress_;
    std::string serverAddress_;
    int commandReadingPort_;
    int dataStreamingPort_;

    void clearDatasets(){
        expectedVaritaMarkerset.clear();
        expectedCharlieMarkerset.clear();
        expectedAllMarkerset.clear();
        expectedUnidentifiedMarkerset.clear();
        expectedLabeledMarkerset.clear();
        expectedVaritaRigidBody.clear();
        expectedCharlieSkeleton.clear();
    }

    void fillVaritaMarkerset(){
        expectedVaritaMarkerset.setID(0);
        expectedVaritaMarkerset.setName("varita");
        lcg::optitrack::Marker marker0, marker1, marker2;
        // Marker position set at full float precision
        marker0.setID(0); marker0.setName("1");
        marker0.setPosition(1.407239735e-01, 1.952854633e+00, 1.307195067e+00);
        marker1.setID(1); marker1.setName("2");
        marker1.setPosition(-1.657311805e-02, 1.972101688e+00, 1.781518459e+00);
        marker2.setID(2); marker2.setName("3");
        marker2.setPosition(3.865728155e-02, 1.965723395e+00, 1.614876866e+00);
        expectedVaritaMarkerset.addMarker(marker0);
        expectedVaritaMarkerset.addMarker(marker1);
        expectedVaritaMarkerset.addMarker(marker2);
    }

    void fillCharlieMarkerset(){
        expectedCharlieMarkerset.setID(1);
        expectedCharlieMarkerset.setName("charly");
        lcg::optitrack::Marker marker0, marker1, marker2, marker3, marker4,
            marker5, marker6, marker7, marker8, marker9, marker10, marker11,
            marker12, marker13, marker14, marker15, marker16, marker17, marker18,
            marker19, marker20, marker21, marker22, marker23, marker24, marker25,
            marker26, marker27, marker28, marker29, marker30, marker31, marker32,
            marker33, marker34, marker35, marker36;
        // Marker position set at full float precision
        marker0.setID(0); marker0.setName("charly_Hip_1");
        marker0.setPosition(7.980908155e-01, 8.976786137e-01, 1.922066927e+00);
        marker1.setID(1); marker1.setName("charly_Hip_2");
        marker1.setPosition(6.005586386e-01, 9.538902640e-01, 1.750321269e+00);
        marker2.setID(2); marker2.setName("charly_Hip_3");
        marker2.setPosition(9.307594299e-01, 9.059755206e-01, 1.762212634e+00);
        marker3.setID(3); marker3.setName("charly_Hip_4");
        marker3.setPosition(7.274857163e-01, 9.561080933e-01, 1.596688032e+00);
        marker4.setID(4); marker4.setName("charly_Chest_1");
        marker4.setPosition(7.322196364e-01, 1.208971024e+00, 1.942528129e+00);
        marker5.setID(5); marker5.setName("charly_Chest_2");
        marker5.setPosition(9.106026888e-01, 1.307656765e+00, 1.763132930e+00);
        marker6.setID(6); marker6.setName("charly_Chest_3");
        marker6.setPosition(9.504148960e-01, 1.170968056e+00, 1.770930052e+00);
        marker7.setID(7); marker7.setName("charly_Chest_4");
        marker7.setPosition(8.215170503e-01, 1.210551620e+00, 1.672095776e+00);
        marker8.setID(8); marker8.setName("charly_Head_1");
        marker8.setPosition(8.992505670e-01, 1.602382421e+00, 1.996591806e+00);
        marker9.setID(9); marker9.setName("charly_Head_2");
        marker9.setPosition(8.086966872e-01, 1.519860983e+00, 2.084273100e+00);
        marker10.setID(10); marker10.setName("charly_Head_3");
        marker10.setPosition(7.989701033e-01, 1.558929086e+00, 1.940698147e+00);
        marker11.setID(11); marker11.setName("charly_LShoulder_1");
        marker11.setPosition(9.608798027e-01, 1.305301666e+00, 1.976194978e+00);
        marker12.setID(12); marker12.setName("charly_LShoulder_2");
        marker12.setPosition(1.013676763e+00, 1.252120256e+00, 1.865857124e+00);
        marker13.setID(13); marker13.setName("charly_LUArm_1");
        marker13.setPosition(1.036298990e+00, 9.601079226e-01, 1.965343356e+00);
        marker14.setID(14); marker14.setName("charly_LUArm_2");
        marker14.setPosition(1.041159630e+00, 1.033736587e+00, 1.908152699e+00);
        marker15.setID(15); marker15.setName("charly_LHand_1");
        marker15.setPosition(9.655301571e-01, 6.745933890e-01, 2.016285896e+00);
        marker16.setID(16); marker16.setName("charly_LHand_2");
        marker16.setPosition(9.067157507e-01, 7.477946281e-01, 2.023015499e+00);
        marker17.setID(17); marker17.setName("charly_LHand_3");
        marker17.setPosition(9.820458293e-01, 7.251198292e-01, 1.981912374e+00);
        marker18.setID(18); marker18.setName("charly_RShoulder_1");
        marker18.setPosition(7.513126731e-01, 1.420000315e+00, 1.802410245e+00);
        marker19.setID(19); marker19.setName("charly_RShoulder_2");
        marker19.setPosition(7.800209522e-01, 1.341635227e+00, 1.677561760e+00);
        marker20.setID(20); marker20.setName("charly_RUArm_1");
        marker20.setPosition(5.653971434e-01, 1.276031256e+00, 1.588958859e+00);
        marker21.setID(21); marker21.setName("charly_RUArm_2");
        marker21.setPosition(6.199302077e-01, 1.266115427e+00, 1.615455747e+00);
        marker22.setID(22); marker22.setName("charly_RHand_1");
        marker22.setPosition(2.433027774e-01, 1.340782285e+00, 1.577534676e+00);
        marker23.setID(23); marker23.setName("charly_RHand_2");
        marker23.setPosition(3.032289743e-01, 1.301971078e+00, 1.588537335e+00);
        marker24.setID(24); marker24.setName("charly_RHand_3");
        marker24.setPosition(3.284814060e-01, 1.371426225e+00, 1.613251090e+00);
        marker25.setID(25); marker25.setName("charly_LThigh_1");
        marker25.setPosition(7.605606914e-01, 5.126234293e-01, 1.878883123e+00);
        marker26.setID(26); marker26.setName("charly_LThigh_2");
        marker26.setPosition(8.545972109e-01, 4.413289428e-01, 1.849075913e+00);
        marker27.setID(27); marker27.setName("charly_LShin_1");
        marker27.setPosition(8.623179197e-01, 7.040882111e-02, 1.724575520e+00);
        marker28.setID(28); marker28.setName("charly_LShin_2");
        marker28.setPosition(8.072131872e-01, 2.159572840e-01, 1.806602955e+00);
        marker29.setID(29); marker29.setName("charly_LFoot_1");
        marker29.setPosition(7.502084970e-01, 4.062155634e-02, 1.822031856e+00);
        marker30.setID(30); marker30.setName("charly_LFoot_2");
        marker30.setPosition(8.501413465e-01, 4.042096063e-02, 1.834267259e+00);
        marker31.setID(31); marker31.setName("charly_RThigh_1");
        marker31.setPosition(5.768959522e-01, 5.398091078e-01, 1.744862437e+00);
        marker32.setID(32); marker32.setName("charly_RThigh_2");
        marker32.setPosition(5.646212101e-01, 4.874913394e-01, 1.633973360e+00);
        marker33.setID(33); marker33.setName("charly_RShin_1");
        marker33.setPosition(6.195909977e-01, 1.263073981e-01, 1.535456538e+00);
        marker34.setID(34); marker34.setName("charly_RShin_2");
        marker34.setPosition(5.740082264e-01, 3.339127004e-01, 1.660454273e+00);
        marker35.setID(35); marker35.setName("charly_RFoot_1");
        marker35.setPosition(5.573503971e-01, 4.789064825e-02, 1.677532911e+00);
        marker36.setID(36); marker36.setName("charly_RFoot_2");
        marker36.setPosition(5.171389580e-01, 4.410387203e-02, 1.596749425e+00);
        expectedCharlieMarkerset.addMarker(marker0);
        expectedCharlieMarkerset.addMarker(marker1);
        expectedCharlieMarkerset.addMarker(marker2);
        expectedCharlieMarkerset.addMarker(marker3);
        expectedCharlieMarkerset.addMarker(marker4);
        expectedCharlieMarkerset.addMarker(marker5);
        expectedCharlieMarkerset.addMarker(marker6);
        expectedCharlieMarkerset.addMarker(marker7);
        expectedCharlieMarkerset.addMarker(marker8);
        expectedCharlieMarkerset.addMarker(marker9);
        expectedCharlieMarkerset.addMarker(marker10);
        expectedCharlieMarkerset.addMarker(marker11);
        expectedCharlieMarkerset.addMarker(marker12);
        expectedCharlieMarkerset.addMarker(marker13);
        expectedCharlieMarkerset.addMarker(marker14);
        expectedCharlieMarkerset.addMarker(marker15);
        expectedCharlieMarkerset.addMarker(marker16);
        expectedCharlieMarkerset.addMarker(marker17);
        expectedCharlieMarkerset.addMarker(marker18);
        expectedCharlieMarkerset.addMarker(marker19);
        expectedCharlieMarkerset.addMarker(marker20);
        expectedCharlieMarkerset.addMarker(marker21);
        expectedCharlieMarkerset.addMarker(marker22);
        expectedCharlieMarkerset.addMarker(marker23);
        expectedCharlieMarkerset.addMarker(marker24);
        expectedCharlieMarkerset.addMarker(marker25);
        expectedCharlieMarkerset.addMarker(marker26);
        expectedCharlieMarkerset.addMarker(marker27);
        expectedCharlieMarkerset.addMarker(marker28);
        expectedCharlieMarkerset.addMarker(marker29);
        expectedCharlieMarkerset.addMarker(marker30);
        expectedCharlieMarkerset.addMarker(marker31);
        expectedCharlieMarkerset.addMarker(marker32);
        expectedCharlieMarkerset.addMarker(marker33);
        expectedCharlieMarkerset.addMarker(marker34);
        expectedCharlieMarkerset.addMarker(marker35);
        expectedCharlieMarkerset.addMarker(marker36);
    }

    void fillAllMarkerset(){
        expectedAllMarkerset.setID(2);
        expectedAllMarkerset.setName("all");
        lcg::optitrack::Marker marker0, marker1, marker2, marker3, marker4,
            marker5, marker6, marker7, marker8, marker9, marker10, marker11,
            marker12, marker13, marker14, marker15, marker16, marker17, marker18,
            marker19, marker20, marker21, marker22, marker23, marker24, marker25,
            marker26, marker27, marker28, marker29, marker30, marker31, marker32,
            marker33, marker34, marker35, marker36, marker37, marker38, marker39;
        // Marker position set at full float precision
        marker0.setID(0); marker0.setName("varita_1");
        marker0.setPosition(1.407239735e-01, 1.952854633e+00, 1.307195067e+00);
        marker1.setID(1); marker1.setName("varita_2");
        marker1.setPosition(-1.657311805e-02, 1.972101688e+00, 1.781518459e+00);
        marker2.setID(2); marker2.setName("varita_3");
        marker2.setPosition(3.865728155e-02, 1.965723395e+00, 1.614876866e+00);
        marker3.setID(3); marker3.setName("charly_Hip_1");
        marker3.setPosition(7.980908155e-01, 8.976786137e-01, 1.922066927e+00);
        marker4.setID(4); marker4.setName("charly_Hip_2");
        marker4.setPosition(6.005586386e-01, 9.538902640e-01, 1.750321269e+00);
        marker5.setID(5); marker5.setName("charly_Hip_3");
        marker5.setPosition(9.307594299e-01, 9.059755206e-01, 1.762212634e+00);
        marker6.setID(6); marker6.setName("charly_Hip_4");
        marker6.setPosition(7.274857163e-01, 9.561080933e-01, 1.596688032e+00);
        marker7.setID(7); marker7.setName("charly_Chest_1");
        marker7.setPosition(7.322196364e-01, 1.208971024e+00, 1.942528129e+00);
        marker8.setID(8); marker8.setName("charly_Chest_2");
        marker8.setPosition(9.106026888e-01, 1.307656765e+00, 1.763132930e+00);
        marker9.setID(9); marker9.setName("charly_Chest_3");
        marker9.setPosition(9.504148960e-01, 1.170968056e+00, 1.770930052e+00);
        marker10.setID(10); marker10.setName("charly_Chest_4");
        marker10.setPosition(8.215170503e-01, 1.210551620e+00, 1.672095776e+00);
        marker11.setID(11); marker11.setName("charly_Head_1");
        marker11.setPosition(8.992505670e-01, 1.602382421e+00, 1.996591806e+00);
        marker12.setID(12); marker12.setName("charly_Head_2");
        marker12.setPosition(8.086966872e-01, 1.519860983e+00, 2.084273100e+00);
        marker13.setID(13); marker13.setName("charly_Head_3");
        marker13.setPosition(7.989701033e-01, 1.558929086e+00, 1.940698147e+00);
        marker14.setID(14); marker14.setName("charly_LShoulder_1");
        marker14.setPosition(9.608798027e-01, 1.305301666e+00, 1.976194978e+00);
        marker15.setID(15); marker15.setName("charly_LShoulder_2");
        marker15.setPosition(1.013676763e+00, 1.252120256e+00, 1.865857124e+00);
        marker16.setID(16); marker16.setName("charly_LUArm_1");
        marker16.setPosition(1.036298990e+00, 9.601079226e-01, 1.965343356e+00);
        marker17.setID(17); marker17.setName("charly_LUArm_2");
        marker17.setPosition(1.041159630e+00, 1.033736587e+00, 1.908152699e+00);
        marker18.setID(18); marker18.setName("charly_LHand_1");
        marker18.setPosition(9.655301571e-01, 6.745933890e-01, 2.016285896e+00);
        marker19.setID(19); marker19.setName("charly_LHand_2");
        marker19.setPosition(9.067157507e-01, 7.477946281e-01, 2.023015499e+00);
        marker20.setID(20); marker20.setName("charly_LHand_3");
        marker20.setPosition(9.820458293e-01, 7.251198292e-01, 1.981912374e+00);
        marker21.setID(21); marker21.setName("charly_RShoulder_1");
        marker21.setPosition(7.513126731e-01, 1.420000315e+00, 1.802410245e+00);
        marker22.setID(22); marker22.setName("charly_RShoulder_2");
        marker22.setPosition(7.800209522e-01, 1.341635227e+00, 1.677561760e+00);
        marker23.setID(23); marker23.setName("charly_RUArm_1");
        marker23.setPosition(5.653971434e-01, 1.276031256e+00, 1.588958859e+00);
        marker24.setID(24); marker24.setName("charly_RUArm_2");
        marker24.setPosition(6.199302077e-01, 1.266115427e+00, 1.615455747e+00);
        marker25.setID(25); marker25.setName("charly_RHand_1");
        marker25.setPosition(2.433027774e-01, 1.340782285e+00, 1.577534676e+00);
        marker26.setID(26); marker26.setName("charly_RHand_2");
        marker26.setPosition(3.032289743e-01, 1.301971078e+00, 1.588537335e+00);
        marker27.setID(27); marker27.setName("charly_RHand_3");
        marker27.setPosition(3.284814060e-01, 1.371426225e+00, 1.613251090e+00);
        marker28.setID(28); marker28.setName("charly_LThigh_1");
        marker28.setPosition(7.605606914e-01, 5.126234293e-01, 1.878883123e+00);
        marker29.setID(29); marker29.setName("charly_LThigh_2");
        marker29.setPosition(8.545972109e-01, 4.413289428e-01, 1.849075913e+00);
        marker30.setID(30); marker30.setName("charly_LShin_1");
        marker30.setPosition(8.623179197e-01, 7.040882111e-02, 1.724575520e+00);
        marker31.setID(31); marker31.setName("charly_LShin_2");
        marker31.setPosition(8.072131872e-01, 2.159572840e-01, 1.806602955e+00);
        marker32.setID(32); marker32.setName("charly_LFoot_1");
        marker32.setPosition(7.502084970e-01, 4.062155634e-02, 1.822031856e+00);
        marker33.setID(33); marker33.setName("charly_LFoot_2");
        marker33.setPosition(8.501413465e-01, 4.042096063e-02, 1.834267259e+00);
        marker34.setID(34); marker34.setName("charly_RThigh_1");
        marker34.setPosition(5.768959522e-01, 5.398091078e-01, 1.744862437e+00);
        marker35.setID(35); marker35.setName("charly_RThigh_2");
        marker35.setPosition(5.646212101e-01, 4.874913394e-01, 1.633973360e+00);
        marker36.setID(36); marker36.setName("charly_RShin_1");
        marker36.setPosition(6.195909977e-01, 1.263073981e-01, 1.535456538e+00);
        marker37.setID(37); marker37.setName("charly_RShin_2");
        marker37.setPosition(5.740082264e-01, 3.339127004e-01, 1.660454273e+00);
        marker38.setID(38); marker38.setName("charly_RFoot_1");
        marker38.setPosition(5.573503971e-01, 4.789064825e-02, 1.677532911e+00);
        marker39.setID(39); marker39.setName("charly_RFoot_2");
        marker39.setPosition(5.171389580e-01, 4.410387203e-02, 1.596749425e+00);
        expectedAllMarkerset.addMarker(marker0);
        expectedAllMarkerset.addMarker(marker1);
        expectedAllMarkerset.addMarker(marker2);
        expectedAllMarkerset.addMarker(marker3);
        expectedAllMarkerset.addMarker(marker4);
        expectedAllMarkerset.addMarker(marker5);
        expectedAllMarkerset.addMarker(marker6);
        expectedAllMarkerset.addMarker(marker7);
        expectedAllMarkerset.addMarker(marker8);
        expectedAllMarkerset.addMarker(marker9);
        expectedAllMarkerset.addMarker(marker10);
        expectedAllMarkerset.addMarker(marker11);
        expectedAllMarkerset.addMarker(marker12);
        expectedAllMarkerset.addMarker(marker13);
        expectedAllMarkerset.addMarker(marker14);
        expectedAllMarkerset.addMarker(marker15);
        expectedAllMarkerset.addMarker(marker16);
        expectedAllMarkerset.addMarker(marker17);
        expectedAllMarkerset.addMarker(marker18);
        expectedAllMarkerset.addMarker(marker19);
        expectedAllMarkerset.addMarker(marker20);
        expectedAllMarkerset.addMarker(marker21);
        expectedAllMarkerset.addMarker(marker22);
        expectedAllMarkerset.addMarker(marker23);
        expectedAllMarkerset.addMarker(marker24);
        expectedAllMarkerset.addMarker(marker25);
        expectedAllMarkerset.addMarker(marker26);
        expectedAllMarkerset.addMarker(marker27);
        expectedAllMarkerset.addMarker(marker28);
        expectedAllMarkerset.addMarker(marker29);
        expectedAllMarkerset.addMarker(marker30);
        expectedAllMarkerset.addMarker(marker31);
        expectedAllMarkerset.addMarker(marker32);
        expectedAllMarkerset.addMarker(marker33);
        expectedAllMarkerset.addMarker(marker34);
        expectedAllMarkerset.addMarker(marker35);
        expectedAllMarkerset.addMarker(marker36);
        expectedAllMarkerset.addMarker(marker37);
        expectedAllMarkerset.addMarker(marker38);
        expectedAllMarkerset.addMarker(marker39);
    }

    void fillUnidentifiedMarkerset(){
        expectedUnidentifiedMarkerset.setID(-1);
        expectedUnidentifiedMarkerset.setName("_Unidentified_");
        lcgMarker marker0, marker1, marker2;
        // Marker position set at full float precision
        marker0.setID(0); marker0.setName("Unidentified_1");
        marker0.setPosition(1.407239735e-01, 1.952854633e+00, 1.307195067e+00);
        marker1.setID(1); marker1.setName("Unidentified_2");
        marker1.setPosition(-1.657311805e-02, 1.972101688e+00, 1.781518459e+00);
        marker2.setID(2); marker2.setName("Unidentified_3");
        marker2.setPosition(3.865728155e-02, 1.965723395e+00, 1.614876866e+00);
        expectedUnidentifiedMarkerset.addMarker(marker0);
        expectedUnidentifiedMarkerset.addMarker(marker1);
        expectedUnidentifiedMarkerset.addMarker(marker2);
    }

    void fillVaritaRigidBody(){
        expectedVaritaRigidBody.setID(1);
        expectedVaritaRigidBody.setName("varita");
        lcg::optitrack::Marker marker0, marker1, marker2;
        // Variables set at full float precision
        marker0.setID(0); marker0.setName("1"), marker0.setSize(0.024);
        marker0.setPosition(1.407239735e-01, 1.952854633e+00, 1.307195067e+00);
        marker1.setID(1); marker1.setName("2"), marker1.setSize(0.024);
        marker1.setPosition(-1.657311805e-02, 1.972101688e+00, 1.781518459e+00);
        marker2.setID(2); marker2.setName("3"), marker2.setSize(0.024);
        marker2.setPosition(3.865728155e-02, 1.965723395e+00, 1.614876866e+00);
        expectedVaritaRigidBody.addMarker(marker0);
        expectedVaritaRigidBody.addMarker(marker1);
        expectedVaritaRigidBody.addMarker(marker2);
        expectedVaritaRigidBody.setOrientation(-6.615330279e-02, 1.871326240e-04, 1.618160009e-01, 9.846011400e-01);
        expectedVaritaRigidBody.setPosition(5.427222326e-02, 1.963561535e+00, 1.567862749e+00);
    }

    void fillCharlieSkeleton() {
        expectedCharlieSkeleton.setID(1);
        expectedCharlieSkeleton.setName("charly");
        expectedCharlieSkeleton.setNumberOfElements(21);
        lcgRigidBody bone0, bone1, bone2, bone3, bone4, bone5, bone6, bone7,
            bone8, bone9, bone10, bone11, bone12, bone13, bone14, bone15,
            bone16, bone17, bone18, bone19, bone20;
        // Variables set at full float precision
        bone0.setID(1); bone0.setName("charly_Hip"); bone0.setParentID(0);
        bone0.setOffset(lcgPosition(0.000000000e+00, 8.306143880e-01, 0.000000000e+00));
        bone0.setPosition(7.626053095e-01,8.395130038e-01,1.744112730e+00);
        bone0.setOrientation(-7.909066230e-02,3.490059376e-01,6.398768723e-02,-9.315820336e-01);
        bone0.setMeanMarkerError(2.535111066e+10);
        bone1.setID(2); bone1.setName("charly_Ab"); bone1.setParentID(1);
        bone1.setOffset(lcgPosition(0.000000000e+00, 7.363284379e-02, 0.000000000e+00));
        bone1.setPosition(7.673187852e-01,9.116216898e-01,1.758251905e+00);
        bone1.setOrientation(-5.427018180e-02,3.341363072e-01,1.005843580e-01,-9.355695844e-01);
        bone1.setMeanMarkerError(7.534797652e+12);
        bone2.setID(3); bone2.setName("charly_Chest"); bone2.setParentID(2);
        bone2.setOffset(lcgPosition(0.000000000e+00, 2.009193152e-01, 0.000000000e+00));
        bone2.setPosition(7.965181470e-01,1.098632336e+00,1.790674210e+00);
        bone2.setOrientation(-8.841037750e-02,3.219409883e-01,1.477286816e-01,-9.309746623e-01);
        bone2.setMeanMarkerError(1.010204611e-38);
        bone3.setID(4); bone3.setName("charly_Neck"); bone3.setParentID(3);
        bone3.setOffset(lcgPosition(0.000000000e+00, 2.000941783e-01, 1.819037832e-02));
        bone3.setPosition(8.287870288e-01,1.285600662e+00,1.856781006e+00);
        bone3.setOrientation(-2.359109670e-01,2.777685523e-01,1.129553467e-01,-9.243547916e-01);
        bone3.setMeanMarkerError(7.225547948e+12);
        bone4.setID(5); bone4.setName("charly_Head"); bone4.setParentID(4);
        bone4.setOffset(lcgPosition(0.000000000e+00, 1.257146150e-01, -1.795922965e-02));
        bone4.setPosition(8.495097160e-01,1.405296326e+00,1.908240080e+00);
        bone4.setOrientation(-2.422195971e-01,2.054253221e-01,1.347136050e-01,-9.386065602e-01);
        bone4.setMeanMarkerError(9.642892450e-39);
        bone5.setID(6); bone5.setName("charly_LShoulder"); bone5.setParentID(3);
        bone5.setOffset(lcgPosition(-3.618568555e-02, 1.785806417e-01, -2.072017640e-03));
        bone5.setPosition(8.638747334e-01,1.254757404e+00,1.856193662e+00);
        bone5.setOrientation(-1.251041740e-01,3.680320978e-01,9.193687886e-02,-9.167600274e-01);
        bone5.setMeanMarkerError(0.000000000e+00);
        bone6.setID(7); bone6.setName("charly_LUArm"); bone6.setParentID(6);
        bone6.setOffset(lcgPosition(-1.153472811e-01, 0.000000000e+00, 0.000000000e+00));
        bone6.setPosition(9.460250735e-01,1.224691868e+00,1.931375980e+00);
        bone6.setOrientation(-5.205504894e-01,4.824966788e-01,4.582156837e-01,-5.350351334e-01);
        bone6.setMeanMarkerError(0.000000000e+00);
        bone7.setID(8); bone7.setName("charly_LFArm"); bone7.setParentID(7);
        bone7.setOffset(lcgPosition(-2.647213936e-01, 0.000000000e+00, 0.000000000e+00));
        bone7.setPosition(9.763279557e-01,9.619159698e-01,1.941768050e+00);
        bone7.setOrientation(-4.167547822e-01,5.749916434e-01,4.764885306e-01,-5.183230639e-01);
        bone7.setMeanMarkerError(0.000000000e+00);
        bone8.setID(9); bone8.setName("charly_LHand"); bone8.setParentID(8);
        bone8.setOffset(lcgPosition(-2.321911454e-01, 0.000000000e+00, 0.000000000e+00));
        bone8.setPosition(9.515697360e-01,7.529776692e-01,1.984467745e+00);
        bone8.setOrientation(-4.500550628e-01,5.403608084e-01,6.004642248e-01,-3.806617260e-01);
        bone8.setMeanMarkerError(0.000000000e+00);
        bone9.setID(10); bone9.setName("charly_RShoulder"); bone9.setParentID(3);
        bone9.setOffset(lcgPosition(3.556930274e-02, 1.785806417e-01, -2.072017640e-03));
        bone9.setPosition(8.101258874e-01, 1.278579235e+00, 1.815055370e+00);
        bone9.setOrientation(-1.492643654e-01, 3.177483082e-01, 2.024441808e-01, -9.142059684e-01);
        bone9.setMeanMarkerError(0.000000000e+00);
        bone10.setID(11); bone10.setName("charly_RUArm"); bone10.setParentID(10);
        bone10.setOffset(lcgPosition(1.153472811e-01, 0.000000000e+00, 0.000000000e+00));
        bone10.setPosition(7.275251746e-01, 1.332216740e+00, 1.755012512e+00);
        bone10.setOrientation(8.119563013e-02, 2.852839530e-01, -1.578006297e-01, -9.418701530e-01);
        bone10.setMeanMarkerError(0.000000000e+00);
        bone11.setID(12); bone11.setName("charly_RFArm"); bone11.setParentID(11);
        bone11.setOffset(lcgPosition(2.647213936e-01, 0.000000000e+00, 0.000000000e+00));
        bone11.setPosition(5.190771818e-01, 1.241262913e+00, 1.619534612e+00);
        bone11.setOrientation(2.717871368e-01, -4.715450853e-02, 1.887651980e-01, -9.424839616e-01);
        bone11.setMeanMarkerError(0.000000000e+00);
        bone12.setID(13); bone12.setName("charly_RHand"); bone12.setParentID(12);
        bone12.setOffset(lcgPosition(2.321911454e-01, 0.000000000e+00, 0.000000000e+00));
        bone12.setPosition(3.273118734e-01, 1.320407748e+00, 1.616696477e+00);
        bone12.setOrientation(5.072441101e-01, -5.840024352e-02, 1.385614276e-01, -8.485832810e-01);
        bone12.setMeanMarkerError(0.000000000e+00);
        bone13.setID(14); bone13.setName("charly_LThigh"); bone13.setParentID(1);
        bone13.setOffset(lcgPosition(-8.979614824e-02, 0.000000000e+00, 0.000000000e+00));
        bone13.setPosition(8.297908902e-01, 8.238502145e-01, 1.801594257e+00);
        bone13.setOrientation(2.644882724e-02, 3.231731355e-01, 1.579320803e-02, -9.458383322e-01);
        bone13.setMeanMarkerError(1.401298464e-45);
        bone14.setID(15); bone14.setName("charly_LShin"); bone14.setParentID(14);
        bone14.setOffset(lcgPosition(0.000000000e+00, -3.797295690e-01, 0.000000000e+00));
        bone14.setPosition(8.119547367e-01, 4.448413551e-01, 1.816716909e+00);
        bone14.setOrientation(-1.442226470e-01, 2.555872202e-01, 1.191551425e-02, -9.558938146e-01);
        bone14.setMeanMarkerError(2.535111066e+10);
        bone15.setID(16); bone15.setName("charly_LFoot"); bone15.setParentID(15);
        bone15.setOffset(lcgPosition(0.000000000e+00, -3.886449933e-01, 0.000000000e+00));
        bone15.setPosition(8.317534924e-01, 7.247447968e-02, 1.707191348e+00);
        bone15.setOrientation(4.816371948e-02, 1.175860688e-01, 2.817813307e-02, -9.914937019e-01);
        bone15.setMeanMarkerError(5.326619926e-39);
        bone16.setID(17); bone16.setName("charly_LToe"); bone16.setParentID(16);
        bone16.setOffset(lcgPosition(0.000000000e+00, -5.836749449e-02, 1.346942335e-01));
        bone16.setPosition(7.967897058e-01, 2.822743356e-02, 1.842723727e+00);
        bone16.setOrientation(4.816371948e-02, 1.175860688e-01, 2.817813307e-02, -9.914937019e-01);
        bone16.setMeanMarkerError(0.000000000e+00);
        bone17.setID(18); bone17.setName("charly_RThigh"); bone17.setParentID(1);
        bone17.setOffset(lcgPosition(8.979614824e-02, 0.000000000e+00, 0.000000000e+00));
        bone17.setPosition(6.954197288e-01, 8.551757932e-01, 1.686631203e+00);
        bone17.setOrientation(1.514538657e-02, 3.310874403e-01, 1.169645935e-01, -9.362003207e-01);
        bone17.setMeanMarkerError(0.000000000e+00);
        bone18.setID(19); bone18.setName("charly_RShin"); bone18.setParentID(18);
        bone18.setOffset(lcgPosition(0.000000000e+00, -3.797295690e-01, 0.000000000e+00));
        bone18.setPosition(6.084489226e-01, 4.860103726e-01, 1.667989254e+00);
        bone18.setOrientation(-1.552712172e-01, 3.414839506e-01, 1.748191076e-03, -9.269716740e-01);
        bone18.setMeanMarkerError(5.060917461e+12);
        bone19.setID(20); bone19.setName("charly_RFoot"); bone19.setParentID(19);
        bone19.setOffset(lcgPosition(0.000000000e+00, -3.886449933e-01, 0.000000000e+00));
        bone19.setPosition(6.484032869e-01, 1.161075234e-01, 1.555648327e+00);
        bone19.setOrientation(-9.305407852e-02, 4.507082403e-01, -3.939246759e-02, -8.869336247e-01);
        bone19.setMeanMarkerError(7.780165945e+33);
        bone20.setID(21); bone20.setName("charly_RToe"); bone20.setParentID(20);
        bone20.setOffset(lcgPosition(0.000000000e+00, -5.836749449e-02, 1.346942335e-01));
        bone20.setPosition(5.506775975e-01, 3.191573918e-02, 1.625725031e+00);
        bone20.setOrientation(-9.305407852e-02, 4.507082403e-01, -3.939246759e-02, -8.869336247e-01);
        bone20.setMeanMarkerError(0.000000000e+00);
        expectedCharlieSkeleton.addBone(bone0);
        expectedCharlieSkeleton.addBone(bone1);
        expectedCharlieSkeleton.addBone(bone2);
        expectedCharlieSkeleton.addBone(bone3);
        expectedCharlieSkeleton.addBone(bone4);
        expectedCharlieSkeleton.addBone(bone5);
        expectedCharlieSkeleton.addBone(bone6);
        expectedCharlieSkeleton.addBone(bone7);
        expectedCharlieSkeleton.addBone(bone8);
        expectedCharlieSkeleton.addBone(bone9);
        expectedCharlieSkeleton.addBone(bone10);
        expectedCharlieSkeleton.addBone(bone11);
        expectedCharlieSkeleton.addBone(bone12);
        expectedCharlieSkeleton.addBone(bone13);
        expectedCharlieSkeleton.addBone(bone14);
        expectedCharlieSkeleton.addBone(bone15);
        expectedCharlieSkeleton.addBone(bone16);
        expectedCharlieSkeleton.addBone(bone17);
        expectedCharlieSkeleton.addBone(bone18);
        expectedCharlieSkeleton.addBone(bone19);
        expectedCharlieSkeleton.addBone(bone20);
    }

    void fillLabeledMarkers() {
        expectedLabeledMarkerset.setID(-2);
        expectedLabeledMarkerset.setName("_Labeled_");
        lcgMarker marker0, marker1, marker2, marker3, marker4, marker5, marker6,
            marker7, marker8, marker9, marker10, marker11, marker12, marker13,
            marker14, marker15, marker16, marker17, marker18, marker19, marker20,
            marker21, marker22, marker23, marker24, marker25, marker26, marker27,
            marker28, marker29, marker30, marker31, marker32, marker33, marker34,
            marker35, marker36, marker37, marker38, marker39;
        // Marker position set at full float precision
        marker0.setID(1); marker0.setSize(2.400000021e-02); marker0.setName("Labeled_1");
        marker0.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker0.setPosition(1.407239735e-01, 1.952854633e+00, 1.307195067e+00);
        marker1.setID(2); marker1.setSize(2.400000021e-02); marker1.setName("Labeled_2");
        marker1.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker1.setPosition(-1.657311805e-02, 1.972101688e+00, 1.781518459e+00);
        marker2.setID(3); marker2.setSize(2.400000021e-02); marker2.setName("Labeled_3");
        marker2.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker2.setPosition(3.865728155e-02, 1.965723395e+00, 1.614876866e+00);
        marker3.setID(5); marker3.setSize(2.400000021e-02); marker3.setName("Labeled_5");
        marker3.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker3.setPosition(7.980908155e-01, 8.976786137e-01, 1.922066927e+00);
        marker4.setID(6); marker4.setSize(2.400000021e-02); marker4.setName("Labeled_6");
        marker4.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker4.setPosition(6.005586386e-01, 9.538902640e-01, 1.750321269e+00);
        marker5.setID(7); marker5.setSize(2.400000021e-02); marker5.setName("Labeled_7");
        marker5.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker5.setPosition(9.307594299e-01, 9.059755206e-01, 1.762212634e+00);
        marker6.setID(8); marker6.setSize(2.400000021e-02); marker6.setName("Labeled_8");
        marker6.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker6.setPosition(7.274857163e-01, 9.561080933e-01, 1.596688032e+00);
        marker7.setID(9); marker7.setSize(2.400000021e-02); marker7.setName("Labeled_9");
        marker7.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker7.setPosition(7.322196364e-01, 1.208971024e+00, 1.942528129e+00);
        marker8.setID(10); marker8.setSize(2.400000021e-02); marker8.setName("Labeled_10");
        marker8.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker8.setPosition(9.106026888e-01, 1.307656765e+00, 1.763132930e+00);
        marker9.setID(11); marker9.setSize(2.400000021e-02); marker9.setName("Labeled_11");
        marker9.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker9.setPosition(9.504148960e-01, 1.170968056e+00, 1.770930052e+00);
        marker10.setID(12); marker10.setSize(2.400000021e-02); marker10.setName("Labeled_12");
        marker10.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker10.setPosition(8.215170503e-01, 1.210551620e+00, 1.672095776e+00);
        marker11.setID(13); marker11.setSize(2.400000021e-02); marker11.setName("Labeled_13");
        marker11.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker11.setPosition(8.992505670e-01, 1.602382421e+00, 1.996591806e+00);
        marker12.setID(14); marker12.setSize(2.400000021e-02); marker12.setName("Labeled_14");
        marker12.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker12.setPosition(8.086966872e-01, 1.519860983e+00, 2.084273100e+00);
        marker13.setID(15); marker13.setSize(2.400000021e-02); marker13.setName("Labeled_15");
        marker13.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker13.setPosition(7.989701033e-01, 1.558929086e+00, 1.940698147e+00);
        marker14.setID(16); marker14.setSize(2.400000021e-02); marker14.setName("Labeled_16");
        marker14.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker14.setPosition(9.608798027e-01, 1.305301666e+00, 1.976194978e+00);
        marker15.setID(17); marker15.setSize(2.400000021e-02); marker15.setName("Labeled_17");
        marker15.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker15.setPosition(1.013676763e+00, 1.252120256e+00, 1.865857124e+00);
        marker16.setID(18); marker16.setSize(2.400000021e-02); marker16.setName("Labeled_18");
        marker16.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker16.setPosition(1.036298990e+00, 9.601079226e-01, 1.965343356e+00);
        marker17.setID(19); marker17.setSize(2.400000021e-02); marker17.setName("Labeled_19");
        marker17.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker17.setPosition(1.041159630e+00, 1.033736587e+00, 1.908152699e+00);
        marker18.setID(20); marker18.setSize(2.400000021e-02); marker18.setName("Labeled_20");
        marker18.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker18.setPosition(9.655301571e-01, 6.745933890e-01, 2.016285896e+00);
        marker19.setID(21); marker19.setSize(2.400000021e-02); marker19.setName("Labeled_21");
        marker19.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker19.setPosition(9.067157507e-01, 7.477946281e-01, 2.023015499e+00);
        marker20.setID(22); marker20.setSize(2.400000021e-02); marker20.setName("Labeled_22");
        marker20.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker20.setPosition(9.820458293e-01, 7.251198292e-01, 1.981912374e+00);
        marker21.setID(23); marker21.setSize(2.400000021e-02); marker21.setName("Labeled_23");
        marker21.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker21.setPosition(7.513126731e-01, 1.420000315e+00, 1.802410245e+00);
        marker22.setID(24); marker22.setSize(2.400000021e-02); marker22.setName("Labeled_24");
        marker22.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker22.setPosition(7.800209522e-01, 1.341635227e+00, 1.677561760e+000);
        marker23.setID(25); marker23.setSize(2.400000021e-02); marker23.setName("Labeled_25");
        marker23.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker23.setPosition(5.653971434e-01, 1.276031256e+00, 1.588958859e+00);
        marker24.setID(26); marker24.setSize(2.400000021e-02); marker24.setName("Labeled_26");
        marker24.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker24.setPosition(6.199302077e-01, 1.266115427e+00, 1.615455747e+00);
        marker25.setID(27); marker25.setSize(2.400000021e-02); marker25.setName("Labeled_27");
        marker25.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker25.setPosition(2.433027774e-01, 1.340782285e+00, 1.577534676e+00);
        marker26.setID(28); marker26.setSize(2.400000021e-02); marker26.setName("Labeled_28");
        marker26.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker26.setPosition(3.032289743e-01, 1.301971078e+00, 1.588537335e+00);
        marker27.setID(29); marker27.setSize(2.400000021e-02); marker27.setName("Labeled_29");
        marker27.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker27.setPosition(3.284814060e-01, 1.371426225e+00, 1.613251090e+00);
        marker28.setID(30); marker28.setSize(2.400000021e-02); marker28.setName("Labeled_30");
        marker28.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker28.setPosition(7.605606914e-01, 5.126234293e-01, 1.878883123e+00);
        marker29.setID(31); marker29.setSize(2.400000021e-02); marker29.setName("Labeled_31");
        marker29.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker29.setPosition(8.545972109e-01, 4.413289428e-01, 1.849075913e+00);
        marker30.setID(32); marker30.setSize(2.400000021e-02); marker30.setName("Labeled_32");
        marker30.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker30.setPosition(8.072131872e-01, 2.159572840e-01, 1.806602955e+00);
        marker31.setID(33); marker31.setSize(2.400000021e-02); marker31.setName("Labeled_33");
        marker31.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker31.setPosition(8.623179197e-01, 7.040882111e-02, 1.724575520e+00);
        marker32.setID(34); marker32.setSize(2.400000021e-02); marker32.setName("Labeled_34");
        marker32.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker32.setPosition(7.502084970e-01, 4.062155634e-02, 1.822031856e+00);
        marker33.setID(35); marker33.setSize(2.400000021e-02); marker33.setName("Labeled_35");
        marker33.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker33.setPosition(8.501413465e-01, 4.042096063e-02, 1.834267259e+00);
        marker34.setID(36); marker34.setSize(2.400000021e-02); marker34.setName("Labeled_36");
        marker34.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker34.setPosition(5.768959522e-01, 5.398091078e-01, 1.744862437e+00);
        marker35.setID(37); marker35.setSize(2.400000021e-02); marker35.setName("Labeled_37");
        marker35.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker35.setPosition(5.646212101e-01, 4.874913394e-01, 1.633973360e+00);
        marker36.setID(38); marker36.setSize(2.400000021e-02); marker36.setName("Labeled_38");
        marker36.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker36.setPosition(5.740082264e-01, 3.339127004e-01, 1.660454273e+00);
        marker37.setID(39); marker37.setSize(2.400000021e-02); marker37.setName("Labeled_39");
        marker37.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker37.setPosition(6.195909977e-01, 1.263073981e-01, 1.535456538e+00);
        marker38.setID(40); marker38.setSize(2.400000021e-02); marker38.setName("Labeled_40");
        marker38.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker38.setPosition(5.573503971e-01, 4.789064825e-02, 1.677532911e+00);
        marker39.setID(41); marker39.setSize(2.400000021e-02); marker39.setName("Labeled_41");
        marker39.setVisibility(lcgMarker::POINT_CLOUD_SOLVED);
        marker39.setPosition(5.171389580e-01, 4.410387203e-02, 1.596749425e+00);
        expectedLabeledMarkerset.addMarker(marker0);
        expectedLabeledMarkerset.addMarker(marker1);
        expectedLabeledMarkerset.addMarker(marker2);
        expectedLabeledMarkerset.addMarker(marker3);
        expectedLabeledMarkerset.addMarker(marker4);
        expectedLabeledMarkerset.addMarker(marker5);
        expectedLabeledMarkerset.addMarker(marker6);
        expectedLabeledMarkerset.addMarker(marker7);
        expectedLabeledMarkerset.addMarker(marker8);
        expectedLabeledMarkerset.addMarker(marker9);
        expectedLabeledMarkerset.addMarker(marker10);
        expectedLabeledMarkerset.addMarker(marker11);
        expectedLabeledMarkerset.addMarker(marker12);
        expectedLabeledMarkerset.addMarker(marker13);
        expectedLabeledMarkerset.addMarker(marker14);
        expectedLabeledMarkerset.addMarker(marker15);
        expectedLabeledMarkerset.addMarker(marker16);
        expectedLabeledMarkerset.addMarker(marker17);
        expectedLabeledMarkerset.addMarker(marker18);
        expectedLabeledMarkerset.addMarker(marker19);
        expectedLabeledMarkerset.addMarker(marker20);
        expectedLabeledMarkerset.addMarker(marker21);
        expectedLabeledMarkerset.addMarker(marker22);
        expectedLabeledMarkerset.addMarker(marker23);
        expectedLabeledMarkerset.addMarker(marker24);
        expectedLabeledMarkerset.addMarker(marker25);
        expectedLabeledMarkerset.addMarker(marker26);
        expectedLabeledMarkerset.addMarker(marker27);
        expectedLabeledMarkerset.addMarker(marker28);
        expectedLabeledMarkerset.addMarker(marker29);
        expectedLabeledMarkerset.addMarker(marker30);
        expectedLabeledMarkerset.addMarker(marker31);
        expectedLabeledMarkerset.addMarker(marker32);
        expectedLabeledMarkerset.addMarker(marker33);
        expectedLabeledMarkerset.addMarker(marker34);
        expectedLabeledMarkerset.addMarker(marker35);
        expectedLabeledMarkerset.addMarker(marker36);
        expectedLabeledMarkerset.addMarker(marker37);
        expectedLabeledMarkerset.addMarker(marker38);
        expectedLabeledMarkerset.addMarker(marker39);
    }

    void SetUp() {
        fillVaritaMarkerset();
        fillCharlieMarkerset();
        fillAllMarkerset();
        fillUnidentifiedMarkerset();
        fillVaritaRigidBody();
        fillCharlieSkeleton();
        fillLabeledMarkers();

        expectedServerVersion[0] = 1;
        expectedServerVersion[1] = 73;
        expectedServerVersion[2] = 0;
        expectedServerVersion[3] = 0;

        expectedNatNetVersion[0] = 2;
        expectedNatNetVersion[1] = 7;
        expectedNatNetVersion[2] = 0;
        expectedNatNetVersion[3] = 0;

        vExpectedServerVersion = lcgVersion(1,73);
        vExpectedNatNetVersion = lcgVersion(2,7);
        vExpectedServerVersionFail = lcgVersion(-1,-1,-1,-1);
        vExpectedNatNetVersionFail = lcgVersion(-1,-1,-1,-1);
        expectedFrameNumber = 16110;
        expectedServerAppName = "Motive";
        expectedConnectionStatus = 0;
        expectedLatency = 0.0d;
        expectedTimecode = 0;
        expectedTimecodeSubFrame = 0;
        expectedTimecodeStr = "00:00:00:00.00";
        expectedTimestamp = 1.342500000e+02;
        expectedIsServerRecording = false;
        expectedHaveTrackedModelsChanged = false;

        dataStreamingAddress_ = "239.255.42.99"; // IANA, local network
        clientAddress_ = "127.0.0.1";
        serverAddress_ = clientAddress_;
        commandReadingPort_ = 1510;
        dataStreamingPort_ = 1511;

        natNetServer_.start(serverAddress_, clientAddress_,
            commandReadingPort_, dataStreamingAddress_,dataStreamingPort_);
    }
    void TearDown(){
        clearDatasets();
        natNetServer_.stop();
    }
};

TEST_F(NatNetClient_initialize_Test, GetServerPingManually)
{
    int actualServerVersion[4];
    int actualNatNetVersion[4];
    std::string actualServerAppName;

    natNetClient.connectToServer(clientAddress_, serverAddress_, commandReadingPort_, dataStreamingPort_);
    natNetClient.getServerVersion(actualServerVersion); // Deprecated
    natNetClient.getNatNetVersion(actualNatNetVersion); // Deprecated
    lcgVersion vActualServerVersion = natNetClient.getServerVersion();
    lcgVersion vActualNatNetVersion = natNetClient.getNatNetVersion();

    actualServerAppName = natNetClient.getServerAppName();
    natNetClient.disconnect();

    ASSERT_THAT(actualServerVersion, ::testing::ContainerEq(expectedServerVersion));
    ASSERT_THAT(actualNatNetVersion, ::testing::ContainerEq(expectedNatNetVersion));
    ASSERT_EQ(vActualServerVersion, vExpectedServerVersion);
    ASSERT_EQ(vActualNatNetVersion, vExpectedNatNetVersion);
    ASSERT_STREQ(actualServerAppName.c_str(), expectedServerAppName.c_str());
}

TEST_F(NatNetClient_initialize_Test, GetServerPingManuallyMultipleTimes)
{
    int actualServerVersion[4];
    int actualNatNetVersion[4];
    std::string actualServerAppName;

    natNetClient.connectToServer(clientAddress_, serverAddress_, commandReadingPort_, dataStreamingPort_);
    natNetClient.getServerVersion(actualServerVersion); // Deprecated
    natNetClient.getNatNetVersion(actualNatNetVersion); // Deprecated
    lcgVersion vActualServerVersion = natNetClient.getServerVersion();
    lcgVersion vActualNatNetVersion = natNetClient.getNatNetVersion();
    actualServerAppName = natNetClient.getServerAppName();
    natNetClient.disconnect();

    ASSERT_THAT(actualServerVersion, ::testing::ContainerEq(expectedServerVersion));
    ASSERT_THAT(actualNatNetVersion, ::testing::ContainerEq(expectedNatNetVersion));
    ASSERT_EQ(vActualServerVersion, vExpectedServerVersion);
    ASSERT_EQ(vActualNatNetVersion, vExpectedNatNetVersion);
    ASSERT_STREQ(actualServerAppName.c_str(), expectedServerAppName.c_str());

    NatNetClient_initialize_Test::TearDown();
    NatNetClient_initialize_Test::SetUp();

    natNetClient.connectToServer(clientAddress_, serverAddress_, commandReadingPort_, dataStreamingPort_);
    natNetClient.getServerVersion(actualServerVersion); // Deprecated
    natNetClient.getNatNetVersion(actualNatNetVersion); // Deprecated
    vActualServerVersion = natNetClient.getServerVersion();
    vActualNatNetVersion = natNetClient.getNatNetVersion();
    actualServerAppName = natNetClient.getServerAppName();
    natNetClient.disconnect();

    ASSERT_THAT(actualServerVersion, ::testing::ContainerEq(expectedServerVersion));
    ASSERT_THAT(actualNatNetVersion, ::testing::ContainerEq(expectedNatNetVersion));
    ASSERT_EQ(vActualServerVersion, vExpectedServerVersion);
    ASSERT_EQ(vActualNatNetVersion, vExpectedNatNetVersion);
    ASSERT_STREQ(actualServerAppName.c_str(), expectedServerAppName.c_str());
}

TEST_F(NatNetClient_initialize_Test, GetServerPingManually_FailToConnect)
{
    // First test for proper connection
    int actualServerVersion[4];
    int actualNatNetVersion[4];
    std::string actualServerAppName;

    natNetClient.connectToServer(clientAddress_, serverAddress_, commandReadingPort_, dataStreamingPort_);
    natNetClient.getServerVersion(actualServerVersion); // Deprecated
    natNetClient.getNatNetVersion(actualNatNetVersion); // Deprecated
    lcgVersion vActualServerVersion = natNetClient.getServerVersion();
    lcgVersion vActualNatNetVersion = natNetClient.getNatNetVersion();
    actualServerAppName = natNetClient.getServerAppName();
    natNetClient.disconnect();

    //ASSERT_THAT(actualConnectionStatus, ::testing::Eq(expectedConnectionStatus));
    ASSERT_THAT(actualServerVersion, ::testing::ContainerEq(expectedServerVersion));
    ASSERT_THAT(actualNatNetVersion, ::testing::ContainerEq(expectedNatNetVersion));
    ASSERT_EQ(vActualServerVersion, vExpectedServerVersion);
    ASSERT_EQ(vActualNatNetVersion, vExpectedNatNetVersion);
    ASSERT_STREQ(actualServerAppName.c_str(), expectedServerAppName.c_str());

    // Stop NatNetServer command thread
    natNetServer_.stopCommandThread();

    // Start of actual test, now test for connection failure

    expectedServerAppName = "";
    expectedConnectionStatus = -1;
    int expectedGetServerVersionStatus = 1;
    int expectedGetNatNetVersionStatus = 1;
    int actualGetServerVersionStatus;
    int actualGetNatNetVersionStatus;

    natNetClient.connectToServer(clientAddress_, serverAddress_, commandReadingPort_, dataStreamingPort_);
    actualGetServerVersionStatus = natNetClient.getServerVersion(actualServerVersion);  // Deprecated
    actualGetNatNetVersionStatus = natNetClient.getNatNetVersion(actualNatNetVersion);  // Deprecated
    lcgVersion vActualServerVersionFail = natNetClient.getServerVersion();
    lcgVersion vActualNatNetVersionFail = natNetClient.getNatNetVersion();
    actualServerAppName = natNetClient.getServerAppName();
    natNetClient.disconnect();

    //ASSERT_THAT(actualConnectionStatus, ::testing::Eq(expectedConnectionStatus));
    ASSERT_THAT(actualGetServerVersionStatus, ::testing::Eq(expectedGetServerVersionStatus));
    ASSERT_THAT(actualGetNatNetVersionStatus, ::testing::Eq(expectedGetNatNetVersionStatus));
    ASSERT_EQ(vActualServerVersionFail, vExpectedServerVersionFail);
    ASSERT_EQ(vActualNatNetVersionFail, vExpectedNatNetVersionFail);
    ASSERT_STREQ(actualServerAppName.c_str(), expectedServerAppName.c_str());
}

TEST_F(NatNetClient_initialize_Test, GetServerPingAutomatically)
{
    int actualServerVersion[4];
    int actualNatNetVersion[4];
    std::string actualServerAppName;

    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.getServerVersion(actualServerVersion); // Deprecated
    natNetClient.getNatNetVersion(actualNatNetVersion); // Deprecated
    lcgVersion vActualServerVersion = natNetClient.getServerVersion();
    lcgVersion vActualNatNetVersion = natNetClient.getNatNetVersion();
    actualServerAppName = natNetClient.getServerAppName();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_THAT(actualServerVersion, ::testing::ContainerEq(expectedServerVersion));
    ASSERT_THAT(actualNatNetVersion, ::testing::ContainerEq(expectedNatNetVersion));
    ASSERT_EQ(vActualServerVersion, vExpectedServerVersion);
    ASSERT_EQ(vActualNatNetVersion, vExpectedNatNetVersion);
    ASSERT_STREQ(actualServerAppName.c_str(), expectedServerAppName.c_str());
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetFrameNumber)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    int actualFrameNumber = natNetClient.getFrameNumber();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_EQ(expectedFrameNumber, actualFrameNumber);
}

TEST_F(NatNetClient_initialize_Test, WaifForNewFrame)
{
    int nActualFrameNumber;
    int nExpectedFrameNumber = -1;
    natNetServer_.startDataThread();

    nActualFrameNumber = natNetClient.waitForNewFrame();
    ASSERT_EQ(nExpectedFrameNumber, nActualFrameNumber);

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    nActualFrameNumber = natNetClient.waitForNewFrame();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    nExpectedFrameNumber = 16110;
    ASSERT_EQ(nExpectedFrameNumber, nActualFrameNumber);
}

TEST_F(NatNetClient_initialize_Test, WaifForNewFrame_TimeoutException)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();

    natNetServer_.stopDataThread();

    EXPECT_THROW(natNetClient.waitForNewFrame(), std::runtime_error);

    natNetClient.disconnect();
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetMarkersetFromName)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    lcg::optitrack::Markerset varitaTestMarkerset = natNetClient.getMarkersetFromName("varita");
    lcg::optitrack::Markerset charlieTestMarkerset =  natNetClient.getMarkersetFromName("charly");
    lcg::optitrack::Markerset allTestMarkerset = natNetClient.getMarkersetFromName("all");

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_EQ(expectedVaritaMarkerset, varitaTestMarkerset);
    ASSERT_EQ(expectedCharlieMarkerset, charlieTestMarkerset);
    ASSERT_EQ(expectedAllMarkerset, allTestMarkerset);
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetMarkersetFromName_unknownNameThrowException)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();

    EXPECT_THROW(natNetClient.getMarkersetFromName("foo_bar"), std::runtime_error);

    natNetServer_.stopDataThread();
    natNetClient.disconnect();
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetUnidentifiedMarkers)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    lcgMarkerset unidentifiedTestMarkerset1 = natNetClient.getMarkersetFromName("_Unidentified_");
    lcgMarkerset unidentifiedTestMarkerset2 = natNetClient.getUnidentifiedMarkers();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_EQ(expectedUnidentifiedMarkerset, unidentifiedTestMarkerset1);
    ASSERT_EQ(unidentifiedTestMarkerset1, unidentifiedTestMarkerset2);
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetRigidBodyFromName)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    lcgRigidBody varitaTestRigidBody = natNetClient.getRigidBodyFromName("varita");

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_EQ(expectedVaritaRigidBody, varitaTestRigidBody);
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetRigidBodyFromName_unknownNameThrowException)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();

    EXPECT_THROW(natNetClient.getRigidBodyFromName("foo_bar"), std::runtime_error);

    natNetServer_.stopDataThread();
    natNetClient.disconnect();
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetSkeletonFromName)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    lcgSkeleton charlieTestSkeleton = natNetClient.getSkeletonFromName("charly");

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_EQ(expectedCharlieSkeleton, charlieTestSkeleton);
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetSkeletonFromName_unknownNameThrowException)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();

    EXPECT_THROW(natNetClient.getSkeletonFromName("foo_bar"), std::runtime_error);

    natNetServer_.stopDataThread();
    natNetClient.disconnect();
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetLabeledMarkers)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    lcgMarkerset labeledTestMarkerset1 = natNetClient.getMarkersetFromName("_Labeled_");
    lcgMarkerset labeledTestMarkerset2 = natNetClient.getLabeledMarkers();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_EQ(expectedLabeledMarkerset, labeledTestMarkerset1);
    ASSERT_EQ(labeledTestMarkerset1, labeledTestMarkerset2);
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetLatency)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    double testLatency = natNetClient.getLatency();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_DOUBLE_EQ(expectedLatency, testLatency);
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetTimecode)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    unsigned int testTimecode, testTimecodeSubFrame;
    std::string testTimecodeStr;
    natNetClient.getTimecode(testTimecode, testTimecodeSubFrame);
    testTimecodeStr = natNetClient.getTimecodeString();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_EQ(expectedTimecode, testTimecode);
    ASSERT_EQ(expectedTimecodeSubFrame, testTimecodeSubFrame);
    ASSERT_STREQ(expectedTimecodeStr.c_str(), testTimecodeStr.c_str());
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGetTimestamp)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    double testTimestamp = natNetClient.getTimestamp();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_DOUBLE_EQ(expectedTimestamp, testTimestamp);
}

TEST_F(NatNetClient_initialize_Test, InitializeAndGet_isServerRecording_And_haveTrackedModelsChanged)
{
    natNetServer_.startDataThread();

    natNetClient.connectToServer(clientAddress_, "", commandReadingPort_, dataStreamingPort_);
    natNetClient.waitForNewFrame();
    bool testIsServerRecording = natNetClient.isServerRecording();
    bool testHaveTrackedModelsChanged = natNetClient.haveTrackedModelsChanged();

    natNetServer_.stopDataThread();
    natNetClient.disconnect();

    ASSERT_EQ(expectedIsServerRecording, testIsServerRecording);
    ASSERT_EQ(expectedHaveTrackedModelsChanged, testHaveTrackedModelsChanged);
}
#endif  // For commeinting all these tests, thus performing other classes' tests faster
