// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Dataset.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/Dataset.h"

#include <lcg/core/Number.h>    // lcg::core::numeric_limits

#include <cmath>        // std::abs, std::cos, std::sin, std::atan2, M_PI, std::asin
#include <limits>       // std::numeric_limits
#include <iostream>     // std::cout
#include <sstream>      // std::ostringstream

namespace lcg{
namespace optitrack{

std::runtime_error Position::throwGetPointerNullException() const
{
    std::stringstream ss;
    ss << "Attempting to fill raw position " << this << " in a null pointer.";
    return std::runtime_error(ss.str());
}

std::runtime_error Position::throwPrintNullException()
{
    return std::runtime_error("Attempting to print null pointer to Position object");
}

Position::Position() : x(0.0), y(0.0), z(0.0)
{
}

Position::Position(const float& x, const float& y, const float& z)
{
    this->x = x; this->y = y; this->z = z;
}

Position::~Position()
{
}

void Position::print() const
{
    std::cout << this;
}

bool Position::isEqual(const Core &other) const
{
    const Position* otherPosition = dynamic_cast<const Position*>(&other);
    if( !(std::abs(x - otherPosition->x) < std::numeric_limits<float>::epsilon()) ) {
        return false;
    }
    else if( !(std::abs(y - otherPosition->y) < std::numeric_limits<float>::epsilon()) ) {
        return false;
    }
    else if( !(std::abs(z - otherPosition->z) < std::numeric_limits<float>::epsilon()) ) {
        return false;
    } else {
        return true;
    }
}

void Position::set(const float& newX, const float& newY, const float& newZ)
{
    x = newX; y = newY; z = newZ;
}

void Position::get(float* currentPosition) const
{
    if(currentPosition==NULL){
        throw throwGetPointerNullException();
    }
    currentPosition[0] = x;
    currentPosition[1] = y;
    currentPosition[2] = z;
}

bool Position::operator==(const Core& other) const
{
    return isEqual(other);
}

bool Position::operator!=(const Core& other) const
{
    return !isEqual(other);
}

std::ostream& operator<<(std::ostream& os, const Position& position)
{
    os << &position;
    return os;
}

std::ostream& operator<<(std::ostream& os, const Position* position)
{
    if(position==NULL){
        throw Position::throwPrintNullException();
    }
    //TODO: Remove precision change, leave the change to the end developer at cout statement
    std::streamsize lastPrecision = os.precision();
    os.precision(core::numeric_limits<float>::max_digits10);
    os << std::scientific;
    //os << std::numeric_limits<float>::epsilon() << std::endl;
    //os << "precision: " << lastPrecision << ", " << lcg::numeric_limits<float>::max_digits10 << ". ";
    os << "[x=" << position->x << ", y=" << position->y << ", z=" << position->z <<
          "]";
    os.unsetf(std::ios_base::floatfield);
    os.precision(lastPrecision);
    return os;
}

std::runtime_error Orientation::throwGetPointerNullException() const
{
    std::stringstream ss;
    ss << "Attempting to fill raw orientation " << this << " in a null pointer.";
    return std::runtime_error(ss.str());

}

std::runtime_error Orientation::throwPrintNullException()
{
    return std::runtime_error("Attempting to print null pointer to Orientation object");
}

Orientation::Orientation() : qx(0.0), qy(0.0), qz(0.0), qw(0.0),
    printNotation(Orientation::EULER_XYZ)
{
}

Orientation::Orientation(const float& qX, const float& qY, const float& qZ, const float& qW) :
    qx(qX), qy(qY), qz(qZ), qw(qW), printNotation(Orientation::EULER_XYZ)
{
}

Orientation::Orientation(const float &eulerX, const float &eulerY, const float &eulerZ) :
    printNotation(Orientation::EULER_XYZ)
{
    set(eulerX, eulerY, eulerZ);
}

Orientation::~Orientation()
{
}

void Orientation::print() const
{
    std::cout << this;
}

bool Orientation::isEqual(const Core& other) const
{
    const Orientation* otherOrientation = dynamic_cast<const Orientation*>(&other);
    if( !(std::abs(qx - otherOrientation->qx) < std::numeric_limits<float>::epsilon()) ) {
        return false;
    }
    else if( !(std::abs(qy - otherOrientation->qy) < std::numeric_limits<float>::epsilon()) ) {
        return false;
    }
    else if( !(std::abs(qz - otherOrientation->qz) < std::numeric_limits<float>::epsilon()) ) {
        return false;
    }
    else if( !(std::abs(qw - otherOrientation->qw) < std::numeric_limits<float>::epsilon()) ) {
        return false;
    } else {
        return true;
    }
}

void Orientation::set(const float& newQX, const float& newQY, const float& newQZ, const float& newQW)
{
    qx = newQX; qy = newQY; qz = newQZ; qw = newQW;
}

void Orientation::set(const float &newEulerX, const float &newEulerY, const float &newEulerZ)
{   // Assuming the angles are in Radians
    //if( (newEulerZ > M_PI/2.0) || (newEulerZ < -M_PI/2.0))  { // If angle greater than 90° or less than 90°
    //
    //}
    double c1 = std::cos(static_cast<double>(newEulerY)/2.0);
    double s1 = std::sin(static_cast<double>(newEulerY)/2.0);
    double c2 = std::cos(static_cast<double>(newEulerZ)/2.0);
    double s2 = std::sin(static_cast<double>(newEulerZ)/2.0);
    double c3 = std::cos(static_cast<double>(newEulerX)/2.0);
    double s3 = std::sin(static_cast<double>(newEulerX)/2.0);
    double c1c2 = c1*c2;
    double s1s2 = s1*s2;
    qw = static_cast<float>(c1c2*c3 - s1s2*s3);
    qx = static_cast<float>(c1c2*s3 + s1s2*c3);
    qy = static_cast<float>(s1*c2*c3 + c1*s2*s3);
    qz = static_cast<float>(c1*s2*c3 - s1*c2*s3);
    // The Quaternions are normalized
}

void Orientation::setPrintNotation(Orientation::Notation newPrintNotation)
{
    printNotation = newPrintNotation;
}

void Orientation::get(Orientation::Notation notation, float* currentOrientation) const
{
    if(currentOrientation==NULL){
        throw throwGetPointerNullException();
    }
    switch (notation){
    case QUATERNIONS:
        currentOrientation[0] = qx;
        currentOrientation[1] = qy;
        currentOrientation[2] = qz;
        currentOrientation[3] = qw;
        break;
    case EULER_XYZ:
        double dqw = static_cast<double>(qw);
        double dqx = static_cast<double>(qx);
        double dqy = static_cast<double>(qy);
        double dqz = static_cast<double>(qz);
        double sqw = dqw*dqw;
        double sqx = dqx*dqx;
        double sqy = dqy*dqy;
        double sqz = dqz*dqz;

        double unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
        double test = dqx*dqy + dqz*dqw;
        //if (test < 0.499999999999*unit) { // singularity at south pole
        if (std::abs((test - static_cast<double>(0.5)*unit)) < std::numeric_limits<float>::epsilon()) { // singularity at north pole
            currentOrientation[1] = static_cast<float>(2.0 * std::atan2(dqx,dqw));   // heading (rotation about Y axis)
            currentOrientation[2] = static_cast<float>(M_PI/2.0);                  // attitude (rotation about Z axis)
            currentOrientation[0] = static_cast<float>(0.0);                       // bank   (rotation about X axis)
            return;
        //} else if (test < -0.4999*unit) { // singularity at south pole
        } else if ((std::abs(test + static_cast<double>(0.5)*unit)) < std::numeric_limits<float>::epsilon()) { // singularity at south pole
            currentOrientation[1] = static_cast<float>(-2.0 * std::atan2(dqx,dqw));  // heading (rotation about Y axis)
            currentOrientation[2] = static_cast<float>(-M_PI/2.0);                 // attitude (rotation about Z axis)
            currentOrientation[0] = static_cast<float>(0.0);                       // bank (rotation about X axis)
            return;
        } else {
            currentOrientation[1] = static_cast<float>(std::atan2(2.0*dqy*dqw-2.0*dqx*dqz , sqx - sqy - sqz + sqw));   // heading (rotation about Y axis)
            currentOrientation[2] = static_cast<float>(std::asin(2.0*test/unit)); // -pi/2 to pi/2                 // attitude (rotation about Z axis)
            currentOrientation[0] = static_cast<float>(std::atan2(2.0*dqx*dqw-2.0*dqy*dqz , -sqx + sqy - sqz + sqw));  // bank (rotation about X axis)
            return;
        }
        break;
    }
}

bool Orientation::operator==(const Core &other) const
{
    return isEqual(other);
}

bool Orientation::operator!=(const Core &other) const
{
    return !isEqual(other);
}

std::ostream& operator<<(std::ostream& os, const Orientation& orientation)
{
    os << &orientation;
    return os;
}

std::ostream& operator<<(std::ostream& os, const Orientation* orientation)
{
    if(orientation==NULL){
        throw Orientation::throwPrintNullException();
    }
    //TODO: Remove precision change, leave the change to the end developer at cout statement
    std::streamsize lastPrecision = os.precision();
    os.precision(core::numeric_limits<float>::max_digits10);
    os << std::scientific;
    //os << std::numeric_limits<float>::epsilon() << std::endl;
    //os << "precision: " << lastPrecision << ", " << lcg::numeric_limits<float>::max_digits10 << ". ";

    switch (orientation->printNotation){
    case Orientation::QUATERNIONS:
        os << "[qX=" << orientation->qx << ",qY=" << orientation->qy <<
              ",qZ=" << orientation->qz << ",qW=" << orientation->qw <<
              "] Quaternions";
        break;
    case Orientation::EULER_XYZ:
        float euler_XYZ[3];
        orientation->get(Orientation::EULER_XYZ, euler_XYZ);
        os << "[X=" << Orientation::radiansToDegrees(euler_XYZ[0]) <<
              ",Y=" << Orientation::radiansToDegrees(euler_XYZ[1]) <<
              ",Z=" << Orientation::radiansToDegrees(euler_XYZ[2]) <<
              "] Degrees";
        break;
    default:
        break;
    }

    os.unsetf(std::ios_base::floatfield);
    os.precision(lastPrecision);
    return os;
}

double Orientation::radiansToDegrees(const double& radians)
{
    return radians*180.0/M_PI;
}

double Orientation::degreesToRadians(const double& degrees)
{
    return degrees*M_PI/180.0;
}

std::runtime_error Marker::throwPrintNullException()
{
    return std::runtime_error("Attempting to print null pointer to Marker object");
}

Marker::Marker(const float x, const float y, const float z) :
    size_(0.0),
    visibility_(UNKNOWN)
{
    position_.set(x,y,z);
}

Marker::~Marker()
{
}

void Marker::print() const
{
    std::cout << this;
}

bool Marker::isEqual(const Core &other) const
{
    const Marker* pMarker = dynamic_cast<const Marker*>(&other);
    if(!Core::isEqual(other)){
        return false;
    }
    else if(size_ != pMarker->getSize()){
        return false;
    }
    else if(position_ != pMarker->getPosition()){
        return false;
    } else {
        return true;
    }
}

void Marker::setPosition(const float newX, const float newY, const float newZ)
{
    position_.set(newX, newY, newZ);
}

void Marker::setPosition(const Position& newPosition)
{
    position_ = newPosition;
}

Position Marker::getPosition() const
{
    return position_;
}

float Marker::getSize() const
{
    return size_;
}

void Marker::setVisibility(Marker::Visibility newVisibility)
{
    visibility_ = newVisibility;
}

Marker::Visibility Marker::getVisibility()
{
    return visibility_;
}

void Marker::setSize(const float newSize)
{
    size_ = newSize;
}

bool Marker::operator==(const Core& other) const
{
    return isEqual(other);
}

bool Marker::operator!=(const Core& other) const
{
    return !isEqual(other);
}

std::ostream& operator<<(std::ostream& os, const Marker& marker)
{
    os << &marker;
    return os;
}

std::ostream& operator<<(std::ostream& os, const Marker* marker)
{
    if(marker==NULL){
        throw Marker::throwPrintNullException();
    }
    std::streamsize lastPrecision = os.precision();
    os.precision(std::numeric_limits<float>::digits10);
    os << "name=" << marker->getName() << " size=" << marker->getSize()
       << " pos=" << marker->getPosition();
    os.precision(lastPrecision);
    return os;
}

Dataset::Dataset()
{
}

Dataset::~Dataset()
{
}

bool Dataset::isEqual(const Core &other) const
{
    if(!Core::isEqual(other)){
        return false;
    }
    return true;
}

bool Dataset::operator==(const Core& other) const
{
    return isEqual(other);
}

bool Dataset::operator!=(const Core& other) const
{
    return !isEqual(other);
}

} // namespace optitrack
} // namespace lcg
