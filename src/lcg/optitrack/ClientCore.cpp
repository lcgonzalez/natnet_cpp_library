// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file ClientCore.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.4
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/ClientCore.h"

#include <lcg/core/Number.h>    // lcg::core::Number::intToString
#include <lcg/core/debug.h>     // LCGDEBUG macro

#include <stdint.h>     // int_fastXX_t

#include <cstring>      // std::memcpy
#include <fstream>      // std::fstream (for extracting testing mock files)
#include <memory>       // std::auto_ptr
#include <stdexcept>    // std::runtime_error
#include <sstream>      // std::ostringstream
#include <iomanip>      // std::setfill, std::setw

namespace lcg{
namespace optitrack{

// Useful typedefs
typedef lcg::core::Version lcgVersion;

ClientCore::ClientCore() :
    dataReadingThread_(this),
    commandReadingThread_(this),
    waitForServerThread_(this),
    commandProcessor_(this),
    frame_(this),
    state_(NOT_CONNECTED),
    appID("[NatNetClient]")
{
	pLocalAddress = new std::string;
	pServerAddress = new std::string;
	pMulticastAddress = new std::string;
}

ClientCore::~ClientCore()
{
    disconnect();
	delete pLocalAddress;
	delete pServerAddress;
    delete pMulticastAddress;
    frame_.getModel()->deleteDefinition();
}

int ClientCore::sendCommand(Command command)
{
    sPacket packetOut;
    std::auto_ptr<std::string> pRequest;
    switch (command){
    case MODEL:
        // send NAT_REQUEST_MODELDEF command to server (response comming on the "commandReadingThread")
        packetOut.iMessage = NAT_REQUEST_MODELDEF;
        packetOut.nDataBytes = 0;
        LCGDEBUG() << "Sending NAT_REQUEST_MODELDEF" << std::endl;
        break;
    case DATA:
        // send NAT_REQUEST_FRAMEOFDATA (response comming on the "commandReadingThread")
        packetOut.iMessage = NAT_REQUEST_FRAMEOFDATA;
        packetOut.nDataBytes = 0;
        LCGDEBUG() << "Sending NAT_REQUEST_FRAMEDATA" << std::endl;
        break;
    case TEST:
        // send NAT_MESSAGESTRING (response comming on the "commandReadingThread")
        pRequest = std::auto_ptr<std::string> (new std::string("TestRequest"));
        packetOut.iMessage = NAT_REQUEST;
        packetOut.nDataBytes = pRequest->length() + 1;
        strcpy(packetOut.Data.szData, pRequest->c_str());
        LCGDEBUG() << "Sending NAT_REQUEST" << std::endl;
        break;
    case PING:
        // send NAT_MESSAGESTRING (response comming on the "commandReadingThread")
        packetOut.iMessage = NAT_PING;
        packetOut.nDataBytes = 0;
        LCGDEBUG() << "Sending NAT_PING" << std::endl;
        break;
    default:
        break;
    }

    int nTries = 1;
    int iRet = 0;
    while (nTries--){
        iRet = commandSocket_.writeDatagram((char *)&packetOut, 4 + packetOut.nDataBytes, *pServerAddress, serverCommandPort);
        if(iRet < 0)
            break;
    }
}

int ClientCore::connectToServer(std::string szLocalAddress, std::string szServerAddress, int serverCommandPort, int clientDataPort)
{
    switch(state_){
    case NOT_CONNECTED:
    case GETTING_VERSION:
    case GETTING_MODEL_DEF:
        *pLocalAddress = szLocalAddress;
        *pServerAddress = szServerAddress;
        *pMulticastAddress = "239.255.42.99"; // IANA, local network
        this->serverCommandPort = serverCommandPort;
        this->clientDataPort = clientDataPort;
        createSockets();

        state_ = GETTING_VERSION;
        if(pServerAddress->empty()){ // Attempt autoconnect to the server using multicasting
            autoConnectToServer();
        } else {        // Connect to the server using the address provided
            requestVersions();
        }

        if(state_ == GETTING_MODEL_DEF){
            return updateModelDefinition();
        } else {
            return -1;  // Error while getting the NatNet versions
        }
    case CONNECTED:
        LCGDEBUG() << appID << "Already connected to server: " << *pServerAddress << std::endl
                  << "Please disconnect from it in order to connect to a new server." << std::endl;
        return 1;
    }
}

int ClientCore::waitForNewFrame()
{
    if(state_ != CONNECTED){
        return -1;
    }
    while(!frame_.hasFrameChanged()){
        if(dataReadingThread_.waitingTimeoutReached()) {  // Timeout reached
            throw errorWaitForNewFrameTimeoutException();
        }
        processThreadsExceptions();
    }

    if(haveTrackedModelsChanged()){
        LCGDEBUG() << "The model definition has changed in the server, querying the new model definition." << std::endl;
        updateModelDefinition();
    }
    processThreadsExceptions();

    return frame_.getFrameNumber();
}

// Deprecated
int ClientCore::getServerVersion(int *serverVersion)
{
    if(state_ == CONNECTED) {
        core::MutexLocker ml(&mutex_);
        for(int i=0; i<4; i++) {
            serverVersion[i] = serverVersion_.fullVersion[i];
        }
        return 0;
    } else {
        return 1;
    }
}

// Deprecated
int ClientCore::getNatNetVersion(int *natNetVersion)
{
    if(state_ == CONNECTED) {
        core::MutexLocker ml(&mutex_);
       for(int i=0; i<4; i++)
            natNetVersion[i] = natNetVersion_.fullVersion[i];
        return 0;
    } else {
        return 1;
    }
}

int ClientCore::setWaitForNewFrameTimeout(unsigned int secTimeout, unsigned int uSecTimeout)
{
    return dataReadingThread_.seTimeout(secTimeout, uSecTimeout);
}

lcgVersion ClientCore::getServerVersion()
{
    if(state_ == CONNECTED) {
        core::MutexLocker ml(&mutex_);
        return serverVersion_;
    } else {
        return lcgVersion(-1, -1, -1, -1);  // -1.-1.-1.-1
    }
}

lcgVersion ClientCore::getNatNetVersion()
{
    if(state_ == CONNECTED) {
        core::MutexLocker ml(&mutex_);
        return natNetVersion_;
    } else {
        return lcgVersion(-1, -1, -1, -1);  // -1.-1.-1.-1
    }
}

int ClientCore::getFrameNumber()
{
    return frame_.getFrameNumber();
}

double ClientCore::getLatency()
{
    return frame_.getLatency();
}

void ClientCore::getTimecode(unsigned int& outTimecode, unsigned int& outTimecodeSubFrame)
{
    frame_.getTimecode(outTimecode, outTimecodeSubFrame);
}

std::string ClientCore::getTimecodeString()
{
    return frame_.getTimecodeString();
}

double ClientCore::getTimestamp()
{
    return frame_.getTimestamp();
}

bool ClientCore::isServerRecording()
{
    return frame_.isServerRecording();
}

bool ClientCore::haveTrackedModelsChanged()
{
    return frame_.haveTrackedModelsChanged();
}

std::string ClientCore::getServerAppName()
{
    if(state_ == CONNECTED) {
        core::MutexLocker ml(&mutex_);
        return serverName_;
    } else {
        std::string tmpStr;
        return tmpStr;
    }
}

Markerset ClientCore::getMarkersetFromName(std::string markersetName)
{
    Markerset *pMarkerset = frame_.getModel()->getMarkersetFromName(markersetName);
    if(pMarkerset != NULL) { // If the markerset exists
        return *pMarkerset;
    } else {
        throw errorNoMarkersetFromNameException(markersetName);
    }

}

Markerset ClientCore::getUnidentifiedMarkers()
{
    return getMarkersetFromName("_Unidentified_");
}

Markerset ClientCore::getLabeledMarkers()
{
    return getMarkersetFromName("_Labeled_");
}

RigidBody ClientCore::getRigidBodyFromName(std::string rigidBodyName)
{
    RigidBody* pRigidBody = frame_.getModel()->getRigidBodyFromName(rigidBodyName);
    if(pRigidBody != NULL) { // If the rigidBody exists
        return *pRigidBody;
    } else {
        throw errorNoMarkersetFromNameException(rigidBodyName);
    }

}

Skeleton ClientCore::getSkeletonFromName(std::string skeletonName)
{
    Skeleton* pSkeleton = frame_.getModel()->getSkeletonFromName(skeletonName);
    if(pSkeleton != NULL) { // If the skeleton exists
        return *pSkeleton;
    } else {
        throw errorNoSkeletonFromNameException(skeletonName);
    }
}

void ClientCore::disconnect()
{
    dataReadingThread_.exit();
    dataReadingThread_.wait();
    commandReadingThread_.exit();
    commandReadingThread_.wait();
    commandSocket_.close();
    dataSocket_.close();
    frame_.resetLatency();
    state_ = NOT_CONNECTED;
}

std::runtime_error ClientCore::Frame::Unpacker::errorAttemptToFillUndefinedMarkersetException(const std::string& markersetName)
{
    std::ostringstream oss;
    oss << "Error while attempting to unpack and fill a MarkerSet." << std::endl <<
          "There is no model definition for Markerset \"" << markersetName << "\".";
    return std::runtime_error(oss.str());
}

std::runtime_error ClientCore::Frame::Unpacker::errorMarkersetMisalignmentErrorException(const std::string& markersetName, const int nMarkers)
{
    Markerset* pMarkerset = owner_->model_.getMarkersetFromName(markersetName);
    std::ostringstream oss;
    oss << "Error while attempting to unpack and fill the Markers from Markerset \"" <<
           markersetName << "\". " << std::endl <<
           "The definition of that Markerset has \"" << pMarkerset->getNumberOfElements() <<
           "\" Markers, while the Markerset in the packed being decoded has \"" << nMarkers << "\" Markers.";
    return std::runtime_error(oss.str());
}

std::runtime_error ClientCore::Frame::Unpacker::errorAttemptToFillUndefinedRigidBodyFromIDException(const int rigidBodyID)
{
    std::ostringstream oss;
    oss << "Error while attempting to unpack and fill a RigidBody." << std::endl <<
           "There is no model definition for a RigidBody with ID \"" << rigidBodyID << "\".";
    return std::runtime_error(oss.str());
}

std::runtime_error ClientCore::Frame::Unpacker::errorAttemptToFillUndefinedSkeletonFromIDException(const int skeletonID)
{
    std::ostringstream oss;
    oss << "Error while attempting to unpack and fill a Skeleton." << std::endl <<
           "There is no model definition for a Skeleton with ID \"" << skeletonID << "\".";
    return std::runtime_error(oss.str());
}

std::runtime_error ClientCore::errorWaitForNewFrameTimeoutException()
{
    unsigned int secTimeout, uSecTimeout;
    dataReadingThread_.getTimeout(secTimeout, uSecTimeout);
    std::ostringstream oss;
    oss << "Timeout exception occurred while waiting for new data frame to arrive." << std::endl
       << "A new data frame did't arrive in " <<uSecTimeout
       << " useconds and " << secTimeout << " seconds.";
    return std::runtime_error(oss.str());
}

void ClientCore::createSockets()
{
    commandSocket_.bind(*pLocalAddress, 0, core::UdpSocket::ShareAddress);

    // TODO: Be careful with the bind address of the dataSocket_, if bound to
    // multicastAddress_ or QHostAddress::AnyIPv4
    // Both approaches work, but multicastAddress_ maight not work for loopback,
    // which means that the server is on the same machine.
    // myAddress_ doens't work here when used in Multicast
    //dataSocket_.bind("0.0.0.0", clientDataPort, UdpSocket::ShareAddress);


        //join multicast group
    //dataSocket_.close();

    dataSocket_.joinMulticastGroup(*pMulticastAddress, *pLocalAddress, clientDataPort);
    // Note: The dataReadingThread should be started after the correct NatNet
    // Version has been set, inside the SetVersions method.

    // Start the thread after all the sockets are ready
    commandReadingThread_.start();
}

void ClientCore::BasicThread::initExceptionMap()
{
    exceptionsMap_[RUNTIME_ERROR] = NULL;
    exceptionsMap_[INVALID_ARGUMENT] = NULL;
}

std::runtime_error ClientCore::BasicThread::processExceptions_unknownExceptionID_Exception(int ID)
{
    std::ostringstream oss;
    oss << "Error while processing exceptions from a thread." << std::endl <<
           "Unknown exception ID " << ID << "!";
    return std::runtime_error(oss.str());
}

void ClientCore::BasicThread::riseException(std::exception* e)
{
    exceptionRised_ = true;
    if(std::runtime_error* v = dynamic_cast<std::runtime_error*>(e)) {
        exceptionsMap_[RUNTIME_ERROR] = new std::runtime_error(*v);
    } else if(std::invalid_argument* v = dynamic_cast<std::invalid_argument*>(e)) {
        exceptionsMap_[INVALID_ARGUMENT] = new std::invalid_argument(*v);
    } else if(std::domain_error* v = dynamic_cast<std::domain_error*>(e)) {
        exceptionsMap_[DOMAIN_ERROR] = new std::domain_error(*v);
    } else {    // Unknown exception
        exceptionsMap_[EXCEPTION] = new std::exception(*e);
    }
}

ClientCore::BasicThread::BasicThread(ClientCore *owner) :
    owner_(owner),
    exitThread_(false),
    waitingTimeout_(false),
    secTimeout_(1),
    uSecTimeout_(0),
    exceptionRised_(false)
{
    initExceptionMap();
}

ClientCore::BasicThread::~BasicThread()
{
    exit();
    wait();
    exitThread_ = false;
}

void ClientCore::BasicThread::exit()
{
    exitThread_ = true;
}

int ClientCore::BasicThread::seTimeout(unsigned int secTimeout, unsigned int uSecTimeout)
{
    secTimeout_ = secTimeout;
    uSecTimeout_ = uSecTimeout;
    return 0;
}

void ClientCore::BasicThread::getTimeout(unsigned int& outSecTimeout, unsigned int& outuSecTimeout)
{
    outSecTimeout = secTimeout_;
    outuSecTimeout = uSecTimeout_;
}

void ClientCore::DataReadingThread::run()
{
    try {
        char  szData[20000];
        do{
            LCGDEBUG() << "Data reading thread" << std::endl;
            int retValue;
            retValue = owner_->dataSocket_.readDatagram(szData, sizeof(szData));
            if(retValue == TRYAGAIN){
                waitingTimeout_ = true;
                continue;
            }
            waitingTimeout_ = false;
            LCGDEBUG() << "Data arriving!" << std::endl;
            owner_->frame_.processData(szData);
        }while(!exitThread_);
    } catch(std::exception& e) {    // For UdpSocket::readDatagram, Frame::processData
        riseException(&e);
    }
    exitThread_ = false; // Ready to start again
}

ClientCore::DataReadingThread::DataReadingThread(ClientCore* owner) :
    BasicThread(owner)
{
    this->seTimeout(secTimeout_, uSecTimeout_);
}

int ClientCore::DataReadingThread::seTimeout(unsigned int secTimeout, unsigned int uSecTimeout)
{
    BasicThread::seTimeout(secTimeout, uSecTimeout);
    owner_->dataSocket_.setReadingTimeout(secTimeout, uSecTimeout);
}

bool ClientCore::BasicThread::waitingTimeoutReached()
{
    return waitingTimeout_;
}

void ClientCore::BasicThread::processExceptions()
{
    if(!exceptionRised_) {  // Check fast if an exception has been rised on the thread
        return;
    }
    for(std::map<ExceptionID, std::exception*>::iterator it=exceptionsMap_.begin();
        it != exceptionsMap_.end(); it++) {
        std::exception* tmpException = it->second;
        if(tmpException != NULL) {  // An exception was rised on the thread
            exit();
            exceptionRised_ = false;
            ExceptionID exceptionID = it->first;
            // The reaseon to not just perform a throw *dynamic_cast<std::runtime_error*>(tmpException);
            // is to delete the exception, effectively consuming it.
            // In C++11, with exception_ptr, the code repetition is not needed
            std::runtime_error* pRuntimeError;
            std::invalid_argument* pInvalidArgument;
            std::domain_error* pDomainError;
            std::exception* pException;
            std::runtime_error runtimeError("");
            std::invalid_argument invalidArgument("");
            std::domain_error domainError("");
            std::exception exception;
            switch(exceptionID) {
            case RUNTIME_ERROR:
                pRuntimeError = dynamic_cast<std::runtime_error*>(tmpException);
                exceptionsMap_[exceptionID] = NULL;
                runtimeError = *pRuntimeError;
                delete pRuntimeError;
                throw runtimeError;
                break;
            case INVALID_ARGUMENT:
                pInvalidArgument = dynamic_cast<std::invalid_argument*>(tmpException);
                exceptionsMap_[exceptionID] = NULL;
                invalidArgument = *pInvalidArgument;
                delete pInvalidArgument;
                throw invalidArgument;
                break;
            case DOMAIN_ERROR:
                pDomainError = dynamic_cast<std::domain_error*>(tmpException);
                exceptionsMap_[exceptionID] = NULL;
                domainError = *pDomainError;
                delete pDomainError;
                throw domainError;
                break;
            case EXCEPTION:
                pException = tmpException;
                exceptionsMap_[exceptionID] = NULL;
                exception = *pException;
                delete pException;
                throw exception;
                break;
            default:
                throw processExceptions_unknownExceptionID_Exception(exceptionID);
            }
        }
    }
}

ClientCore::Frame::Frame(const ClientCore* owner) :
    owner_(owner),
    model_(this),
    unpacker_(this),
    nLastFrameNumber_(0),
    nOldFrameNumber_(0),
    timestamp_(0.0d),
    bIsRecording_(false),
    bHaveTrackedModelsChanged_(false)
{
}

void ClientCore::Frame::processData(const char* pRawData)
{
    const char* ptr = pRawData;

    LCGDEBUG() << "Begin Packet" << std::endl << "-------" << std::endl;

    // message ID
    int MessageID = 0;
    std::memcpy(&MessageID, ptr, 2); ptr += 2;
    LCGDEBUG() << "Message ID : " << MessageID << std::endl;

    // size
    nBytes_ = 0;
    std::memcpy(&nBytes_, ptr, 2); ptr += 2;
    LCGDEBUG() << "Byte count : " << nBytes_ << std::endl;

//    std::ofstream myFile;
//    myFile.open ("skeletonAndRigidBody_data.bin", std::ios::out | std::ios::binary);
//    myFile.write(pData, nBytes+4);

    if(MessageID == 7) {    // FRAME OF MOCAP DATA packet

        unpacker_.unpackAndUpdateFrame(ptr);

        LCGDEBUG() << "End of Packet" << std::endl << "-------------" << std::endl;

        // Update the last frame number at the end of the packet, so that
        // hasFrameChanged() returns true after the packet has been decoded
        // TODO: Add updateFrame() method
        nLastFrameNumber_ = unpacker_.getFrameNumber();
    }                       // FRAME OF MOCAP DATA packet
    else if(MessageID == 5) { // Data Descriptions
        model_.unpackAndDefineDefinition(ptr);
    }
    else {
        LCGDEBUG() << "Unrecognized Packet Type." << std::endl;
    }

}

bool ClientCore::Frame::hasFrameChanged()
{
    if(nOldFrameNumber_ != nLastFrameNumber_){   // Frame has changed
        nOldFrameNumber_ = nLastFrameNumber_;
        return true;
    } else {
        return false;
    }
}

int ClientCore::Frame::getFrameNumber()
{
    return nLastFrameNumber_;
}

void ClientCore::Frame::getTimecode(unsigned int& outTimecode, unsigned int& outTimecodeSubFrame)
{
    outTimecode = timecode_;
    outTimecodeSubFrame = timecodeSubFrame_;
}

std::string ClientCore::Frame::getTimecodeString()
{
    std::string strTimecode;
    unpacker_.timecodeStringify(timecode_, timecodeSubFrame_, strTimecode);
    return strTimecode;
}

double ClientCore::Frame::getTimestamp()
{
    return timestamp_;
}

double ClientCore::Frame::getLatency()
{
    return unpacker_.getLatency();
}

void ClientCore::Frame::resetLatency()
{
    unpacker_.resetLatency();
}

bool ClientCore::Frame::isServerRecording()
{
    return bIsRecording_;
}

bool ClientCore::Frame::haveTrackedModelsChanged()
{
    return bHaveTrackedModelsChanged_;
}

ClientCore::Frame::Model* ClientCore::Frame::getModel()
{
    return &model_;
}

ClientCore::CommandProcessor::CommandProcessor(ClientCore *owner) :
    owner_(owner)
{
}

// handle command
void ClientCore::CommandProcessor::processData(const sPacket* pPacket)
{
    switch (pPacket->iMessage)
    {
    case NAT_MODELDEF:
        owner_->frame_.processData((char*)pPacket);
        break;
    case NAT_FRAMEOFDATA:
        owner_->frame_.processData((char*)pPacket);
        break;
    case NAT_PINGRESPONSE:
        LCGDEBUG() << owner_->appID << " Received 'NAT_PINGRESPONSE'" << std::endl;
        owner_->commandProcessor_.unpackAndUpdateVersions((char*)pPacket);
        break;
    case NAT_RESPONSE:
        {
            char* szResponse = (char *)pPacket->Data.cData;
            LCGDEBUG() << "Response : " << szResponse;
            break;
        }
    case NAT_MESSAGESTRING:
        LCGDEBUG() << owner_->appID << " Received message: " << pPacket->Data.szData << std::endl;
        break;
    case NAT_UNRECOGNIZED_REQUEST:
    default:
        LCGDEBUG() << owner_->appID << " Received 'unrecognized request'" << std::endl;
        break;
    }
}

void ClientCore::CommandProcessor::unpackAndUpdateVersions(const char* pRawData)
{
//    // This can be used in conjunction with precompiler directive:
//    // #pragma pack(push, 1)
//    // See .h file for more info.
//    for(int i=0; i<4; i++){
//        NatNetVersion_[i] = (int)PacketIn.Data.Sender.NatNetVersion[i];
//        ServerVersion_[i] = (int)PacketIn.Data.Sender.Version[i];
//    }

    // This is the portable alternative to directly access the sPacket structure fields
    // The pointer needs to traverse the datagram manually.
    pRawData += 2;   // message ID
    pRawData += 2;   // payload size
    // sending's app name
    owner_->serverName_ = std::string(pRawData);
    LCGDEBUG() << "Server app name:" << owner_->serverName_ << std::endl;
    pRawData += MAX_NAMELENGTH;
    // sending app's version [major.minor.build.revision]
    for(int i=0; i<4; i++){
        owner_->serverVersion_.fullVersion[i] = (int)*pRawData; pRawData++;
    }
    // sending app's NatNet version [major.minor.build.revision]
    for(int i=0; i<4; i++){
        owner_->natNetVersion_.fullVersion[i] = (int)*pRawData; pRawData++;
    }

    LCGDEBUG() << "Server version: " << owner_->serverVersion_ << std::endl;
    LCGDEBUG() << "NatNet version: " << owner_->natNetVersion_ << std::endl;

    // Exit the commandReadingThread, in order to minimize the time spent
    // waiting for the ping response to arrive.
    //commandReadingThread_.exit();
    owner_->state_ = GETTING_MODEL_DEF;
}

void ClientCore::requestVersions()
{
    state_ = GETTING_VERSION;

//    std::ofstream myFile;
//    myFile.open ("dataPing.bin", std::ios::out | std::ios::binary);
//    myFile.write((char *)&packetOut, 4 + packetOut.nDataBytes);

    int nTries = 3;
    for(int i=0; i < nTries; i++){
        sendCommand(PING);  // send initial ping command

        commandReadingThread_.start();
        commandReadingThread_.exit();
        commandReadingThread_.wait();   //Wait unit data
        if(state_ == GETTING_MODEL_DEF){
            break;
        }
    }
    if(state_ == GETTING_MODEL_DEF){
        commandReadingThread_.start();
        LCGDEBUG() << "Client ready for getting model definitions!" << std::endl;
    } else {
        LCGDEBUG() << "Init communication failed" << std::endl;
    }
}

void ClientCore::autoConnectToServer()
{
    LCGDEBUG() << "Attempting autoconnect to server." << std::endl;

    // Wait for datagram on WaitForServerThread
    waitForServerThread_.start();
    waitForServerThread_.exit();
    waitForServerThread_.wait();
    if(state_ != GETTING_MODEL_DEF){
        LCGDEBUG() << "Autoconnection failed! Timeout on receiving frame of data" << std::endl;
    } else {
        LCGDEBUG() << "Autoconnection successful!" << std::endl;
    }
}

int ClientCore::checkConnectionStatus()
{
    if(state_ == CONNECTED){
        return 0; // Initialization completed
    } else {
        return -1; // Connecting still in process
    }
}

int ClientCore::updateModelDefinition()
{
    switch (state_) {
    case CONNECTED: // Stop data thread until the new definition is ready
        dataReadingThread_.exit();
        dataReadingThread_.wait();
        state_ = GETTING_MODEL_DEF;
    case GETTING_MODEL_DEF: {
        frame_.getModel()->deleteDefinition();
        int nTries = 3;
        for(int i=0; i < nTries; i++){
            sendCommand(MODEL);
            commandReadingThread_.start();
            commandReadingThread_.exit();
            commandReadingThread_.wait();   //Wait unit model definition arrives
            if(frame_.getModel()->hasDefinition()){
                break;
            }
        }
        if(frame_.getModel()->hasDefinition()){
            state_ = CONNECTED;
            // Start the dataReadingThread, just after the Model Definition has been
            // established, in order to avoid reading data before the correct definition
            // version has been set.
            dataReadingThread_.start();
            // Start the commandReadingThread after the state has been defined to CONNECTED
            // in order to avoid an attempt of filling the model definition while it
            // is being used by the dataReadingThread.
            commandReadingThread_.start();


            LCGDEBUG() << "Model Definition Obtained!" << std::endl;
            return 0;
        } else {
            // Start the commandReadingThread in case that a new attempt of receiving
            // the model definition is made.
            commandReadingThread_.start();
            LCGDEBUG() << "updateModelDefinition failed" << std::endl;
            return -1;
        }
        break;
    }   // This block is needed to avoid the cross initalization of nTries variable
    case GETTING_VERSION:
    case NOT_CONNECTED: // First connect, then get model definition, throw exception
        return -1;
        break;
    default:
        return -2;
    }
}

void ClientCore::processThreadsExceptions()
{
    dataReadingThread_.processExceptions();
    commandReadingThread_.processExceptions();
    waitForServerThread_.processExceptions();
}

bool ClientCore::Frame::Model::hasDefinition()
{
    // If there is only one Dataset, there is a model Definition
    return !mAllDatasets.empty();
}

ClientCore::Frame::Model::Model(const ClientCore::Frame* owner) :
    owner_(owner)
{
}

int ClientCore::Frame::Model::unpackAndDefineDefinition(const char* rawData)
{
    if(owner_->owner_->state_ != GETTING_MODEL_DEF){ // If not getting a model definition
        return -1;
    }

    deleteDefinition();    // Delte previous model definition in order to avoid ambiguities
    const char* ptr = rawData;
    // number of datasets
    int nDatasets = 0; std::memcpy(&nDatasets, ptr, 4); ptr += 4;
    LCGDEBUG() << "Dataset Count : " << nDatasets << std::endl;

    for(int i=0; i < nDatasets; i++)
    {
        LCGDEBUG() << "Dataset " << i << std::endl;
        int type = 0; std::memcpy(&type, ptr, 4); ptr += 4;
        LCGDEBUG() << "Type : " << type; // Formating continues on type specific code


        if(type == 0)   // markerset
        {
            Markerset* newMarkerset = new Markerset;

            LCGDEBUG() << " (Markerset)" << std::endl;
            // name
            std::string markersetName(ptr);
            int nDataBytes = markersetName.length() + 1;
            ptr += nDataBytes;
            LCGDEBUG() << "Markerset Name: " << markersetName << std::endl;
            newMarkerset->setName(markersetName);

            // marker data
            int nMarkers = 0; std::memcpy(&nMarkers, ptr, 4); ptr += 4;
            LCGDEBUG() << "Marker Count : " << nMarkers << std::endl;
            newMarkerset->setID(mNameToMarkerset.size());
            for(int j=0; j < nMarkers; j++)
            {
                Marker newMarker;
                std::string markerName(ptr);
                int nDataBytes = markerName.length() + 1;
                ptr += nDataBytes;
                LCGDEBUG() << "Marker Name: " << markerName << std::endl;
                newMarker.setName(markerName);
                newMarker.setID(j);
                newMarkerset->addMarker(newMarker);
            }
            addMarkerset(newMarkerset);
        } // markerset
        else if(type ==1)   // rigid body
        {
            RigidBody* newRigidBody = new RigidBody;
            LCGDEBUG() << " (Rigid body)" << std::endl;
            // 2.0 and later
            if(owner_->owner_->natNetVersion_ >= lcgVersion(2))
            {
                // name
                std::string szName(ptr);
                ptr += szName.length() + 1;
                LCGDEBUG() << "Rigid Body Name: " << szName << std::endl;
                newRigidBody->setName(szName);
            }

            int ID = 0; std::memcpy(&ID, ptr, 4); ptr +=4;
            LCGDEBUG() << "Rigid Body ID : " << ID << std::endl;
            newRigidBody->setID(ID);

            int parentID = 0; std::memcpy(&parentID, ptr, 4); ptr +=4;
            LCGDEBUG() << "Parent ID : " << parentID << std::endl;
            newRigidBody->setParentID(parentID);

            float fXOffset = 0; std::memcpy(&fXOffset, ptr, 4); ptr +=4;
            LCGDEBUG() << "X Offset : " << fXOffset << std::endl;

            float fYOffset = 0; std::memcpy(&fYOffset, ptr, 4); ptr +=4;
            LCGDEBUG() << "Y Offset : " << fYOffset << std::endl;

            float fZOffset = 0; std::memcpy(&fZOffset, ptr, 4); ptr +=4;
            LCGDEBUG() << "Z Offset : " << fZOffset << std::endl;

            newRigidBody->setOffset(Position(fXOffset, fYOffset, fZOffset));

            addRigidBody(newRigidBody);
        } // rigid body
        else if(type ==2)   // skeleton
        {
            Skeleton* newSkeleton = new Skeleton;
            LCGDEBUG() << " (Skeleton)" << std::endl;

            std::string szName(ptr);
            ptr += szName.length() + 1;
            LCGDEBUG() << "Skeleton Name: " << szName << std::endl;
            newSkeleton->setName(szName);

            int ID = 0; std::memcpy(&ID, ptr, 4); ptr +=4;
            LCGDEBUG() << "Skeleton ID : " << ID << std::endl;
            newSkeleton->setID(ID);

            int nRigidBodies = 0; std::memcpy(&nRigidBodies, ptr, 4); ptr +=4;
            LCGDEBUG() << "RigidBody (Bone) Count : " << nRigidBodies << std::endl;
            newSkeleton->setNumberOfElements(nRigidBodies);

            for(int i=0; i< nRigidBodies; i++)
            {
                RigidBody newBone;
                // 2.0 and later
                if(owner_->owner_->natNetVersion_ >= lcgVersion(2))
                {
                    // RB name
                    std::string szName(ptr);
                    ptr += szName.length() + 1;
                    LCGDEBUG() << "Rigid Body Name: " << szName << std::endl;
                    newBone.setName(szName);
                }

                int ID = 0; std::memcpy(&ID, ptr, 4); ptr +=4;
                LCGDEBUG() << "RigidBody ID : " << ID << std::endl;
                newBone.setID(ID);

                int parentID = 0; std::memcpy(&parentID, ptr, 4); ptr +=4;
                LCGDEBUG() << "Parent ID : " << parentID << std::endl;
                newBone.setParentID(parentID);

                float xoffset = 0; std::memcpy(&xoffset, ptr, 4); ptr +=4;
                LCGDEBUG() << "X Offset : " << xoffset << std::endl;

                float yoffset = 0; std::memcpy(&yoffset, ptr, 4); ptr +=4;
                LCGDEBUG() << "Y Offset : " << yoffset << std::endl;

                float zoffset = 0; std::memcpy(&zoffset, ptr, 4); ptr +=4;
                LCGDEBUG() << "Z Offset : " << zoffset << std::endl;
                newBone.setOffset(Position(xoffset, yoffset, zoffset));
                newSkeleton->addBone(newBone);
            }
            addSkeleton(newSkeleton);
        } // skeleton

    }   // next dataset
    addUnidentifiedMarkersDefinition();
    addLabeledMarkersDefinition();

    // Note: The Data Descriptions packet doesn't end with an EOD (end of data tag),
    // it just ends after the last byte of data is delivered.
    LCGDEBUG() << "End Packet" << std::endl << "-------------" << std::endl;
}

void ClientCore::Frame::Model::addMarkerset(Markerset* newMarkerset)
{
    mAllDatasets.push_back(newMarkerset);
    mNameToMarkerset[newMarkerset->getName()] = newMarkerset;
    mIDtoMarkerset[newMarkerset->getID()] = newMarkerset;
}

void ClientCore::Frame::Model::addRigidBody(RigidBody* newRigidBody)
{
    mAllDatasets.push_back(newRigidBody);
    mNameToRigidBody[newRigidBody->getName()] = newRigidBody;
    mIDtoRigidBody[newRigidBody->getID()] = newRigidBody;
}

void ClientCore::Frame::Model::addSkeleton(Skeleton* newSkeleton)
{
    mAllDatasets.push_back(newSkeleton);
    mNameToSkeleton[newSkeleton->getName()] = newSkeleton;
    mIDtoSkeleton[newSkeleton->getID()] = newSkeleton;
}

void ClientCore::Frame::Model::addUnidentifiedMarkersDefinition()
{
    Markerset* unidentifiedMarkerset = new Markerset;
    unidentifiedMarkerset->setID(-1);
    unidentifiedMarkerset->setName("_Unidentified_");
    addMarkerset(unidentifiedMarkerset);
}

void ClientCore::Frame::Model::addLabeledMarkersDefinition()
{
    Markerset* labeledMarkerset = new Markerset;
    labeledMarkerset->setID(-2);
    labeledMarkerset->setName("_Labeled_");
    addMarkerset(labeledMarkerset);
}

void ClientCore::Frame::Model::deleteDefinition()
{
    if(mAllDatasets.size() > 0) {
        for(std::vector<Dataset*>::iterator datasetIt = mAllDatasets.begin();
            datasetIt != mAllDatasets.end(); datasetIt++) {
            Dataset* pDataset = *datasetIt;
            if(pDataset) {
                delete pDataset;
            }
        }
        // Erase after traversal, not while doing it, in order to avoid the problem
        // of elements moving around.
        mAllDatasets.erase(mAllDatasets.begin(), mAllDatasets.end());
    }
    if(mNameToMarkerset.size() > 0) {
        mNameToMarkerset.erase(mNameToMarkerset.begin(), mNameToMarkerset.end());
    }
    if(mIDtoMarkerset.size() > 0) {
        mIDtoMarkerset.erase(mIDtoMarkerset.begin(), mIDtoMarkerset.end());
    }
    if(mNameToRigidBody.size() > 0) {
        mNameToRigidBody.erase(mNameToRigidBody.begin(), mNameToRigidBody.end());
    }
    if(mIDtoRigidBody.size() > 0) {
        mIDtoRigidBody.erase(mIDtoRigidBody.begin(), mIDtoRigidBody.end());
    }
    if(mNameToSkeleton.size() > 0) {
        mNameToSkeleton.erase(mNameToSkeleton.begin(), mNameToSkeleton.end());
    }
    if(mIDtoSkeleton.size() > 0) {
        mIDtoSkeleton.erase(mIDtoSkeleton.begin(), mIDtoSkeleton.end());
    }
}

ClientCore::Frame::Unpacker::Unpacker(ClientCore::Frame* owner) :
    EOD_TAG_(0),
    owner_(owner),
    badLatency_(0.0f),
    calculatedLatency_(0.0d),
    firstLatency_(true)
{
}

void ClientCore::Frame::Unpacker::unpackAndUpdateFrame(const char*& pRawFrameData)
{
    // The order of this methods shouldn't be changed, because they also move the pointer
    // and the offset of this movement can't be known in advance without traversing the buffer.
    checkIfPacketIsValid(pRawFrameData);                // Check if packet is not corrupted
    unpackAndUpdateFrameNumber(pRawFrameData);          // Frame number
    unpackAndUpdateMarkersets(pRawFrameData);           // Identified Markersets (rigidbodies, skeletons, all)
    unpackAndUpdateUnidentifiedMarkers(pRawFrameData);  // Unidentified Markers
    unpackAndUpdateRigidBodies(pRawFrameData);          // Rigid bodies
    unpackAndUpdateSkeletons(pRawFrameData);            // Skeletons
    unpackAndUpdateLabeledMarkers(pRawFrameData);       // Labeled markers
    unpackAndUpdateLatency(pRawFrameData);              // Packet "bad" Latency (equal to timestamp)
    unpackAndUpdateTimecode(pRawFrameData);             // Timecode and TimecodeSubFrame
    unpackAndUpdateTimestamp(pRawFrameData);            // TimeStamp
    unpackAndUpdateFrameParameters(pRawFrameData);      // Frame parameters like: bIsRecording and bTrackedModelChanged
    unpackAndGetEODTag(pRawFrameData);                  // EOD (check if packet is corrupted)
}

int ClientCore::Frame::Unpacker::getFrameNumber()
{
    return tmpFrameNumber_;
}

double ClientCore::Frame::Unpacker::getLatency()
{
    return calculatedLatency_;  // This can be changed to badLatency_ if its fixed in server future releases
}

void ClientCore::Frame::Unpacker::resetLatency()
{
    firstLatency_ = true;
}

void ClientCore::Frame::Unpacker::unpackAndUpdateFrameNumber(const char*& pRawData)
{
    // frame number
    std::memcpy(&tmpFrameNumber_, pRawData, 4); pRawData += 4;
    LCGDEBUG() << "Frame # : " << tmpFrameNumber_ << std::endl;
}

void ClientCore::Frame::Unpacker::unpackAndUpdateMarkersets(const char*& pRawData)
{
    // number of Identified Markersets (rigidbodies, skeletons, all)
    int nMarkerSets = 0; std::memcpy(&nMarkerSets, pRawData, 4); pRawData += 4;
    LCGDEBUG() << "Identified Markerset Count : " << nMarkerSets << std::endl;

    for (int i=0; i < nMarkerSets; i++)
    {
        // Markerset name
        std::string szName(pRawData);
        int nDataBytes = (int) szName.length() + 1;
        pRawData += nDataBytes;
        LCGDEBUG() << "Model Name: " << szName << std::endl;

        Markerset* pMarkerset = owner_->model_.getMarkersetFromName(szName);
        if(pMarkerset < NULL){    // Markerset not defined in the model
            throw errorAttemptToFillUndefinedMarkersetException(szName);
        }

        // marker data
        int nMarkers = 0; std::memcpy(&nMarkers, pRawData, 4); pRawData += 4;
        LCGDEBUG() << "Marker Count : " << nMarkers << std::endl;
        if(pMarkerset->getNumberOfElements() != nMarkers){  // Misalignment error
            throw errorMarkersetMisalignmentErrorException(szName, nMarkers);
        }

        float x,y,z;
        for(int j=0; j < nMarkers; j++)
        {
            Marker tmpMarker = pMarkerset->getMarkerFromID(j);
            std::memcpy(&x, pRawData, 4); pRawData += 4;
            std::memcpy(&y, pRawData, 4); pRawData += 4;
            std::memcpy(&z, pRawData, 4); pRawData += 4;
            LCGDEBUG() << "\tMarker " << j << " : [x=" << x << ",y=" <<
                         y << ",z=" << z << "]" << std::endl;
            tmpMarker.setPosition(x, y, z);
            pMarkerset->addMarker(tmpMarker);
        }
    }
}

void ClientCore::Frame::Unpacker::unpackAndUpdateUnidentifiedMarkers(const char *&pRawData)
{
    Markerset* unidentifiedMarkerset = owner_->model_.getMarkersetFromName("_Unidentified_");

    // Just clear the unidentified markerset if the number of new unidentified markers is bigger than the old one
    int nOtherMarkers = 0; std::memcpy(&nOtherMarkers, pRawData, 4); pRawData += 4;
    LCGDEBUG() << "Unidentified Marker Count : " << nOtherMarkers << std::endl;
    if(unidentifiedMarkerset->getNumberOfElements() > nOtherMarkers) {
        unidentifiedMarkerset->clear();
    }

    std::ostringstream oss;
    float x,y,z;
    for(int j=0; j < nOtherMarkers; j++)
    {
        Marker tmpMarker;
        tmpMarker.setID(j);
        oss << "Unidentified_" << j+1;
        tmpMarker.setName(oss.str());
        memcpy(&x, pRawData, 4); pRawData += 4;
        memcpy(&y, pRawData, 4); pRawData += 4;
        memcpy(&z, pRawData, 4); pRawData += 4;
        LCGDEBUG() << "\tMarker " << j << " : pos = [" << x << "," <<
                     y << "," << z << "]" << std::endl;
        tmpMarker.setPosition(x,y,z);
        unidentifiedMarkerset->addMarker(tmpMarker);
        oss.str(std::string());  // Clear oss
    }
}

void ClientCore::Frame::Unpacker::unpackAndUpdateRigidBodies(const char *&pRawData, Skeleton* pSkeleton)
{
    // # of Rigid bodies (Bones if in a Skeleton)
    int nRigidBodies = 0;
    std::memcpy(&nRigidBodies, pRawData, 4); pRawData += 4;
    LCGDEBUG() << "Rigid Body Count : " << nRigidBodies << std::endl;
    for (int j=0; j < nRigidBodies; j++) // For every RigidBody (Bone if in a skeleton)
    {
        // rigid body pos/ori

        // Issue #5 Fix: Just read a short int (2 bytes of data) from
        // the data packet for the ID.
        // NOTE: The other 2 bytes seem to be the Skeleton ID.
        // Move the data pointer 4 bytes in order to jump to the
        // rigid body data.
        int ID = 0; std::memcpy(&ID, pRawData, 2); pRawData += 4; // Issue #5 Fix.
        float x = 0.0f; std::memcpy(&x, pRawData, 4); pRawData += 4;
        float y = 0.0f; std::memcpy(&y, pRawData, 4); pRawData += 4;
        float z = 0.0f; std::memcpy(&z, pRawData, 4); pRawData += 4;
        float qx = 0; std::memcpy(&qx, pRawData, 4); pRawData += 4;
        float qy = 0; std::memcpy(&qy, pRawData, 4); pRawData += 4;
        float qz = 0; std::memcpy(&qz, pRawData, 4); pRawData += 4;
        float qw = 0; std::memcpy(&qw, pRawData, 4); pRawData += 4;
        LCGDEBUG() << "ID : " << ID << std::endl;
        LCGDEBUG() << "pos: [" << x << "," << y << "," << z << "]" << std::endl;
        LCGDEBUG() << "ori: [" << qx << "," << qy << "," << qz << "," << qw << "]" << std::endl;

        RigidBody* pRigidBody = NULL;
        RigidBody skeletonBone;
        if( pSkeleton == NULL) {        // If the RigidBody doesn't belong to a Skeleton
            pRigidBody = owner_->model_.getRigidBodyFromID(ID);
        } else {                        // If the RigidBody belongs to a Skeleton (It's a Bone)
            skeletonBone = pSkeleton->getBoneFromID(ID);
            pRigidBody = &skeletonBone;
        }

        if(pRigidBody < NULL){    // RigidBody not defined in the model
            throw errorAttemptToFillUndefinedRigidBodyFromIDException(ID);
        }
        pRigidBody->setOrientation(qx,qy,qz,qw);
        pRigidBody->setPosition(x,y,z);

        // associated marker positions
        int nRigidMarkers = 0;  std::memcpy(&nRigidMarkers, pRawData, 4); pRawData += 4;
        LCGDEBUG() << "Marker Count: " << nRigidMarkers << std::endl;
        int nBytes = nRigidMarkers*3*sizeof(float);
        float* markerData = new float[nBytes];
        std::memcpy(markerData, pRawData, nBytes);
        pRawData += nBytes;

        // 2.0 and later
        if(owner_->owner_->natNetVersion_ >= lcgVersion(2)) // New data format
        {
            // associated marker IDs
            nBytes = nRigidMarkers*sizeof(int);
            int* markerIDs = new int[nBytes];
            std::memcpy(markerIDs, pRawData, nBytes);
            pRawData += nBytes;

            // associated marker sizes
            nBytes = nRigidMarkers*sizeof(float);
            float* markerSizes = new float[nBytes];
            std::memcpy(markerSizes, pRawData, nBytes);
            pRawData += nBytes;

            for(int k=0; k < nRigidMarkers; k++)
            {
                LCGDEBUG() << "\tMarker " << k << ": id=" << markerIDs[k] <<
                             "\tsize=" << markerSizes[k] << "\tpos=[" <<
                             markerData[k*3] << "," << markerData[k*3+1] <<
                             "," << markerData[k*3+2] << "]" << std::endl;

                Marker tmpMarker;
                tmpMarker.setID(k);
                tmpMarker.setName(core::Number::intToString(markerIDs[k]));
                tmpMarker.setSize(markerSizes[k]);
                tmpMarker.setPosition(markerData[k*3], markerData[k*3+1], markerData[k*3+2]);
                pRigidBody->addMarker(tmpMarker);
            }

            // release resources
            if(markerIDs)
                delete[] markerIDs;
            if(markerSizes)
                delete[] markerSizes;

        }
        else    // Old data format
        {
            for(int k=0; k < nRigidMarkers; k++)
            {
                LCGDEBUG() << "\tMarker " << k << ": pos = [" <<
                             markerData[k*3] << "," << markerData[k*3+1] <<
                             "," << markerData[k*3+2] << "]" << std::endl;

                Marker tmpMarker;
                tmpMarker.setID(k);
                tmpMarker.setPosition(markerData[k*3], markerData[k*3+1], markerData[k*3+2]);
                pRigidBody->addMarker(tmpMarker);
            }
        }
        if(markerData)
            delete[] markerData;

        // 2.0 and later
        if( owner_->owner_->natNetVersion_ >= lcgVersion(2) ) {
            // Mean marker error
            float fMeanMarkerError = 0.0f; std::memcpy(&fMeanMarkerError, pRawData, 4); pRawData += 4;
            LCGDEBUG() << "Mean marker error: " << fMeanMarkerError << std::endl;
            pRigidBody->setMeanMarkerError(fMeanMarkerError);
        }

        // 2.6 and later
        if( owner_->owner_->natNetVersion_ >= lcgVersion(2,6) ) {
            // params
            short params = 0; std::memcpy(&params, pRawData, 2); pRawData += 2;
            bool bTrackingValid = params & 0x01; // 0x01 : rigid body was successfully tracked in this frame
            LCGDEBUG() << "Traking valid?: " << bTrackingValid << std::endl;
            pRigidBody->setIsTrackingValid(bTrackingValid);
        }
        if(pSkeleton != NULL){
            pSkeleton->addBone(*pRigidBody);     // If the RigidBody belongs to a Skeleton (It's a Bone)
        }

    } // Next RigidBody (Bone if in a Skeleton)
}

void ClientCore::Frame::Unpacker::unpackAndUpdateSkeletons(const char *&pRawData)
{
    // skeletons (version 2.1 and later)
    if( owner_->owner_->natNetVersion_ >= lcgVersion(2,1) ) {
        int nSkeletons = 0;
        std::memcpy(&nSkeletons, pRawData, 4); pRawData += 4;
        LCGDEBUG() << "Skeleton Count : " << nSkeletons << std::endl;
        for (int j=0; j < nSkeletons; j++)
        {
            // skeleton id
            int skeletonID = 0;
            std::memcpy(&skeletonID, pRawData, 4); pRawData += 4;
            LCGDEBUG() << "Skeleton ID: " << skeletonID << std::endl;
            Skeleton* pSkeleton = owner_->model_.getSkeletonFromID(skeletonID);
            if(pSkeleton < NULL){    // RigidBody not defined in the model
                throw errorAttemptToFillUndefinedSkeletonFromIDException(skeletonID);
            }

            unpackAndUpdateRigidBodies(pRawData, pSkeleton);

        } // next skeleton
    } // skeletons (version 2.1 and later)
}

void ClientCore::Frame::Unpacker::unpackAndUpdateLabeledMarkers(const char*& pRawData)
{
    // labeled markers (version 2.3 and later)
    if( owner_->owner_->natNetVersion_ >= lcgVersion(2,3) ) {
        Markerset* labeledMarkerset = owner_->model_.getMarkersetFromName("_Labeled_");

        int nLabeledMarkers = 0;
        std::memcpy(&nLabeledMarkers, pRawData, 4); pRawData += 4;
        LCGDEBUG() << "Labeled Marker Count : " << nLabeledMarkers << std::endl;
        if(labeledMarkerset->getNumberOfElements() > nLabeledMarkers) {
            labeledMarkerset->clear();
        }

        std::ostringstream oss;
        for (int j=0; j < nLabeledMarkers; j++)
        {
            // id
            int ID = 0; std::memcpy(&ID, pRawData, 4); pRawData += 4;
            // x
            float x = 0.0f; std::memcpy(&x, pRawData, 4); pRawData += 4;
            // y
            float y = 0.0f; std::memcpy(&y, pRawData, 4); pRawData += 4;
            // z
            float z = 0.0f; std::memcpy(&z, pRawData, 4); pRawData += 4;
            // size
            float size = 0.0f; std::memcpy(&size, pRawData, 4); pRawData += 4;

            Marker tmpMarker;
            tmpMarker.setID(ID);
            tmpMarker.setPosition(x, y, z);
            tmpMarker.setSize(size);

            oss << "Labeled_" << ID;
            tmpMarker.setName(oss.str());
            oss.str(std::string()); // Clear oss

            // 2.6 and later
            if( owner_->owner_->natNetVersion_ >= lcgVersion(2,6) ) {
                // marker params
                short params = 0; std::memcpy(&params, pRawData, 2); pRawData += 2;
                bool bOccluded = params & 0x01;     // marker was not visible (occluded) in this frame
                bool bPCSolved = params & 0x02;     // position provided by point cloud solve
                bool bModelSolved = params & 0x04;  // position provided by model solve

                // Start from the most advanced position, to the visible one
                if(bModelSolved){
                    tmpMarker.setVisibility(Marker::MODEL_SOLVED);
                    LCGDEBUG() << "Visibility: MODEL_SOLVED " << Marker::MODEL_SOLVED << std::endl;
                } else if(bPCSolved) {
                    tmpMarker.setVisibility(Marker::POINT_CLOUD_SOLVED);
                    LCGDEBUG() << "Visibility: POINT_CLOUD_SOLVED " << Marker::POINT_CLOUD_SOLVED << std::endl;
                } else if(bOccluded) {
                    tmpMarker.setVisibility(Marker::OCCLUDED);
                    LCGDEBUG() << "Visibility: OCCLUDED " << Marker::OCCLUDED << std::endl;
                }
            }

            LCGDEBUG() << "ID  : " << ID << std::endl;
            LCGDEBUG() << "pos : [" << x << "," << y << "," << z << "]" << std::endl;
            LCGDEBUG() << "size: [" << size << "]" << std::endl;
            labeledMarkerset->addMarker(tmpMarker);
        }
    } // labeled markers (version 2.3 and later)
}

void ClientCore::Frame::Unpacker::unpackAndUpdateLatency(const char*& pRawData)
{
    // NOTE: The received timestamp and latency is the same
    // Latency is the capture computer's hardware timestamp for the given frame,
    // which is also displayed in Motive in the Camera Preview viewport when
    // camera info is enabled.
    memcpy(&badLatency_, pRawData, 4); pRawData += 4;
    LCGDEBUG() << "latency : " << badLatency_ << std::endl;
}

void ClientCore::Frame::Unpacker::unpackAndUpdateTimecode(const char*& pRawData)
{
    // timecode
    std::memcpy(&owner_->timecode_, pRawData, 4); pRawData += 4;
    std::memcpy(&owner_->timecodeSubFrame_, pRawData, 4); pRawData += 4;
}

void ClientCore::Frame::Unpacker::unpackAndUpdateTimestamp(const char*& pRawData)
{
    // timestamp
    // 2.7 and later - increased from single to double precision
    if( owner_->owner_->natNetVersion_ >= lcgVersion(2,7) ) {
        std::memcpy(&owner_->timestamp_, pRawData, 8); pRawData += 8;
    }
    else {
        float fTemp = 0.0f;
        std::memcpy(&fTemp, pRawData, 4); pRawData += 4;
        owner_->timestamp_ = (double)fTemp;
    }

    LCGDEBUG() << "timestamp: " << owner_->timestamp_ << std::endl;

    updateCalculatedLatencyFromTimestamp();        // The CalculatedLatency is calculated from the last and current timestamp
}

void ClientCore::Frame::Unpacker::updateCalculatedLatencyFromTimestamp()
{
    // Latency is zero the first time because lastTimestamp_ is not known from the first data packet
    if(firstLatency_) {
        firstLatency_ = false;
    } else {
        calculatedLatency_ = owner_->timestamp_ - owner_->lastTimestamp_;
    }
    owner_->lastTimestamp_ = owner_->timestamp_;
    LCGDEBUG() << "Latency: " << calculatedLatency_*1000 << " ms" << std::endl;
}

void ClientCore::Frame::Unpacker::unpackAndUpdateFrameParameters(const char*& pRawData)
{
    // frame params
    short params = 0;  std::memcpy(&params, pRawData, 2); pRawData += 2;
    owner_->bIsRecording_ = params & 0x01;                  // 0x01 Motive is recording
    owner_->bHaveTrackedModelsChanged_ = params & 0x02;         // 0x02 Actively tracked model list has changed

    LCGDEBUG() << "bIsRecording: " << ((owner_->bIsRecording_) ? "true" : "false") << std::endl;
    LCGDEBUG() << "bHaveTrackedModelsChanged: " << ((owner_->bHaveTrackedModelsChanged_) ? "true" : "false") << std::endl;
}

int_fast32_t ClientCore::Frame::Unpacker::unpackAndGetEODTag(const char*& pRawData)
{
    // End Of Data (EOD) tag
    int_fast32_t eodTag = 0; std::memcpy(&eodTag, pRawData, 4); pRawData += 4;
    if(eodTag != EOD_TAG_){
        throw errorFrameOfMOCAPDataCorruptedException(eodTag);
    }
    return eodTag;
}

void ClientCore::Frame::Unpacker::checkIfPacketIsValid(const char*& pRawData)
{

    const char* ptrToEODTag = pRawData + (owner_->nBytes_-1)-(4/*sizeof(int32_t)*/-1);  // Jump to the EOD tag at the end of the MOCAP packet
    unpackAndGetEODTag(ptrToEODTag);
}

std::runtime_error ClientCore::errorNoMarkersetFromNameException(std::string markersetName)
{
    std::ostringstream oss;
    oss << "Error while getting Markerset from name. "
        << "No Markerset found with the name: \"" << markersetName << "\".";
    throw std::runtime_error(oss.str());
}

std::runtime_error ClientCore::errorNoRigidBodyFromNameException(std::string rigidBodyName)
{
    std::ostringstream oss;
    oss << "Error while getting RigidBody from name." <<
           "No RigidBody found with the name: \"" << rigidBodyName << "\".";
    throw std::runtime_error(oss.str());
}

std::runtime_error ClientCore::errorNoSkeletonFromNameException(std::string skeletonName)
{
    std::ostringstream oss;
    oss << "Error while getting Skeleton from name." <<
           "No Skeleton found with the name: \"" << skeletonName << "\".";
    throw std::runtime_error(oss.str());
}

void ClientCore::CommandReadingThread::run()
{
    try {
        do{
            LCGDEBUG() << "Command reading thread" << std::endl;

            sPacket packetIn;
            std::string theirAddress;
            int theirPort;
            int retValue = owner_->commandSocket_.readDatagram((char *)&packetIn, sizeof(sPacket), theirAddress, &theirPort);
            if(retValue == TRYAGAIN) {
                continue;
            }

            // debug - print message
            LCGDEBUG() << owner_->appID << " Received command from: " << theirAddress <<
                         ": Command=" << packetIn.iMessage << ", nDataBytes=" <<
                         packetIn.nDataBytes <<std::endl;

            // Process command
            owner_->commandProcessor_.processData(&packetIn);

        } while(!exitThread_);
    } catch(std::exception &e) {    // For UdpSocket::readDatagram, CommandProcessor::processData
        riseException(&e);
    }
    exitThread_ = false; // Ready to start again
    LCGDEBUG() << "Exiting CommandReadingThread" << std::endl;
}

ClientCore::CommandReadingThread::CommandReadingThread(ClientCore* owner) :
    BasicThread(owner)
{
    this->seTimeout(secTimeout_, uSecTimeout_);
}

int ClientCore::CommandReadingThread::seTimeout(unsigned int secTimeout, unsigned int uSecTimeout)
{
    BasicThread::seTimeout(secTimeout, uSecTimeout);
    owner_->commandSocket_.setReadingTimeout(secTimeout, uSecTimeout);
}

bool ClientCore::Frame::Unpacker::decodeTimecode(const unsigned int inTimecode,
    const unsigned int inTimecodeSubframe, int& hour, int& minute, int& second,
    int& frame, int& subframe)
{
    bool bValid = true;

    hour = (inTimecode>>24)&255;
    minute = (inTimecode>>16)&255;
    second = (inTimecode>>8)&255;
    frame = inTimecode&255;
    subframe = inTimecodeSubframe;

    return bValid;
}

std::runtime_error ClientCore::Frame::Unpacker::errorFrameOfMOCAPDataCorruptedException(int_fast32_t actualEODTag)
{
    std::ostringstream oss;
    oss << "Error while unpacking a frame of MOCAP data packet." << std::endl <<
           "The expexted EOD tag \"" << EOD_TAG_ << "\" is different from the " <<
           "EOD tag found in the packet: \"" << actualEODTag << "\"." << std::endl <<
           "The packet is corrupted!" << std::endl;
    return std::runtime_error(oss.str());
}

bool ClientCore::Frame::Unpacker::timecodeStringify(const unsigned int inTimecode, const unsigned int inTimecodeSubframe, std::string& str)
{
    bool bValid;
    int hour, minute, second, frame, subframe;
    bValid = decodeTimecode(inTimecode, inTimecodeSubframe, hour, minute, second, frame, subframe);

    std::ostringstream oss;
    oss << std::setfill('0') << std::internal;
    // Format the timecode into  a user friendly string in the form: "hh:mm:ss:ff:yy"
    // std::setw(2) is called before each number because it is not "sticky", it
    // is reseted to .width(0) each time a << overloaded operator is called on oss
    oss << std::setw(2) << hour << ":" << std::setw(2) << minute << ":" <<
           std::setw(2) << second << ":" << std::setw(2) <<  frame << "." <<
           std::setw(2) << subframe;
    str = oss.str();
    str = oss.str();

    return bValid;
}

void ClientCore::WaitForServerThread::run()
{
    try {
        char  szData[20000];
        std::string theirAddress;
        int theirPort;

        do{
            LCGDEBUG() << "Wait for server thread" << std::endl;
            owner_->dataSocket_.readDatagram(szData, sizeof(szData), theirAddress, &theirPort);
            if(!theirAddress.empty()){
                // Exit from this thread in order to wait for data streaming (multicast) on the DataReadingThread
                exit();

                LCGDEBUG() << "Auto server address: " << theirAddress << std::endl;
                *(owner_->pServerAddress) = theirAddress;
                owner_->requestVersions();
            }
        }while(!exitThread_);
    } catch(std::exception &e) {    // For UdpSocket::readDatagram, CommandProcessor::processData
        riseException(&e);
    }
    exitThread_ = false; // Ready to start again
}

ClientCore::WaitForServerThread::WaitForServerThread(ClientCore* owner) :
    BasicThread(owner)
{
}

Markerset* ClientCore::Frame::Model::getMarkersetFromName(std::string markersetName) const
{
    if(mNameToMarkerset.count(markersetName) < 1) {   // Markerset with that name was not defined in the Model
        return NULL;
    }
    return mNameToMarkerset[markersetName];
}

Markerset *ClientCore::Frame::Model::getMarkersetFromID(int markersetID) const
{
    if(mIDtoMarkerset.count(markersetID) < 1) {          // Markerset with that ID was not defined int he model
        return NULL;
    }
    return mIDtoMarkerset[markersetID];
}

RigidBody *ClientCore::Frame::Model::getRigidBodyFromName(std::string rigidBodyName) const
{
    if(mNameToRigidBody.count(rigidBodyName) < 1) {    // RigidBody with that name was not defined in the model
        return NULL;
    }
    return mNameToRigidBody[rigidBodyName];
}

RigidBody* ClientCore::Frame::Model::getRigidBodyFromID(int rigidBodyID) const
{
    if(mIDtoRigidBody.count(rigidBodyID) < 1) {          // RigidBody with that ID was not defined in the Model
        return NULL;
    }
    return mIDtoRigidBody[rigidBodyID];
}

Skeleton *ClientCore::Frame::Model::getSkeletonFromName(std::string skeletonName) const
{
    if(mNameToSkeleton.count(skeletonName) < 1) {        // Skeleton with that name was not defined in the Model
        return NULL;
    }
    return mNameToSkeleton[skeletonName];
}

Skeleton* ClientCore::Frame::Model::getSkeletonFromID(int skeletonID) const
{
    if(mIDtoSkeleton.count(skeletonID) < 1) {            // Skeleton with that ID was not defined in the Model
        return NULL;
    }
    return mIDtoSkeleton[skeletonID];
}


} // namespace optitrack
} // namespace lcg
