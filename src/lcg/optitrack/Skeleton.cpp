
// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Skeleton.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/Skeleton.h"

#include <sstream>      // std::ostringstream
#include <iostream>     // std::cout

namespace lcg {
namespace optitrack {

std::runtime_error Skeleton::noBoneFromNameException(const std::string& boneName) const
{
    std::ostringstream oss;
    oss << "Error while getting a bone from the skeleton \"" << name_ << "\"." << std::endl <<
           "The bone with name \"" << boneName << "\" doesn't exists on this skeleton.";
    return std::runtime_error(oss.str());
}

std::runtime_error Skeleton::noBoneFromIDException(const int &boneID) const
{
    std::ostringstream oss;
    oss << "Error while getting a bone from the skeleton \"" << name_ << "\"." << std::endl <<
           "The bone with ID \"" << boneID << "\" doesn't exists on this skeleton.";
    return std::runtime_error(oss.str());
}

std::runtime_error Skeleton::throwPrintNullException()
{
    return std::runtime_error("Error: Attempting to print null pointer to Skeleton object");
}

Skeleton::Skeleton() :
    nRigidBodies(0)
{
}

Skeleton::~Skeleton()
{
}

void Skeleton::print() const
{
    std::cout << this;
}

bool Skeleton::isEqual(const core::Core &other) const
{
    if(!Core::isEqual(other)) {
        return false;
    }

    const Skeleton* pOtherSkeleton = dynamic_cast<const Skeleton*>(&other);
    if(getNumberOfElements() != pOtherSkeleton->getNumberOfElements()) {
        return false;
    }

    std::map<std::string, RigidBody>::iterator oIt = pOtherSkeleton->nameToBone.begin();
    for(std::map<std::string, RigidBody>::iterator it = nameToBone.begin();
        it != nameToBone.end(); it++) {
        if(it->second != oIt->second){
            return false;
        }
        oIt++;
    }
    return true;
}

int Skeleton::addBone(const RigidBody &newBone)
{
    std::string boneName = newBone.getName();
    nameToBone[boneName] = newBone;
    IDtoBone[newBone.getID()] = &(nameToBone[boneName]);
    return 0;
}

RigidBody Skeleton::getBoneFromName(const std::string &boneName) const
{
    if(nameToBone.count(boneName) > 0) {
        return nameToBone[boneName];
    } else {
        throw noBoneFromNameException(boneName);
    }
}

RigidBody Skeleton::getBoneFromID(const int &boneID) const
{
    if(IDtoBone.count(boneID) > 0) {
        return *IDtoBone[boneID];
    } else {
        throw noBoneFromIDException(boneID);
    }
}

void Skeleton::setNumberOfElements(int numberOfElements)
{
    nRigidBodies = numberOfElements;
}

int Skeleton::getNumberOfElements() const
{
    return nRigidBodies;
}

int Skeleton::clear()
{
    nameToBone.clear();
    IDtoBone.clear();
}

//////////////// Operators \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

bool Skeleton::operator==(const Core& other) const
{
    return isEqual(other);
}

bool Skeleton::operator!=(const Core& other) const
{
    return !isEqual(other);
}

std::ostream& operator<<(std::ostream& os, const Skeleton& skeleton)
{
    os << &skeleton;
    return os;
}

std::ostream& operator<<(std::ostream& os, const Skeleton* skeleton)
{
    if(skeleton==NULL) {
        throw Skeleton::throwPrintNullException();
    }

    os << "Skeleton Name : " << skeleton->getName() << std::endl <<
          "\tID : " << skeleton->getID() << std::endl <<
          "\tBones Count : " << skeleton->getNumberOfElements() << std::endl;
    for(std::map<int, RigidBody*>::iterator it = skeleton->IDtoBone.begin();
        it != skeleton->IDtoBone.end(); it++){
        os << "\t" << it->second;
    }
    return os;
}

}   // optitrack namespace
}   // lcg optitrack
