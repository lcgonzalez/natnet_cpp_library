// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file NatNetClient.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.3.3
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/NatNetClient.h"

#include "lcg/optitrack/ClientCore.h"     // lcg::optitrack::ClientCore

namespace lcg{
namespace optitrack{

// Useful typedefs

typedef lcg::core::Version lcgVersion;

NatNetClient::NatNetClient() :
    pClientCore_(new ClientCore())
{
}

NatNetClient::~NatNetClient()
{
    if(pClientCore_)
        delete pClientCore_;
}

int NatNetClient::connectToServer(std::string szLocalAddress, std::string szServerAddress, int hostCommandPort, int hostDataPort)
{
    return pClientCore_->connectToServer(szLocalAddress, szServerAddress, hostCommandPort, hostDataPort);
}

int NatNetClient::setWaitForNewFrameTimeout(unsigned int secTimeout, unsigned int uSecTimeout)
{
    return pClientCore_->setWaitForNewFrameTimeout(secTimeout, uSecTimeout);
}

int NatNetClient::waitForNewFrame()
{
    return pClientCore_->waitForNewFrame();
}

int NatNetClient::sendCommand(Command command)
{
    return pClientCore_->sendCommand(command);
}

int NatNetClient::getServerVersion(int* serverVersion)
{
    return pClientCore_->getServerVersion(serverVersion);
}

int NatNetClient::getNatNetVersion(int* natNetVersion)
{
    return pClientCore_->getNatNetVersion(natNetVersion);
}

lcgVersion NatNetClient::getServerVersion()
{
    return pClientCore_->getServerVersion();
}

lcgVersion NatNetClient::getNatNetVersion()
{
    return pClientCore_->getNatNetVersion();
}

int NatNetClient::getFrameNumber()
{
    return pClientCore_->getFrameNumber();
}

double NatNetClient::getLatency()
{
    return pClientCore_->getLatency();
}

void NatNetClient::getTimecode(unsigned int &outTimecode, unsigned int &outTimecodeSubFrame)
{
    pClientCore_->getTimecode(outTimecode, outTimecodeSubFrame);
}

std::string NatNetClient::getTimecodeString()
{
    return pClientCore_->getTimecodeString();
}

double NatNetClient::getTimestamp()
{
    return pClientCore_->getTimestamp();
}

bool NatNetClient::isServerRecording()
{
    return pClientCore_->isServerRecording();
}

bool NatNetClient::haveTrackedModelsChanged()
{
    return pClientCore_->haveTrackedModelsChanged();
}

std::string NatNetClient::getServerAppName()
{
    return pClientCore_->getServerAppName();
}

Markerset NatNetClient::getMarkersetFromName(std::string markersetName)
{
    return pClientCore_->getMarkersetFromName(markersetName);
}

Markerset NatNetClient::getUnidentifiedMarkers()
{
    return pClientCore_->getUnidentifiedMarkers();
}

Markerset NatNetClient::getLabeledMarkers()
{
    return pClientCore_->getLabeledMarkers();
}

RigidBody NatNetClient::getRigidBodyFromName(std::string rigidBodyName)
{
    return pClientCore_->getRigidBodyFromName(rigidBodyName);
}

Skeleton NatNetClient::getSkeletonFromName(std::string skeletonName)
{
    return pClientCore_->getSkeletonFromName(skeletonName);
}

void NatNetClient::disconnect()
{
    pClientCore_->disconnect();
}

} // namespace optitrack
} // namespace lcg
