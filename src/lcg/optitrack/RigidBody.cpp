// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file RigidBody.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.2.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/RigidBody.h"

#include <iostream>     // std::cout

namespace lcg{
namespace optitrack{

std::runtime_error RigidBody::throwPrintNullException()
{
    return std::runtime_error("Attempting to print null pointer to RigidBody object");
}

RigidBody::RigidBody() :
    fMeanMarkerError_(0.0),
    bTrackingValid_(false),
    iParentID_(-1)
{
}

RigidBody::~RigidBody()
{
}

void RigidBody::print() const
{
    std::cout << this;
}

bool RigidBody::isEqual(const Core &other) const
{
    // TODO: This should be removed, just comparing the position and orientation
    // is enough to define to rigid bodies as equals.
    if(!Markerset::isEqual(other)){
        return false;
    }
    const RigidBody* pRigidBody = dynamic_cast<const RigidBody*>(&other);
    if(position_ != pRigidBody->position_) {
        return false;
    } else if (orientation_ != pRigidBody->orientation_) {
        return false;
    }
    return true;
}

void RigidBody::setPosition(const float& newX, const float& newY, const float& newZ)
{
    position_.set(newX, newY, newZ);
}

void RigidBody::setPosition(const Position& newPosition)
{
    position_ = newPosition;
}

Position RigidBody::getPosition() const
{
    return position_;
}

void RigidBody::setOrientation(const float& newQX, const float& newQY, const float& newQZ, const float& newQW)
{
    orientation_.set(newQX, newQY, newQZ, newQW);
}

void RigidBody::setOrientation(const Orientation& newOrientation)
{
    orientation_ = newOrientation;
}

Orientation RigidBody::getOrientation() const
{
    return orientation_;
}

void RigidBody::setMeanMarkerError(const float& newMeanMarkerError)
{
    fMeanMarkerError_ = newMeanMarkerError;
}

float RigidBody::getMeanMarkerError() const
{
    return fMeanMarkerError_;
}

void RigidBody::setIsTrackingValid(const bool& newIsTrackingValid)
{
    bTrackingValid_ = newIsTrackingValid;
}

bool RigidBody::isTrackingValid() const
{
    return bTrackingValid_;
}

void RigidBody::setParentID(const int &newParentID)
{
    iParentID_ = newParentID;
}

int RigidBody::getParentID() const
{
    return iParentID_;
}

void RigidBody::setOffset(Position newOffset)
{
    offset_ = newOffset;
}

Position RigidBody::getOffset() const
{
    return offset_;
}

int RigidBody::clear()
{
    Markerset::clear();
    position_.set(0.0,0.0,0.0);
    orientation_.set(0.0,0.0,0.0,0.0);
}

//////////////// Operators \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

bool RigidBody::operator==(const Core& other) const
{
    return isEqual(other);
}

bool RigidBody::operator!=(const Core& other) const
{
    return !isEqual(other);
}

std::ostream& operator<<(std::ostream& os, const RigidBody& rigidBody)
{
    os << &rigidBody;
    return os;
}

std::ostream& operator<<(std::ostream& os, const RigidBody* rigidBody)
{
    if(rigidBody==NULL) {
        throw RigidBody::throwPrintNullException();
    }

    os << "Rigid Body Name : " << rigidBody->getName() << std::endl <<
          "\tID : " << rigidBody->getID() << std::endl <<
          "\tParent ID : " << rigidBody->getParentID() << std::endl <<
          "\tOffset : " << rigidBody->getOffset() << std::endl <<
          "\tPos : " << rigidBody->getPosition() << std::endl <<
          "\tOri : " << rigidBody->getOrientation() << std::endl;
    for(std::map<int, Marker*>::iterator markerIt = rigidBody->IDtoMarker.begin();
        markerIt != rigidBody->IDtoMarker.end(); markerIt++){
        os << "\t\tMarker " << markerIt->second->getID() << ": " <<
              "id=" << markerIt->second->getName() <<
              " size=" << markerIt->second->getSize() <<
              " pos=" << markerIt->second->getPosition() << std::endl;
    }
    os << "\tMean marker error: " << rigidBody->getMeanMarkerError() << std::endl <<
          "\tIs tracking valid?: " << ((rigidBody->isTrackingValid()) ? "True" : "False") << std::endl;

    return os;
}

} // namespace optitrack
} // namespace lcg
