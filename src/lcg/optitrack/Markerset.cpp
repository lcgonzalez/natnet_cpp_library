// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Markerset.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.2.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the NatNet Cpp Library.
//
//                          License Agreement
//                     For the NatNet Cpp Library
//
//    NatNet Cpp Library is an open source library used to directly
//    communicate to a NatNet server.
//
//    NatNet Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    NatNet Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/optitrack/Markerset.h"

#include <sstream>      // std::stringstream
#include <iostream>     // std::cout

namespace lcg{
namespace optitrack{

std::runtime_error Markerset::noMarkerFromNameException(const std::string& markerName) const
{
    std::ostringstream oss;
    oss << "Error while getting Marker from name. "
       << "No Marker found with the name: " << markerName;
    return std::runtime_error(oss.str());
}

std::runtime_error Markerset::noMarkerFromIdException(const int& markerID) const
{
    std::ostringstream oss;
    oss << "Error while getting Marker from ID. "
       << "No Marker found with the ID: " << markerID;
    return std::runtime_error(oss.str());
}

std::runtime_error Markerset::throwPrintNullException()
{
    return std::runtime_error("Attempting to print null pointer to Markerset object");
}

Markerset::Markerset()
{
}

Markerset::~Markerset()
{
}

void Markerset::print() const
{
    std::cout << this;
}

bool Markerset::isEqual(const Core &other) const
{
    if(!Dataset::isEqual(other)){
        return false;
    }
    // TODO: The size comparition should be removed. Just comparing the markers'
    // position is enough to define two Markersets as equals.
    const Markerset* pMarkerset = dynamic_cast<const Markerset*>(&other);
    return IDtoMarker.size() == pMarkerset->IDtoMarker.size()
        && std::equal(stringToMarker.begin(), stringToMarker.end(),
                      pMarkerset->stringToMarker.begin());
}

int Markerset::addMarker(const Marker& newMarker)
{
    std::string markerName = newMarker.getName();

    stringToMarker[markerName] = newMarker;
    IDtoMarker[newMarker.getID()] = &(stringToMarker[markerName]);
    return 0;
}

Marker Markerset::getMarkerFromName(const std::string& markerName) const
{
    if(stringToMarker.count(markerName) > 0) { // If the marker exists
        return stringToMarker[markerName];
    } else {
        throw noMarkerFromNameException(markerName);
    }
}

Marker Markerset::getMarkerFromID(const int& ID) const
{
    if(IDtoMarker.count(ID) > 0) { // If the marker exists
        return *IDtoMarker[ID];
    } else {
        throw noMarkerFromIdException(ID);
    }
}

int Markerset::getNumberOfElements() const
{
    return stringToMarker.size();
}

int Markerset::clear()
{
    stringToMarker.clear();
    IDtoMarker.clear();
}

bool Markerset::operator==(const Core& other) const
{
    return isEqual(other);
}

bool Markerset::operator!=(const Core& other) const
{
    return !isEqual(other);
}

std::ostream& operator<<(std::ostream& os, const Markerset& markerset)
{
    os << &markerset;
    return os;
}

std::ostream& operator<<(std::ostream& os, const Markerset* markerset)
{
    if(markerset==NULL) {
        throw Markerset::throwPrintNullException();
    }
    os << "Markerset " << markerset->getID()
       << ": Model name: " << markerset->getName() << std::endl
       << "Marker count : " << markerset->IDtoMarker.size() << std::endl;
    for(std::map<int,Marker*>::iterator markerIt = markerset->IDtoMarker.begin();
        markerIt != markerset->IDtoMarker.end(); markerIt++){
        os << "\tMarker " << markerIt->second->getID() << ": "
           << "name=" << markerIt->second->getName()
           << " pos=" << markerIt->second->getPosition() << std::endl;
    }
    return os;
}

} // namespace optitrack
} // namespace lcg
